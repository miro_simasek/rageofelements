
attribute vec4 a_Position;
attribute vec2 a_TextureCoordinates;
varying vec2 v_TextureCoordinates;
uniform mat4 u_ProjectionMatrix;
uniform mat4 u_TextureMatrix;
uniform mat4 u_ModelMatrix;

void main(){

	v_TextureCoordinates = (u_TextureMatrix * vec4(a_TextureCoordinates, 0.0, 1.0)).xy;
	gl_Position = u_ProjectionMatrix * u_ModelMatrix  * a_Position;

}
