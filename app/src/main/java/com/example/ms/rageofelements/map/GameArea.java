package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.base.AreaManager;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.combat.Projectile;

import java.util.ArrayList;

import static com.example.ms.rageofelements.base.screens.GamePlayScreen.MS_PER_UPDATE;
import static com.example.ms.rageofelements.map.Segment.SEGMENT_HEIGHT;
import static com.example.ms.rageofelements.map.Segment.SEGMENT_WIDTH;

/**
 * Created by Miroslav Simasek
 * <p>
 * Base class representing base functionality for each game area
 */
public abstract class GameArea {

    protected final Map map;
    final ArrayList<Character> characters;
    private final ArrayList<GameEntity> entities;
    private final ArrayList<Door> doors;
    private final ArrayList<Character> removeList;
    private final AreaManager areaManager;
    private final EArea name;
    private boolean runningFlag;
    private Segment initialPoint;


    GameArea(Map map, AreaManager areaManager, EArea name) {
        this.map = map;
        this.characters = new ArrayList<>();
        this.entities = new ArrayList<>();
        this.doors = new ArrayList<>();
        this.removeList = new ArrayList<>();
        this.areaManager = areaManager;
        this.runningFlag = false;
        this.name = name;

    }

    public void onSet() {
        runningFlag = true;
        Thread animationThread = this.createThread();
        animationThread.start();
    }

    public void onReplace() {
        // allow thread to finis after executing updates
        runningFlag = false;

    }

    private Thread createThread() {
        return new Thread() {
            @Override
            public void run() {
                super.run();
                long beginning;
                long delta;
                long sleepTime;
                while (runningFlag) {

                    beginning = System.currentTimeMillis();
                    synchronized (characters) {
                        for (Character character : characters) {
                            character.updateAnimation();
                        }
                    }

                    synchronized (entities) {
                        for (GameEntity entity : entities) {
                            entity.updateAnimation();
                        }
                    }

                    synchronized (removeList) {
                        for (Character character : removeList) {
                            character.updateAnimation();
                        }
                    }

                    delta = System.currentTimeMillis() - beginning;
                    sleepTime = MS_PER_UPDATE - delta;

                    if (sleepTime > 0) {
                        try {
                            sleep(sleepTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        };
    }

    public Map getMap() {
        return map;
    }

    public void update() {
        synchronized (characters) {
            for (int i = 0; i < characters.size(); i++) {
                Character character = characters.get(i);
                if (character.isActive()) {
                    character.update();
                    if (character.isDead()) {
                        removeList.add(character);
                        characters.remove(i);
                        character.setActivity(EActivities.DEATH);
                        character.setTimeOut(Character.DYING_TIME);
                        character.switchAnimation(true);
                        character.setActive(false);
                        // notify observers about character death
                        character.setObservableChanged(true);
                        character.notifyObservers(EActivities.DEATH);
                    }
                } else {
                    character.checkTimeOut();
                }

            }
        }

        synchronized (entities) {
            for (int i = 0; i < entities.size(); i++) {
                GameEntity otherEntity = entities.get(i);
                if (otherEntity.isActive()) {
                    otherEntity.update();
                    if (otherEntity instanceof Projectile) {
                        Projectile projectile = (Projectile) otherEntity;
                        int x, y;
                        x = (int) -(projectile.getCenterX() / SEGMENT_WIDTH + projectile.getCenterY() / SEGMENT_HEIGHT);
                        y = (int) -(projectile.getCenterY() / SEGMENT_HEIGHT - projectile.getCenterX() / SEGMENT_WIDTH);
                        Segment pos = map.getSegment(x, y);
                        projectile.setSegment(pos);
                    }
                } else if (otherEntity.checkTimeOut()) {
                    otherEntity.getPosition().removeEntity(otherEntity);
                    entities.remove(i);
                }
            }
        }

        synchronized (removeList) {
            for (int i = 0; i < removeList.size(); i++) {
                Character character = removeList.get(i);
                character.checkTimeOut();
                if (character.isActive()) {
                    if (character instanceof Player) {
                        Player.respawn(this);
                        removeList.remove(i);
                        continue;
                    }
                    Segment segment = character.getPosition();
                    segment.removeEntity(character);
                    segment.setItems(character.getInventory());
                    areaManager.sendToDisplay(character.getInventory().toArray(new GameEntity[0]));
                    segment.setObstacle(false);
                    character.setPosition(null);
                    removeList.remove(i);
                }
            }
        }

    }

    public void addEntity(GameEntity gameEntity) {
        if (gameEntity instanceof Character) {
            synchronized (characters) {
                if (gameEntity instanceof Player) {
                    characters.remove(gameEntity);
                }
                characters.add((Character) gameEntity);
            }
        } else {
            synchronized (entities) {
                entities.add(gameEntity);
            }
        }

    }

    public void requestEntityDisplay(GameEntity gameEntity) {
        areaManager.sendToDisplay(gameEntity);
    }

    public ArrayList<Character> getCharacters() {
        return characters;
    }

    public ArrayList<Door> getDoors() {
        return doors;
    }

    public void addDoor(Door door) {
        this.doors.add(door);
    }

    public void removeCharacter(Character character) {
        synchronized (characters) {
            characters.remove(character);
        }
    }

    public EArea getName() {
        return name;
    }

    public void disconnectFromArea(EArea areaName) {
        for (Door door : doors) {
            if (door.getDestination() == areaName) {
                door.getPosition().removeEntity(door);
                doors.remove(door);
            }
        }
    }

    public abstract Segment getInitialPoint();
}
