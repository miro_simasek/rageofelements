package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.map.EArea;

/**
 * Created by Miroslav Simasek
 */

public class Key extends Item {

    private final EArea area;

    public Key(EArea area) {
        super(R.drawable.key, 0, "Key to " + area.getClass().getSimpleName());
        this.area = area;
    }

    public EArea getArea() {
        return area;
    }
}
