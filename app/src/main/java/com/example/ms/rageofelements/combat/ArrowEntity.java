package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureShader;

import static com.example.ms.rageofelements.combat.FireBallEntity.fireballTag;

/**
 * Created by Miroslav Simasek
 *
 */
class ArrowEntity extends Projectile {

    private static final float imageYfit = (Segment.SEGMENT_WIDTH - Segment.SEGMENT_HEIGHT) / 2f;

    ArrowEntity(Character owner, float damageVal, EDirection direction) {
        super(owner, 5f, direction);
        this.getDamages().put(EDamage.SHARP,damageVal);
    }

    @Override
    public void initGraphics() {
        if (position != null) {
            Shape shape = new Shape(createShape(), fireballTag);
            SpriteSheet spriteSheet = new SpriteSheet(8, 8, fireballTag, R.drawable.icicle);
            AnimationData animationData = new AnimationData(2000,8, false, spriteSheet);
            int row = getRow(getBaseDirection());
            animationData.setStart(7, row);
            animationData.setEnd(0, row);
            animationData.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_BOTTOM_TO_TOP);
            animation = new Animation<>(shape);
            animation.putData(fireballTag, getBaseDirection(), animationData);

            animation.switchAnimationByKey(fireballTag, getBaseDirection());
            animation.translateModelMatrix(position.getX(), position.getY());
        }
    }

    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        if (animation!=null)
            animation.draw(matrix,shaderProgram);
    }

    private float[] createShape(){
        return new float[]{
                -Segment.HALF_SEGMENT_WIDTH, Segment.SEGMENT_WIDTH - imageYfit,
                -Segment.HALF_SEGMENT_WIDTH, -imageYfit,
                Segment.HALF_SEGMENT_WIDTH, Segment.SEGMENT_WIDTH - imageYfit,
                Segment.HALF_SEGMENT_WIDTH, -imageYfit};
    }

}
