package com.example.ms.rageofelements.character;

import android.content.res.Resources;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.combat.FireBall;
import com.example.ms.rageofelements.combat.FireStorm;
import com.example.ms.rageofelements.combat.HealingPotion;
import com.example.ms.rageofelements.combat.Item;
import com.example.ms.rageofelements.combat.ManaPotion;
import com.example.ms.rageofelements.combat.ManaShield;
import com.example.ms.rageofelements.combat.NatureTrap;
import com.example.ms.rageofelements.combat.Scroll;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.quests.Quest;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureData;

/**
 * Created by Miroslav Simasek
 *
 */
public class Roshanak extends NPC implements Adressable {

    private final String name;
    private DialogData dialogData;


    public Roshanak(Segment position) {
        super(position, ECharacterGroup.neutral);
        name = GameActivity.getTextFromResources(R.string.roshanak);
        boundaryHeight = Segment.SEGMENT_HEIGHT * 2;
        boundaryWidth = Segment.HALF_SEGMENT_WIDTH;
        boundaryX = position.getX() - boundaryWidth / 2f;
        boundaryY = position.getY() + boundaryHeight / 2f;
        this.goldValue = 1000;
        initAbilities();
    }

    @Override
    public void initGraphics() {
        super.initGraphics();
        Shape shape = new Shape(Character.getCharacterData(1.0f,0f), this.getClass().getSimpleName());
        animation = new Animation<>(shape);

            this.initRunning();
            this.initStanding();
            this.animation.switchAnimationByKey(EActivities.STANDING, EDirection.NORTH);
            this.animation.translateModelMatrix(position.getX(), position.getY());

    }

    @Override
    protected void initInventory() {
        // add items for trading
        inventory.add(new HealingPotion());
        inventory.add(new HealingPotion());
        inventory.add(new HealingPotion());
        inventory.add(new HealingPotion());
        inventory.add(new ManaPotion());
        inventory.add(new ManaPotion());
        inventory.add(new ManaPotion());
        inventory.add(new ManaPotion());
        inventory.add(new ManaPotion());
        inventory.add(new Scroll(new FireBall()));
        inventory.add(new Scroll(new FireBall()));
        inventory.add(new Scroll(new NatureTrap()));
        inventory.add(new Scroll(new NatureTrap()));
        inventory.add(new Scroll(new ManaShield()));
        inventory.add(new Scroll(new NatureTrap()));
        inventory.add(new Scroll(new NatureTrap()));
        inventory.add(new Scroll(new FireStorm()));

        for (Item item : inventory) {
            if (item instanceof Scroll){
                Scroll scroll = (Scroll) item;
                scroll.getSkill().setLevel(2);
            }
        }

    }

    @Override
    protected void initSkills() {
        // this character does not need any skills
    }

    @Override
    protected void initAbilities() {
        abilities.setMaxHelth(10);
        abilities.addHealth(10);
    }

    @Override
    public void adress(DataManager dataManager) {
        dataManager.setDialogData(dialogData);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void initDialog(Quest... questRelated) {

        this.dialogData = new DialogData(this);

        Resources resources = GameActivity.getAppResources();
        String text = resources.getString(R.string.Dialog_RoshanakIntoduction);
        this.addDialogData(name, text, null);

        String title = resources.getString(R.string.DialogTitle_GreetingsTitle);
        text = resources.getString(R.string.Dialog_Greetings);
        this.addDialogData(title, text, null);

        title = resources.getString(R.string.DialogTitle_TroublesInVillageTitle);
        text = resources.getString(R.string.Dialog_TroublesInVillage);

        this.addDialogData(title, text, questRelated[0]);

        title = resources.getString(R.string.DialogTitle_trade);
        this.addDialogData(title, null, null);
//        dialogData.addData(title, null, null);

    }

    @Override
    public void addDialogData(String title, String text, Quest relatedQuest) {
        dialogData.addData(title,text, relatedQuest);
    }

    private void initStanding() {

//        TextureLoader textureLoader = Renderer.getTextureLoader();
        SpriteSheet spriteSheet = new SpriteSheet(5, 4, TextureData.NECROMANCER_STANDING, R.drawable.necromancer_standing_n);
        AnimationData standing = new AnimationData(2000,20, false, spriteSheet);
        standing.setStart(4, 0);
        standing.setEnd(0, 3);
        standing.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_BOTTOM_TO_TOP);

        animation.putData(EActivities.STANDING, EDirection.NORTH, standing);
        animation.putData(EActivities.STANDING, EDirection.NORTH_EAST,standing.copy(R.drawable.necromancer_standing_ne));
        animation.putData(EActivities.STANDING, EDirection.NORTH_WEST,standing.copy(R.drawable.necromancer_standing_nw));
        animation.putData(EActivities.STANDING, EDirection.EAST,standing.copy(R.drawable.necromancer_standing_e));
        animation.putData(EActivities.STANDING, EDirection.SOUTH_EAST,standing.copy(R.drawable.necromancer_standing_se));
        animation.putData(EActivities.STANDING, EDirection.SOUTH,standing.copy(R.drawable.necromancer_standing_s));
        animation.putData(EActivities.STANDING, EDirection.SOUTH_WEST,standing.copy(R.drawable.necromancer_standing_sw));
        animation.putData(EActivities.STANDING, EDirection.WEST,standing.copy(R.drawable.necromancer_standing_w));
    }

    private void initRunning() {
//        TextureLoader textureLoader = Renderer.getTextureLoader();
        SpriteSheet spriteSheet = new SpriteSheet(8, 4, TextureData.NECROMANCER_RUNNING,
                R.drawable.necromancer_running_n);
        AnimationData running = new AnimationData(1000,30, false, spriteSheet);
        running.setStart(0, 3);
        running.setEnd(5, 0);
        running.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_TOP_TO_BOTTOM);
        animation.putData(EActivities.RUNNING, EDirection.NORTH, running);
        animation.putData(EActivities.RUNNING, EDirection.NORTH_EAST,running.copy(R.drawable.necromancer_running_ne));
        animation.putData(EActivities.RUNNING, EDirection.NORTH_WEST,running.copy(R.drawable.necromancer_running_nw));
        animation.putData(EActivities.RUNNING, EDirection.EAST,running.copy(R.drawable.necromancer_running_e));
        animation.putData(EActivities.RUNNING, EDirection.SOUTH_EAST,running.copy(R.drawable.necromancer_running_se));
        animation.putData(EActivities.RUNNING, EDirection.SOUTH,running.copy(R.drawable.necromancer_running_s));
        animation.putData(EActivities.RUNNING, EDirection.SOUTH_WEST,running.copy(R.drawable.necromancer_running_sw));
        animation.putData(EActivities.RUNNING, EDirection.WEST,running.copy(R.drawable.necromancer_running_w));
    }


}
