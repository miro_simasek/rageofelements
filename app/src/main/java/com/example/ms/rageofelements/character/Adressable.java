package com.example.ms.rageofelements.character;

import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.quests.Quest;


public interface Adressable {

    void adress(DataManager dataManager);

    String getName();

    void initDialog(Quest... questRelated);

    void addDialogData( String title, String text, Quest relatedQuest );
}
