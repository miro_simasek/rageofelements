package com.example.ms.rageofelements.character;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.combat.Archery;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.HealingPotion;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureData;

/**
 * Created by Miroslav Simasek
 * Skeleton archer
 */
public class SkeletonArcher extends NPC {

    public SkeletonArcher(Segment position) {
        super(position, ECharacterGroup.undead);
        boundaryHeight = Segment.SEGMENT_HEIGHT * 2;
        boundaryWidth = Segment.HALF_SEGMENT_WIDTH;
        boundaryX = position.getX() - boundaryWidth / 2f;
        boundaryY = position.getY() + boundaryHeight / 2f;
        this.setBasickAttack(EDamage.SHARP, abilities.getStrength());
    }

    @Override
    protected void initSkills() {
        Skill skill = new Archery();
        skill.setLevel(2);
        this.getSkills().add(skill);
    }

    @Override
    protected void initInventory() {
        inventory.add(new HealingPotion());
    }

    @Override
    protected void initAbilities() {
        abilities.setDexterity(10f);
        abilities.setVitality(5f);
        abilities.setStrength(5f);
        abilities.setKillExperience(150);
        abilities.setMaxHelth(80);
        abilities.addHealth(100);
    }

    @Override
    public void initGraphics() {
        super.initGraphics();
        animation = new Animation<>(new Shape(Character.getCharacterData(0.6f,1f), this.getClass().getSimpleName()));
        this.initStanding();
        this.initShooting();
        this.initDeath();
        this.initHit();
        this.initRunning();
        animation.switchAnimationByKey(EActivities.STANDING, EDirection.NORTH);
        animation.translateModelMatrix(position.getX(), position.getY());
    }

    private void initStanding() {
        SpriteSheet spritesheet1 = new SpriteSheet(8, 8, TextureData.SKELETON_ARCHER_STANDING, R.drawable.skeleton_archer_standing);
        AnimationData data = new AnimationData(Character.STANDING_ANIMATION_TIME,8,false,spritesheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,7);

        animation.putData(EActivities.STANDING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,6);
        data.setEnd(7,6);
        animation.putData(EActivities.STANDING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,5);
        animation.putData(EActivities.STANDING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,4);
        data.setEnd(7,4);
        animation.putData(EActivities.STANDING,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,3);
        animation.putData(EActivities.STANDING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.STANDING,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.STANDING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.STANDING,EDirection.NORTH,data);

    }

    private void initShooting(){
        SpriteSheet spritesheet1 = new SpriteSheet(8, 8, TextureData.SKELETON_ARCHER_SHOOTING, R.drawable.skeleton_archer_shooting);
        AnimationData data = new AnimationData(Character.SHOOTING_TIME_OUT,8,false,spritesheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,7);

        animation.putData(EActivities.SHOOTING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,6);
        data.setEnd(7,6);
        animation.putData(EActivities.SHOOTING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,5);
        animation.putData(EActivities.SHOOTING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,4);
        data.setEnd(7,4);
        animation.putData(EActivities.SHOOTING,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,3);
        animation.putData(EActivities.SHOOTING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.SHOOTING,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.SHOOTING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.SHOOTING,EDirection.NORTH,data);
    }

    private void initHit(){
        SpriteSheet spritesheet1 = new SpriteSheet(8, 8, TextureData.SKELETON_ARCHER_HIT, R.drawable.skeleton_archer_hit);
        AnimationData data = new AnimationData(Character.HIT_TIME_OUT,8,false,spritesheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,7);

        animation.putData(EActivities.HIT,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,6);
        data.setEnd(7,6);
        animation.putData(EActivities.HIT,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,5);
        animation.putData(EActivities.HIT,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,4);
        data.setEnd(7,4);
        animation.putData(EActivities.HIT,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,3);
        animation.putData(EActivities.HIT,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.HIT,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.HIT,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.HIT,EDirection.NORTH,data);
    }

    private void  initDeath(){
        SpriteSheet spritesheet1 = new SpriteSheet(8, 8, TextureData.SKELETON_ARCHER_DEATH, R.drawable.skeleton_archer_death);
        AnimationData data = new AnimationData(Character.DEATH_ANIMATION_TIME,8,false,spritesheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,7);

        animation.putData(EActivities.DEATH,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,6);
        data.setEnd(7,6);
        animation.putData(EActivities.DEATH,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,5);
        animation.putData(EActivities.DEATH,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,4);
        data.setEnd(7,4);
        animation.putData(EActivities.DEATH,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,3);
        animation.putData(EActivities.DEATH,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.DEATH,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.DEATH,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.DEATH,EDirection.NORTH,data);
    }

    private void initRunning(){
        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.SKELETON_ARCHER_RUNNING,R.drawable.skeleton_archer_running1);
        AnimationData data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.SKELETON_ARCHER_RUNNING,R.drawable.skeleton_archer_running2);
        data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.NORTH,data);
    }

}
