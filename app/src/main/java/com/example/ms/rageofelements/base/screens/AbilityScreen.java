package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Abilities;

/**
 * Created by Miroslav Simasek
 * Screen to display players abilities
 */
public class AbilityScreen extends Fragment {

    private DataManager dataManager;
    private Abilities abilities;
    private Button strength;
    private Button vitality;
    private Button dexterity;
    private Button concentration;
    private TextView strength_label;
    private TextView vitality_label;
    private TextView concentration_label;
    private TextView dexterity_label;
    private TextView points;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = ((GameActivity) getActivity()).getDataManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.ability_layout, container, false);
        abilities = dataManager.getAbilities();

        strength = (Button) v.findViewById(R.id.strength_button);
        vitality = (Button) v.findViewById(R.id.vitality_button);
        concentration = (Button) v.findViewById(R.id.concentration_button);
        dexterity = (Button) v.findViewById(R.id.dexterity_button);

        strength_label = (TextView) v.findViewById(R.id.strength_value);
        strength_label.setText(String.valueOf(abilities.getStrength()));

        vitality_label = (TextView) v.findViewById(R.id.vitality_value);
        vitality_label.setText(String.valueOf(abilities.getVitality()));

        dexterity_label = (TextView) v.findViewById(R.id.dexterity_value);
        dexterity_label.setText(String.valueOf(abilities.getDexterity()));

        concentration_label = (TextView) v.findViewById(R.id.concentration_value);
        concentration_label.setText(String.valueOf(abilities.getConcentration()));

        points = (TextView) v.findViewById(R.id.increase_point_count);
        points.setText(String.valueOf(abilities.getPointToIncrease()));

        TextView experience = (TextView) v.findViewById(R.id.experience_value);
        experience.setText(String.valueOf(abilities.getExperience()));

        TextView nextLevel = (TextView) v.findViewById(R.id.next_level_value);
        nextLevel.setText(String.valueOf(abilities.getLevelRequirements()));

        TextView level = (TextView) v.findViewById(R.id.current_level);
        level.setText(String.valueOf(abilities.getLevel()));

        this.initControls();

        return v;
    }

    private void initControls() {
        if (abilities.getPointToIncrease() > 0) {
            final Button[] buttons = {strength, vitality, concentration, dexterity};
            for (Button button : buttons) {
                button.setHighlightColor(Color.BLUE);
            }

            strength.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abilities.addStrenth();
                    strength_label.setText(String.valueOf(abilities.getStrength()));
                    points.setText(String.valueOf(abilities.getPointToIncrease()));
                    if (abilities.getPointToIncrease() == 0) {
                        for (Button button : buttons) {
                            button.setHighlightColor(0);
                            button.setOnClickListener(null);
                        }
                    }
                }
            });

            dexterity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abilities.addDexterity();
                    dexterity_label.setText(String.valueOf(abilities.getDexterity()));
                    points.setText(String.valueOf(abilities.getPointToIncrease()));
                    if (abilities.getPointToIncrease() == 0) {
                        for (Button button : buttons) {
                            button.setHighlightColor(0);
                            button.setOnClickListener(null);
                        }
                    }
                }
            });

            vitality.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abilities.addVitality();
                    vitality_label.setText(String.valueOf(abilities.getVitality()));
                    points.setText(String.valueOf(abilities.getPointToIncrease()));
                    if (abilities.getPointToIncrease() == 0) {
                        for (Button button : buttons) {
                            button.setHighlightColor(0);
                            button.setOnClickListener(null);
                        }
                    }
                }
            });

            concentration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abilities.addConcentration();
                    concentration_label.setText(String.valueOf(abilities.getConcentration()));
                    points.setText(String.valueOf(abilities.getPointToIncrease()));
                    if (abilities.getPointToIncrease() == 0) {
                        for (Button button : buttons) {
                            button.setHighlightColor(0);
                            button.setOnClickListener(null);
                        }
                    }
                }
            });
        }
    }


}
