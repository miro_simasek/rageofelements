package com.example.ms.rageofelements.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ConfigurationInfo;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.screens.GamePlayScreen;
import com.example.ms.rageofelements.character.Player;


/**
 * Game-play activity
 */
public class GameActivity extends Activity {

    private static Resources appResources; // for purpose of resource reading
    private DataManager dataManager;
    private String playerType;

    public static String getTextFromResources(int resID){
        return appResources.getString(resID);
    }
    public static Resources getAppResources() {
        return appResources;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appResources = this.getApplicationContext().getResources();
        // create attribute for transporting game data between fragments
        dataManager = new DataManager();
//        if no character was selected stop application
        if (getIntent().getExtras() == null) {
            Toast.makeText(this, Messages.noCharacter, Toast.LENGTH_LONG).show();

            // return to main activity
            Intent intent = new Intent(getBaseContext(),MenuActivity.class);
            startActivity(intent);
            this.finish();
        }

        //Setting activity to full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // setting activity to be hardware accelerated
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

        //Setting screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Check if OPEN GL ES 2.0 is supported on device

        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo ci = am.getDeviceConfigurationInfo();
        final boolean supportsES20 = ci.reqGlEsVersion >= 0x20000 ||
                Build.FINGERPRINT.startsWith("generic") ||
                Build.FINGERPRINT.startsWith("unknown") ||
                Build.MODEL.contains("google_sdk") ||
                Build.MODEL.contains("Emulator") ||
                Build.MODEL.contains("Android SDK build for x86");

        if (!supportsES20) {
            //  If openGL es 2.0 is not supported, show message and exit game
            Toast.makeText(this, appResources.getString(R.string.openGL_not_supported), Toast.LENGTH_LONG).show();
            // return to main activity
            Intent intent = new Intent(getBaseContext(),MenuActivity.class);
            startActivity(intent);
            this.finish();
        }
        setContentView(R.layout.main_layout);

        playerType = getIntent().getExtras().getString(Constants.characterKey);
        //Create and add fragment for initial game state
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//        create game play
        Fragment initialFragment = new GamePlayScreen();
//        pass values to game play
        initialFragment.setArguments(getIntent().getExtras());
        fragmentTransaction.add(R.id.main_layout, initialFragment);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            Intent intent = new Intent(getBaseContext(),MenuActivity.class);
            startActivity(intent);
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
//        allow application appResources to be freed
        Player.clearInstance();
        appResources = null;

    }

    @Override
    protected void onResume() {
        super.onResume();
//       set application appResources to be available
        appResources = this.getApplicationContext().getResources();

    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public String getPlayerType() {
        return playerType;
    }
}
