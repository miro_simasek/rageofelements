package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by Miroslav Simasek
 * Archery skill
 */
public class Archery extends Skill {

    private final float damageIndex;
    private float damage;
    public Archery() {
        super("Archery", R.drawable.archery);
        this.setMaxLevel(5);
        damage = 10f;
        damageIndex = 0.05f;
    }

    @Override
    public void increaseLevel() {
        if (level >= getMaxLevel())
            return;
        super.increaseLevel();
        damage += 2f;
    }

    @Override
    public EActivities getRelatedActivity() {
        return EActivities.SHOOTING;
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        if (segment == null || segment.length == 0 || segment[0] == null) {
            return null;
        }
        float finalDamage = damage + damageIndex * character.getAbilities().getDexterity();

        ArrowEntity entity = new ArrowEntity(character, finalDamage, Map.getDirection(character.getPosition(),segment[0]));
        float[] directions;
        directions = Projectile.getProjectileDirection(entity, segment[0]);
        entity.setDirections(directions[0], directions[1]);
        this.adjustCharacter(character,segment[0]);
        return new GameEntity[]{entity};
    }
}
