package com.example.ms.rageofelements.base.util;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.Constants;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.Messages;
import com.example.ms.rageofelements.base.screens.TradeScreen;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.DialogData;
import com.example.ms.rageofelements.quests.Quest;

/**
 * Created by Miroslav Simasek
 *
 */

public class DialogAdapter extends ArrayAdapter<String> {

    private DataManager dataManager;
    private FragmentManager fragmentManager;

    public DialogAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        final int pos = position;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) v.getRootView().findViewById(R.id.dialog_title);
                String title = getItem(pos);
                final Character character = (Character) dataManager.getDialogData().getOwner();
                // if we clicked  trade option, init trading
                if (DialogData.TRADE.equals(title)){
                    dataManager.setOtherItems(character.getInventory());
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_layout,new TradeScreen(), Constants.tradeScreen);
                    fragmentTransaction.addToBackStack(Constants.tradeScreen);
                    fragmentTransaction.commit();
                    return;
                }

                // set dialog text and title
                textView.setText(title);
                final Pair<String, Quest> related = dataManager.getDialogData()
                        .getDataByTile(title);
                TextView content = (TextView) v.getRootView().findViewById(R.id.dialog_content);
                content.setText(related.first);

                // get confirm button for quests
                Button confirmButton = (Button) v.getRootView().findViewById(R.id.confirm_button);
                // if dialog contain som quest
                if (related.second != null) {
                    final String titleKey = title;
                    // make confirm button enabled
                    confirmButton.setVisibility(View.VISIBLE);
                    confirmButton.setEnabled(true);

                    confirmButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // add quest to quest manager
                            dataManager.getQuestManager().addQuest(related.second);
                            remove(titleKey);
                            dataManager.getDialogData().removeDialogOption(titleKey);
                            Toast.makeText(v.getContext(),
                                    Messages.newQuest.replace("&1", related.second.getName()),
                                    Toast.LENGTH_LONG).show();
                            character.setObservableChanged(true);
                            character.notifyObservers(related);

                            v.setEnabled(false);
                            v.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        });

        return view;
    }

    public void setDataManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }


    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }
}
