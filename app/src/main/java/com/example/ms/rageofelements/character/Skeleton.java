package com.example.ms.rageofelements.character;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.combat.Diamond;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.HealingPotion;
import com.example.ms.rageofelements.combat.ManaPotion;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureData;

/**
 * Created by MS on 3/19/2017.
 */

public class Skeleton extends NPC {

//    private VBO boundary;

    public Skeleton(Segment position) {
        super(position, ECharacterGroup.undead);
        boundaryHeight = Segment.SEGMENT_HEIGHT * 2;
        boundaryWidth = Segment.HALF_SEGMENT_WIDTH;
        boundaryX = position.getX() - boundaryWidth / 2f;
        boundaryY = position.getY() + boundaryHeight / 2f ;
//        initAbilities();
        this.setBasickAttack(EDamage.SHARP,10);

    }

    @Override
    public void setActivity(EActivities activity) {
        super.setActivity(activity);
    }

    @Override
    protected void initAbilities() {
        abilities.setVitality(5);
        abilities.setStrength(3);
        abilities.setMaxHelth(100);
        abilities.addHealth(100);
        abilities.setKillExperience(100);
    }

    @Override
    protected void initInventory() {
        this.inventory.add(new Diamond());
        this.inventory.add(new HealingPotion());
        this.inventory.add(new ManaPotion());
    }

    @Override
    protected void initSkills() {
        // skeleton does not need any skills
    }

    @Override
    public void initGraphics() {
        super.initGraphics();
        animation = new Animation<>(new Shape(Character.getCharacterData(1.0f,0f), this.getClass().getSimpleName()));
        this.initStanding();
        this.initRunning();
        this.initAttacking();
        this.initHit();
        this.initDeath();
        animation.switchAnimationByKey(EActivities.STANDING, EDirection.NORTH);
        animation.translateModelMatrix(position.getX(), position.getY());

    }

    private void initHit() {
        SpriteSheet spritesheet = new SpriteSheet(8, 8, TextureData.SKELETON_HIT, R.drawable.skeleton_hit);
        AnimationData data = new AnimationData(Character.HIT_TIME_OUT, 8, false, spritesheet);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        EActivities activity = EActivities.HIT;
        animation.putData(activity, EDirection.SOUTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(activity,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(activity,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(activity,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(activity,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(activity,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(activity,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(activity,EDirection.EAST,data);
    }

    private void initStanding(){
        SpriteSheet spritesheet = new SpriteSheet(8, 8, TextureData.SKELETON_STANDING, R.drawable.skeleton_standing);
        AnimationData data = new AnimationData(Character.STANDING_ANIMATION_TIME, 8, false, spritesheet);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        EActivities activity = EActivities.STANDING;
        animation.putData(activity, EDirection.SOUTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(activity,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(activity,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(activity,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(activity,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(activity,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(activity,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(activity,EDirection.EAST,data);


    }

    private void initRunning() {

        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.SKELETON_RUNNING,R.drawable.skeleton_running1);
        AnimationData data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.SKELETON_RUNNING,R.drawable.skeleton_running2);
        data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.EAST,data);

    }

    private void initAttacking() {
        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.SKELETON_ATTACK,R.drawable.skeleton_attack1);
        AnimationData data = new AnimationData(Character.ATTACK_TIME_OUT,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.ATTACK,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.SKELETON_ATTACK,R.drawable.skeleton_attack2);
        data = new AnimationData(Character.ATTACK_TIME_OUT,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.ATTACK,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.ATTACK,EDirection.EAST,data);

    }

    private void initDeath(){
        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.SKELETON_DEATH,R.drawable.skeleton_death1);
        AnimationData data = new AnimationData(Character.DEATH_ANIMATION_TIME,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.DEATH,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.DEATH,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.DEATH,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.DEATH,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.SKELETON_STANDING,R.drawable.skeleton_death2);
        data = new AnimationData(Character.DEATH_ANIMATION_TIME,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.DEATH,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.DEATH,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.DEATH,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.DEATH,EDirection.EAST,data);
    }
}
