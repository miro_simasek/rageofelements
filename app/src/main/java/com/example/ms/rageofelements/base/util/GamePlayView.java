package com.example.ms.rageofelements.base.util;

import android.content.Context;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.MenuActivity;
import com.example.ms.rageofelements.base.screens.GamePlayScreen;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.render.Renderable;

public class GamePlayView extends GLSurfaceView {
    private com.example.ms.rageofelements.render.Renderer renderer;
    private Map map;
    private Player player;
    private GamePlayScreen parent;

    public GamePlayView(Context context) {
        super(context);
    }

    public GamePlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setParent( GamePlayScreen parent ){
        this.parent = parent;
        this.renderer.setRoot( parent );
    }

    @Override
    public void onResume() {
        super.onResume();
        renderer.initGameDisplay(map,player);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.renderer.disposeCurrent();
    }

    @Override
    public void setRenderer(Renderer renderer) {
        super.setRenderer(renderer);
        this.renderer = (com.example.ms.rageofelements.render.Renderer)renderer;
    }

    public void setForDisplay(Renderable... renderable){
        for (Renderable ren : renderable) {
            renderer.initRenderable(ren);
        }
    }

    public com.example.ms.rageofelements.render.Renderer getRenderer() {
        return renderer;
    }

    public void initGame(Map map, Player player){
        this.map = map;
        this.player = player;
    }

    public void runOnUiThread(Runnable runnable) {
        if (this.parent != null){
            parent.getActivity().runOnUiThread(runnable);
        }
    }


    public GamePlayScreen getFragment() {
        return parent;
    }

    public void finisGame() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GameActivity activity = (GameActivity) parent.getActivity();
                Intent intent = new Intent(activity.getBaseContext(),MenuActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        });
    }
}
