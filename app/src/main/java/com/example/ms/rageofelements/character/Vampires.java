package com.example.ms.rageofelements.character;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.strategy.VampiresStrategy;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.FireBall;
import com.example.ms.rageofelements.combat.FireStorm;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.quests.Quest;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureData;

public class Vampires extends NPC implements Adressable{

    private DialogData dialogData;

    public Vampires(Segment position) {
        super(position, ECharacterGroup.neutral);
        boundaryHeight = Segment.SEGMENT_HEIGHT * 2;
        boundaryWidth = Segment.HALF_SEGMENT_WIDTH;
        boundaryX = position.getX() - boundaryWidth / 2f;
        boundaryY = position.getY() + boundaryHeight / 2f ;
        this.setBasickAttack(EDamage.SHARP,15f);
    }


    @Override
    protected void initSkills() {
        FireBall fireBall = new FireBall();
        fireBall.setLevel(3);
        getSkills().add(fireBall);
        FireStorm fireStorm = new FireStorm();
        fireStorm.setLevel(3);
        getSkills().add(fireStorm);
    }

    @Override
    protected void initInventory() {

    }

    @Override
    protected void initAbilities() {
        abilities.setMaxHelth(500);
        abilities.addHealth(500);
        abilities.setMaxMana(300);
        abilities.setDexterity(15f);
        abilities.setVitality(20f);
        abilities.setConcentration(20f);
        abilities.setStrength(15f);
    }

    @Override
    public void initGraphics() {
        super.initGraphics();
        Shape shape = new Shape(Character.getCharacterData(0.8f,1.0f), this.getClass().getSimpleName());
        animation = new Animation<>(shape);

        // initialize character behaviour animations
        this.initStanding();
        this.initAttacking();
        this.initRunning();
        this.initCasting();
        this.initHit();
        this.initDeath();

        this.animation.switchAnimationByKey(EActivities.STANDING, EDirection.NORTH);
        this.animation.translateModelMatrix(position.getX(), position.getY());


    }


    private void initHit(){
        SpriteSheet spritesheet = new SpriteSheet(8, 8, TextureData.VAMPIRES_HIT, R.drawable.vampiress_hit);
        AnimationData data = new AnimationData(Character.HIT_TIME_OUT, 8, true, spritesheet);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        EActivities activity = EActivities.HIT;
        animation.putData(activity, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(activity,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(activity,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(activity,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(activity,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(activity,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(activity,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(activity,EDirection.SOUTH,data);
    }

    private void initCasting(){
        SpriteSheet spritesheet = new SpriteSheet(8, 8, TextureData.VAMPIRES_CASTING, R.drawable.vampiress_casting);
        AnimationData data = new AnimationData(Character.MAGIC_TIME_OUT, 8, true, spritesheet);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        EActivities casting = EActivities.CASTING;
        animation.putData(casting, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(casting,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(casting,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(casting,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(casting,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(casting,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(casting,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(casting,EDirection.SOUTH,data);
    }

    private void initDeath(){
        SpriteSheet spritesheet = new SpriteSheet(8, 8, TextureData.VAMPIRES_DEATH, R.drawable.vampiress_death);
        AnimationData data = new AnimationData(Character.MAGIC_TIME_OUT, 8, true, spritesheet);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        EActivities activity = EActivities.DEATH;
        animation.putData(activity, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(activity,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(activity,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(activity,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(activity,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(activity,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(activity,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(activity,EDirection.SOUTH,data);

    }

    private void initStanding() {
        SpriteSheet spritesheet = new SpriteSheet(8, 8, TextureData.VAMPIRES_STANDING, R.drawable.vampiress_standing);
        AnimationData data = new AnimationData(Character.STANDING_ANIMATION_TIME, 8, false, spritesheet);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        EActivities activity = EActivities.STANDING;
        animation.putData(activity, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(activity,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(activity,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(activity,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(activity,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(activity,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(activity,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(activity,EDirection.SOUTH,data);
    }

    private void initAttacking() {
        SpriteSheet spritesheet = new SpriteSheet(8, 8, TextureData.VAMPIRES_ATTACK, R.drawable.vampiress_attack);
        AnimationData data = new AnimationData(Character.ATTACK_TIME_OUT, 8, false, spritesheet);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        animation.putData(EActivities.ATTACK, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(EActivities.ATTACK,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(EActivities.ATTACK,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH,data);
    }

    private void initRunning() {
        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.VAMPIRES_RUNNING,R.drawable.vampires_running1);
        AnimationData data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.VAMPIRES_RUNNING,R.drawable.vampires_running2);
        data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.NORTH,data);
    }

    @Override
    public void adress(DataManager dataManager) {
        dataManager.setDialogData(dialogData);
    }

    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.vampires);
    }

    @Override
    public void initDialog(Quest... questRelated) {
        dialogData = new DialogData(this);

        dialogData.addData(GameActivity.getTextFromResources(R.string.confront_summoner),
                GameActivity.getTextFromResources(R.string.confront_text)
                ,questRelated[0]);
    }

    @Override
    public void addDialogData(String title, String text, Quest relatedQuest) {
        dialogData.addData(title,text,relatedQuest);
    }


    public void createSkeletons() {
        VampiresStrategy strategy = (VampiresStrategy) this.getStrategyManager();
        strategy.summonSkeletonArmy();
    }
}
