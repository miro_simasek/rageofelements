package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.Messages;
import com.example.ms.rageofelements.base.util.InstantActionButton;
import com.example.ms.rageofelements.base.util.ItemAdapter;
import com.example.ms.rageofelements.combat.Item;
import com.example.ms.rageofelements.combat.Key;
import com.example.ms.rageofelements.combat.Usable;

/**
 * Created by MS on 3/25/2017.
 */

public class InventoryScreen extends Fragment {

    private final static String segmentAdapter = "segment_adapter";
    private final static String characterAdapter = "character_adapter";
    private InstantActionButton[] activeUsables;
    private DataManager dataManager;
    private ItemAdapter segmentItemsAdapter, characterItemsAdapter;
    private GridView myItemsView, segmentItemsView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameActivity gameActivity = (GameActivity) getActivity();
        dataManager = gameActivity.getDataManager();
        activeUsables = new InstantActionButton[GamePlayScreen.activeButtonsCount];
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.inventory_layout, container, false);

        segmentItemsAdapter = new ItemAdapter(v.getContext(), segmentAdapter);
        characterItemsAdapter = new ItemAdapter(v.getContext(), characterAdapter);

        segmentItemsAdapter.setItems(dataManager.getSegmentItems());
        segmentItemsAdapter.notifyDataSetChanged();

        characterItemsAdapter.setItems(dataManager.getPlayerItems());
        characterItemsAdapter.notifyDataSetChanged();

        segmentItemsView = (GridView) v.findViewById(R.id.i_other_content);
        segmentItemsView.setAdapter(segmentItemsAdapter);

        myItemsView = (GridView) v.findViewById(R.id.i_my_content);
        myItemsView.setAdapter(characterItemsAdapter);

        activeUsables[0] = (InstantActionButton) v.findViewById(R.id.activeView1);
        activeUsables[1] = (InstantActionButton) v.findViewById(R.id.activeView2);
        activeUsables[2] = (InstantActionButton) v.findViewById(R.id.activeView3);
        activeUsables[3] = (InstantActionButton) v.findViewById(R.id.activeView4);
        activeUsables[4] = (InstantActionButton) v.findViewById(R.id.activeView5);

        this.initControls();

        return v;
    }


    private void initControls(){
        segmentItemsView.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int index;
                GridView itemsView = (GridView) v;
                switch (event.getAction()) {
                    case DragEvent.ACTION_DROP:

                        index = Integer.valueOf(event.getClipData().getItemAt(0).getText().toString());
                        if (itemsView.getAdapter() instanceof ItemAdapter) {
                            String tag = event.getClipData().getItemAt(1).getText().toString();
                            if (tag.equals(segmentAdapter)){return true;}
                            Item item = characterItemsAdapter.getItem(index);
                            // we don't want to allow character to drop keys
                            if (item instanceof Key){
                                Toast.makeText(itemsView.getContext(),R.string.keys_cannot_be_dropped,Toast.LENGTH_SHORT)
                                        .show();
                                return true;
                            }
                            // if we checked the item for removal, remove it
                            characterItemsAdapter.removeItem(index);
                            characterItemsAdapter.notifyDataSetChanged();
                            // put item to the ground (segment)
                            segmentItemsAdapter.addItem(item);
                            segmentItemsAdapter.notifyDataSetChanged();

                            //check if item that is going to be removed is currently in use
                            for (InstantActionButton activeUsable : activeUsables) {
                                // if item si in use, remove it from corresponding controls
                                Usable usable = activeUsable.getUsable();
                                if (usable == item)
                                    activeUsable.setUsabe(null);
                            }


                        }
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;
                    default:
                        break;
                }

                return false;
            }
        });

        myItemsView.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                GridView itemsView = (GridView) v;
                int index;
                switch (event.getAction()) {
                    case DragEvent.ACTION_DROP:
                        String tag = event.getClipData().getItemAt(1).getText().toString();
                        if (tag.equals(characterAdapter)){return true;}
                        index = Integer.valueOf(event.getClipData().getItemAt(0).getText().toString());
                        if (itemsView.getAdapter() instanceof ItemAdapter) {
                            characterItemsAdapter.addItem(segmentItemsAdapter.removeItem(index));
                            characterItemsAdapter.notifyDataSetChanged();
                            segmentItemsAdapter.notifyDataSetChanged();
                        }
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;
                    default:
                        break;
                }

                return false;
            }
        });

        View.OnDragListener onDragListener = new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                if (event.getAction() == DragEvent.ACTION_DROP) {
                    InstantActionButton button = (InstantActionButton) v;
                    int index;
                    index = Integer.valueOf(event.getClipData().getItemAt(0).getText().toString());
                    switch (event.getClipData().getItemAt(1).getText().toString()) {
                        case characterAdapter:
                            Item item = characterItemsAdapter.getItem(index);
                            if (item instanceof Usable) {
                                button.setUsabe((Usable) characterItemsAdapter.getItem(index));
                            } else {
                                Toast.makeText(v.getContext(), Messages.notUsable,Toast.LENGTH_SHORT).show();
                            }
                            return true;
                        default:
                            return false;
                    }
                }
                return true;
            }
        };

        activeUsables[0].setOnDragListener(onDragListener);
        activeUsables[1].setOnDragListener(onDragListener);
        activeUsables[2].setOnDragListener(onDragListener);
        activeUsables[3].setOnDragListener(onDragListener);
        activeUsables[4].setOnDragListener(onDragListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        // upload changes to data manager
//        dataManager.setPlayerItems(characterItemsAdapter.getItems());
//        dataManager.setSegmentItems(segmentItemsAdapter.getItems());
        // upload changes on usable objects to data manager
        Usable[] usabels = new Usable[activeUsables.length];
        for (int i = 0; i < usabels.length; i++) {
            usabels[i] = activeUsables[i].getUsable();
        }
        dataManager.setActionButtonsContent(usabels);

    }

    @Override
    public void onResume() {
        super.onResume();
        // set content to action button views from data manager
        Usable[] usables = dataManager.getActionButtonsContent();
        for (int i = 0; i < activeUsables.length; i++) {
            activeUsables[i].setUsabe(usables[i]);
        }
    }
}
