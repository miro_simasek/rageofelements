package com.example.ms.rageofelements.render;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.util.SparseIntArray;

import com.example.ms.rageofelements.base.GameActivity;

import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_LINEAR_MIPMAP_LINEAR;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glGenerateMipmap;
import static android.opengl.GLES20.glTexParameteri;
import static android.opengl.GLUtils.texImage2D;


/**
 * Class handling texture load
 */
class TextureLoader {

    private static SparseIntArray loadedResources = new SparseIntArray();
//    private final Context context;

    TextureLoader(/*Context context*/){
//        this.context = context;
    }

    /**
     * Loads single texture from resource
     *
     * @param resourceID - ID of resource containing texture
     * @return - Texture id in OpenGL Context
     */
    int getTexture(int resourceID){
//        if texture was already loaded from resource, return loaded texture ID

        if (loadedResources.get(resourceID)!=0){
            return loadedResources.get(resourceID);
        }

        final int[] textureObjectsID = new int[1];
        // generate texture
        glGenTextures(1, textureObjectsID, 0);

        if(textureObjectsID[0] == 0){
            return 0;
        }

        //connect texture
        glBindTexture(GL_TEXTURE_2D, textureObjectsID[0]);

        //setting for texture filtering
        //GL_TEXTURE_MIN_FILTER -> minification, GL_LINEAR_MIPMAP_LINEAR->trilinear filtering
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        //GL_TEXTURE_MAG_FILTER-> magnification, GL_LINEAR-> bilinear filtering
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        // create bitmap to be loaded as texture
        final BitmapFactory.Options options = new BitmapFactory.Options();
        //do not scale bitmap by default
        options.inScaled = false;

        //create bitmap from resources
        Bitmap bitmap = BitmapFactory.decodeResource(GameActivity.getAppResources(),resourceID,options);
        //resize bitmap to nearest power of two if needed
        bitmap = getPowerOf2Bitmap(bitmap);

        if(bitmap == null){
            glDeleteTextures(1, textureObjectsID, 0);

            return 0;
        }

        //read data to OpenGL

        texImage2D(GL_TEXTURE_2D,0, bitmap, 0);

        //remove bitmap from memory
        bitmap.recycle();

        //generate texture layers
        glGenerateMipmap(GL_TEXTURE_2D);

        //disconnect texture
        glBindTexture(GL_TEXTURE_2D,0);

//        Log.w(TAG,"Resource ID "+resourceID+" Texture Loaded");
        loadedResources.put(resourceID,textureObjectsID[0]);
        return textureObjectsID[0];
    }

    static void clearTextures(){
        int[] textures = new int[loadedResources.size()];
        for(int i = 0; i<loadedResources.size();i++){
            textures[i] = loadedResources.valueAt(i);
        }
        GLES20.glDeleteTextures(textures.length,textures,0);
        loadedResources.clear();
    }

    public void loadTextures(int... resources){
        for (int resource : resources) {
            getTexture(resource);
        }

    }

    /**
     * Scale bitmap to power of two
     *
     * @return bitmap scaled to power of 2 dimensions
     */
    private static Bitmap getPowerOf2Bitmap(Bitmap original){
//        if (isPowerOfTwo(original.getWidth())&&isPowerOfTwo(original.getHeight())){
//            return original;
//        }
        // if no both sizes are power of two we need to scale it
        int width = original.getWidth();
        int height = original.getHeight();

//        if (!isPowerOfTwo(width)){
            width = getNearestPowerOfTwo(width);
//        }

//        if (!isPowerOfTwo(height)){
            height = getNearestPowerOfTwo(height);
//        }

        Bitmap resized = Bitmap.createScaledBitmap(original,width,height,false);
//        original.recycle();
        return resized;
    }

    /**
     * Returns if value is power of two
     * @param value - value to be checked
     * @return - is power of two
     */
    private static boolean isPowerOfTwo(int value){
        return ((value & -value) == value);
    }

    /**
     * Get nearest lower power of two
     * @param value - number
     * @return - nearest power of two
     */
    private static int getNearestPowerOfTwo(int value){
        int val = 2;
        int prev = 0;
        while ( val < value){
            // shift value left to get next power of two
            prev = val;
            val = val << 1;
        }
        // previous represent lower limit, val represents upper limit
        // get size of intervals
        return prev;
    }

}
