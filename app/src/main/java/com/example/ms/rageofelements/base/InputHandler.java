package com.example.ms.rageofelements.base;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.util.Pair;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.screens.DialogScreen;
import com.example.ms.rageofelements.base.screens.InventoryScreen;
import com.example.ms.rageofelements.base.util.InstantActionButton;
import com.example.ms.rageofelements.character.Adressable;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.character.ECharacterGroup;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.Item;
import com.example.ms.rageofelements.combat.Key;
import com.example.ms.rageofelements.combat.Usable;
import com.example.ms.rageofelements.map.Door;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Renderer;

import static com.example.ms.rageofelements.map.Segment.SEGMENT_HEIGHT;
import static com.example.ms.rageofelements.map.Segment.SEGMENT_WIDTH;
import static com.example.ms.rageofelements.render.Renderer.scaleFactor;

/**
 * Class handling user input
 */
public class InputHandler implements View.OnTouchListener, View.OnDragListener, View.OnLongClickListener {

    private final FragmentManager fragmentManager;
    private Player player;
    private Map map;
    private InstantActionButton[] actionButtons;
    private Context context;
    private float eventX, eventY;
    private AreaManager areaManager;


    /**
     * Creates new instance of input handler for game
     *
     * @param fragmentManager - activity's fragment manager, to allow input handler change fragments on user input
     */
    public InputHandler(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // we don't want to allow player to use items during inactivity (e.g. if was hit)
        // or the input has no meaning while player is dead
        if (!player.isActive() || player.isDead()) {
            return true;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                //transform to normalized coordinates
                // now we have x and y from range to -1f to 1f for each axis
                float normalizedX = (event.getX() / (float) v.getWidth()) * 2 - 1;
                float normalizedY = -((event.getY() / (float) v.getHeight()) * 2 - 1);
                //transform to game coordinates (coordinates after orthographic projection)
                float gameX = normalizedX * scaleFactor * Renderer.getAspectRatio();
                float gameY = normalizedY * scaleFactor;

                // check for doors in area
                Door door = areaManager.getDoorAt(gameX + player.getPosition().getX(), gameY + player.getPosition().getY());
                if (door != null) {
                    if (player.getPosition() == door.getAccessPoint()) {
                        eventX = event.getX();
                        eventY = event.getY();
                        Toast.makeText(context, GameActivity.getAppResources().getString(R.string.doors_approached), Toast.LENGTH_SHORT).show();
                        return false;
                    } else {
                        this.setDestination(door.getAccessPoint());
                        return true;
                    }

                }

                synchronized (areaManager) {
                    for (Character character : areaManager.getCharacters()) {
                        if (character.inBoundary(gameX + player.getPosition().getX(), gameY + player.getPosition().getY())) {

                            if (Map.inReach(player.getPosition(),character.getPosition(),1)) {
                                if (character instanceof Adressable && this.isFriendly(character)) {
                                    this.adressCharacter((Adressable) character);
                                } else if (!isFriendly(character)) {
//                                    perform close combat attack
                                    this.closeCombat(character);
                                }
                                return true;
                            } else {
                                this.setDestination(character.getPosition());
                            }

                            return true;
                        }
                    }
                }
                // recalculate game coordinates to segment coordinates (revert isometric transformation)
                float indexX = -(gameX / SEGMENT_WIDTH + gameY / SEGMENT_HEIGHT);
                float indexY = -(gameY / SEGMENT_HEIGHT - gameX / SEGMENT_WIDTH);
                // remove decimal part of index to obtain map coordinates of touched segment
                // calculation suppose that we are on segment 0,0 so add position index to indexes
                int x = (int) indexX + player.getPosition().getXIndex();
                int y = (int) indexY + player.getPosition().getYIndex();
                this.setDestination(map.getSegment(x, y));
                return true;
            default:
                break;
        }
        return false;
    }

    private boolean isFriendly(Character character) {
        return character.getGroup() == ECharacterGroup.player_group || character.getGroup() == ECharacterGroup.neutral;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        // we don't want to allow player to use items during inactivity (e.g. if was hit)
        // or the input has no meaning while player is dead
        if (!player.isActive() || player.isDead()) {
            return true;
        }
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                return true;
            case DragEvent.ACTION_DROP:
                this.use(v, event);
                return true;

        }
        return true;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }


    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * initiate dialog with character
     *
     * @param adressable - character concerned in dialog
     */
    private void adressCharacter(Adressable adressable) {
        adressable.adress(areaManager.getDataManager());
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_layout, new DialogScreen(), adressable.getName());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void openInventory(){
        if (Player.getInstance() == null || Player.getInstance().getPosition() == null)
            return;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_layout, new InventoryScreen(), Constants.inventoryScreen);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setActionButtons(InstantActionButton[] actionButtons) {
        this.actionButtons = actionButtons;
    }

    private void closeCombat(Character target) {
        player.setDirection(target.getPosition());
        player.setActivity(EActivities.ATTACK);
        player.switchAnimation(false);
        Pair<EDamage, Float> playerAttack = player.getBasickAttack();
        player.setTimeOut(Player.ATTACK_TIME);
        player.setActive(false);
        target.hit(playerAttack.first, playerAttack.second);
        if (target.isDead()){
            player.getAbilities().addExperience(target.getAbilities().getKillExperience());
        }
    }

    private void setDestination(Segment destination) {
        if (destination == null)
            return;
        synchronized (player) {
            if (destination == player.getPosition() && !destination.getItemList().isEmpty()){
                this.openInventory();
                return;
            }
            if (Map.inReach(destination,player.getPosition(),1)
                    && destination.getMultiSegmentObject() instanceof Usable){
                Usable usable = (Usable) destination.getMultiSegmentObject();
                usable.onUse(player,destination);
                return;
            }
            player.setActivity(EActivities.RUNNING);
            player.setDestination(destination);
            player.switchAnimation(false);
        }

    }

    private void use(View v, DragEvent event) {
//        perform active event action
        // now we have x and y from range to -1f to 1f for each axis
        float normalizedX = (event.getX() / (float) v.getWidth()) * 2 - 1;
        float normalizedY = -((event.getY() / (float) v.getHeight()) * 2 - 1);
        //transform to game coordinates (coordinates after orthographic projection)
        float gameX = normalizedX * scaleFactor * Renderer.getAspectRatio();
        float gameY = normalizedY * scaleFactor;
        // recalculate game coordinates to segment coordinates (revert isometric transformation)
        float indexX = -(gameX / SEGMENT_WIDTH + gameY / SEGMENT_HEIGHT);
        float indexY = -(gameY / SEGMENT_HEIGHT - gameX / SEGMENT_WIDTH);
        // remove decimal part of index to obtain map coordinates of touched segment
        // calculation suppose that we are on segment 0,0 so add position index to indexes
        int x = (int) indexX + player.getPosition().getXIndex();
        int y = (int) indexY + player.getPosition().getYIndex();

        Segment target = map.getSegment(x, y);
        // get usable object from view
        int index = Integer.valueOf(event.getClipData().getItemAt(0).getText().toString());
        Usable usable = this.actionButtons[index].getUsable();
        if (usable == null) {
            return;
        }

        // we want to removed used object from inventory if it was item
        if (usable instanceof Item) {
            player.getInventory().remove(usable);
            for (InstantActionButton actionButton : actionButtons) {
                // remove usable from controls
                if (actionButton.getUsable() == usable) {
                    actionButton.setUsabe(null);
                }
            }
        }

        // add effects to game (it could be projectiles, graphical effects or even characters)
        GameEntity[] results = usable.onUse(player, target);
        if (results != null) {
            for (GameEntity result : results) {
                areaManager.addEntity(result);
            }
        }

    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public boolean onLongClick(View v) {

        // now we have x and y from range to -1f to 1f for each axis
        float normalizedX = (eventX / (float) v.getWidth()) * 2 - 1;
        float normalizedY = -((eventY / (float) v.getHeight()) * 2 - 1);
        //transform to game coordinates (coordinates after orthographic projection)
        float gameX = normalizedX * scaleFactor * Renderer.getAspectRatio();
        float gameY = normalizedY * scaleFactor;

        // check for doors in area
        Door door = areaManager.getDoorAt(gameX + player.getPosition().getX(), gameY + player.getPosition().getY());

        if (door != null) {
            boolean accessGranted = !door.isLocked();

            if (door.isLocked()) {
                // if doors are locked try all available keys to open it
                for (Item item : player.getInventory()) {
                    if (item instanceof Key) {
                        Key key = (Key) item;
                        accessGranted = door.unlock(key);
                        if (accessGranted) {
                            break;
                        }
                    }
                }
            }
            if (!accessGranted) {
                Toast.makeText(v.getContext(), R.string.this_doors_are_locked, Toast.LENGTH_SHORT).show();
                return true;
            }

            if (player.getPosition() == door.getAccessPoint()) {
                door.entered(player);
                return true;
            }
        }

        return false;
    }

    void setAreaManager(AreaManager areaManager) {
        this.areaManager = areaManager;
    }
}
