package com.example.ms.rageofelements.base;

import android.graphics.Point;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.character.Roshanak;
import com.example.ms.rageofelements.character.Vampires;
import com.example.ms.rageofelements.character.strategy.RoshanakStrategy;
import com.example.ms.rageofelements.character.strategy.VampiresStrategy;
import com.example.ms.rageofelements.map.Door;
import com.example.ms.rageofelements.map.EArea;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.MainArea;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.OnEnterListener;
import com.example.ms.rageofelements.map.Pathfinder;
import com.example.ms.rageofelements.map.RoshanakHousehold;
import com.example.ms.rageofelements.map.VampireTower;
import com.example.ms.rageofelements.map.VampireTowerObject;
import com.example.ms.rageofelements.quests.Quest;
import com.example.ms.rageofelements.quests.QuestManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Miroslav Simasek
 * Class handling game areas management
 */
public class AreaManager implements OnEnterListener {

    private final Player player;
    private final GameManager gameManager;
    //Game areas
    private final HashMap<EArea, GameArea> areas;
    private GameArea current;
    private final QuestManager questManager;
    /**
     * Creates new area manager
     *
     * @param playerType - type of player that was selected on selection screen
     * @param gameManager - game manager this instance is rgistered in
     */
    AreaManager(String playerType, GameManager gameManager) {
        this.gameManager = gameManager;
        this.areas = new HashMap<>();
        //main area must be initialized specially because of player creation other areas are independent
        //Main area must be created first because it creates Player instance !!!
        current = new MainArea(playerType, this);
        this.questManager = new QuestManager(this);
//        current.addEntity(player);
        areas.put(EArea.VAMPIRE_WOODS, current);
        Pathfinder.setArea(current.getMap().getSegments());
        //init other areas
        areas.put(EArea.ROSHANAK_HOUSEHOLD, new RoshanakHousehold(this));
        areas.put(EArea.VAMPIRE_TOWER, new VampireTower(this));

        this.initSpecialEntities();
        this.questManager.init();
        this.connectAreas();
        this.player = Player.getInstance();

    }

    private void connectAreas() {
        // create conection points for areas
        this.createDoorPair(EArea.VAMPIRE_WOODS, EArea.ROSHANAK_HOUSEHOLD, new Point(38, 9), new Point(0, 7));
        this.createDoorPair(EArea.VAMPIRE_WOODS, EArea.VAMPIRE_TOWER, new Point(10, 45), new Point(0, 15));
    }

    private void initSpecialEntities() {
        // init special characters adjust them
        GameArea area = areas.get(EArea.ROSHANAK_HOUSEHOLD);
        Roshanak roshanak = new Roshanak(area.getMap().getSegment(7, 7));

        Quest quest = questManager.getQuestInstance(R.string.quest_get_roshanaks_work);
        roshanak.addObserver(quest);

        roshanak.setStrategyManager(new RoshanakStrategy(roshanak, area));
        roshanak.initDialog(questManager.getQuestInstance(R.string.Dialog_visitNortheastForest));
        area.addEntity(roshanak);
        // register this character as quest character
        questManager.addQuestEntity(roshanak);

        area = areas.get(EArea.VAMPIRE_TOWER);

        Vampires vampires = new Vampires(area.getMap().getSegment(15, 15));
        vampires.setStrategyManager(new VampiresStrategy(vampires,area));
        vampires.initDialog(questManager.getQuestInstance(R.string.quest_kill_the_summoner));
        area.addEntity(vampires);
        // register this character as quest character
        questManager.addQuestEntity(vampires);

        area = areas.get(EArea.VAMPIRE_WOODS);
        // add buildings related to quest from this area
        for (GameEntity entity : area.getMap().getBuildings()) {
            if (entity instanceof VampireTowerObject){
                questManager.addQuestEntity(entity);
            }
        }

    }

    public void sendNotification(String message){
        gameManager.displayNotification(message);
    }


    public void update() {
        // update current area
        current.update();
        //check quest status
        questManager.checkQuests();
    }

    /**
     * Add game entity to current area
     * @param gameEntity - entity
     */
    public void addEntity(GameEntity gameEntity) {
        current.addEntity(gameEntity);
        gameManager.sendEntityToDisplay(gameEntity);
    }

    /**
     * Get areaCharacters
     * @return - list of characters
     */
    ArrayList<Character> getCharacters() {
        return current.getCharacters();
    }

    /**
     * Get current area
     * @return - current GameArea
     */
    GameArea getCurrent() {
        return current;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public void onEnter(Door door, Character character) {

            // remove character rom previous area
            this.current.removeCharacter(character);
            // get targeted area
            GameArea newArea = this.areas.get(door.getDestination());
            // set position of character to new area segment
            character.setPosition(door.getTargetPoint());
            // add character to area
            newArea.addEntity(character);

            // notify observers about character entering new area
            character.setObservableChanged(true);
            character.notifyObservers(newArea);

            // if player entered new area we need to change current area
            if (character instanceof Player) {
                // switch area
                // notify old area about replace
                this.current.onReplace();
                // set new area as current area
                this.current = newArea;
                Pathfinder.setArea(current.getMap().getSegments());
                // push new area map to be displayed
                gameManager.adjustAreaChange(current.getMap());
                //init new area
                this.current.onSet();
            }


    }

    Door getDoorAt(float gameX, float gameY) {
        for (Door door : current.getDoors()) {
            if (door.inBoundary(gameX, gameY)) {
                return door;
            }
        }

        return null;
    }

    private void createDoorPair(EArea first, EArea second, Point pFirst, Point pSecond) {
//        create doors and put the into appropriate area
        // get created areas
        GameArea area1 = areas.get(first);
        GameArea area2 = areas.get(second);
        // get maps from areas
        Map map1 = area1.getMap();
        Map map2 = area2.getMap();
        //create door from first area to second
        Door door1 = new Door(second, map1.getSegment(pFirst.x, pFirst.y), map1.getSegment(pFirst.x, pFirst.y));
        door1.setInner(first.isInner());
        //create door from second area to first
        Door door2 = new Door(first, map2.getSegment(pSecond.x, pSecond.y), map2.getSegment(pSecond.x, pSecond.y));
        door2.setInner(second.isInner());
        //set doors destination points
        door1.connectTo(door2);
        door2.connectTo(door1);
        // add doors to areas
        area1.addDoor(door1);
        area2.addDoor(door2);
        // set area manager to hear door enter
        door1.setOnEnterListener(this);
        door2.setOnEnterListener(this);
        // set door locks
        door1.setLock(second.isDefaultLock());
        door2.setLock(first.isDefaultLock());

    }

    void onPause(){
        current.onReplace();
    }

    void onResume(){
        current.onSet();
    }

    QuestManager getQuestManager() {
        return questManager;
    }

    public DataManager getDataManager() {
        return gameManager.getDataManager();
    }

    /**
     * Send entities to game thread for graphics initialization
     * @param entities - entities to be adjusted
     */
    public void sendToDisplay(GameEntity... entities) {
        gameManager.sendEntityToDisplay(entities);
    }

    public void finishGame() {
        this.gameManager.finishGame();
    }
}
