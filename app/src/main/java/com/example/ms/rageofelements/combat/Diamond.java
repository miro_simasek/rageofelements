package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;

import java.util.Random;

/**
 * Created by MS on 4/18/2017.
 */

public class Diamond extends Item {

    public Diamond() {
        super(R.drawable.light_air_fire_1,
                10 * new Random().nextInt(30),
                GameActivity.getAppResources().getString(R.string.diamond));
    }

}
