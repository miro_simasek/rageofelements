package com.example.ms.rageofelements.base.util;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.Messages;
import com.example.ms.rageofelements.quests.Quest;

/**
 * Created by Miroslav Simasek
 *  Adapter to display quest list
 */
public class QuestAdapter extends ArrayAdapter<Quest> {

    public QuestAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Button v = (Button) super.getView(position, convertView, parent);
        Quest item = getItem(position);
        if (item != null) {
            v.setText(item.getName());
        }
        v.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.ButtonBg));
        final int pos = position;
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View root = v.getRootView();
                v.setBackgroundColor(Color.DKGRAY);
                TextView desc = (TextView) root.findViewById(R.id.quest_description);
                Quest q = getItem(pos);
                if (q != null) {
                    desc.setText(q.getDescription());
                } else {
                    desc.setText(Messages.noQuest);
                }
            }
        });

        return v;
    }
}
