package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.Map;

/**
 * Created by Miroslav Simasek
 *
 */

public abstract class Strategy {

    private final Map area;
    private Character owner;
    private long delay;
    private long delayStart;
    private Character target;

    Strategy(Character character, Map area) {
        this.owner = character;
        delayStart = System.currentTimeMillis();
        this.area = area;
    }

    public abstract void execute();

    public abstract void onActivityChange(EActivities activities);

    void setTarget(Character target){
        this.target = target;
    }

    boolean checkTime(){
        return System.currentTimeMillis() - delayStart < delay;
    }

    public Map getArea() {
        return area;
    }

    public Character getOwner() {
        return owner;
    }

    long getDelay() {
        return delay;
    }

    void setDelay(long delay) {
        this.delay = delay;
    }

    void setDelayStart() {
        this.delayStart = System.currentTimeMillis();
    }

    Character getTarget() {
        return target;
    }

}
