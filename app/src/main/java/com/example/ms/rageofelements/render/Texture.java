package com.example.ms.rageofelements.render;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.HashMap;

/**
 * Created by Miroslav Simasek
 *
 */
public class Texture{

    public static final float TEXTURE_WIDTH = 1.0f;

    // texture coordinates for tags in gl buffers
    private static HashMap<String, Integer> texturePositions = new HashMap<>();
    private int glTextureData = -1;
    private String tag;

    public Texture(float[] textureData, String tag) {
        if (texturePositions.get(tag) == null) {
            final int[] buffers = new int[1];

            // generate texture and vertex buffer
            GLES20.glGenBuffers(buffers.length, buffers, 0);
            glTextureData = buffers[0];
            //      prepare texture buffer data
            FloatBuffer textureBuffer = ByteBuffer.allocateDirect(textureData.length * Renderer.bytesPerFloat)
                    .order(ByteOrder.nativeOrder())
                    .asFloatBuffer();
            textureBuffer.put(textureData);
            textureBuffer.position(0);
            //        send data for texture buffer to gpu
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, glTextureData);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, textureBuffer.capacity() * Renderer.bytesPerFloat, textureBuffer, GLES20.GL_STATIC_DRAW);
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
            texturePositions.put(tag, glTextureData);
        } else {
            glTextureData = texturePositions.get(tag);
        }
        this.tag = tag;

    }


    int getGlTextureData() {
        return glTextureData;
    }

    public String getTag() {
        return tag;
    }

    static void disposeAll() {

        int[] toDispose = new int[texturePositions.size()];
        int i = 0;
        for (Integer integer : texturePositions.values()) {
            toDispose[i] = integer;
            i++;
        }

        GLES20.glDeleteBuffers(toDispose.length,toDispose,0);
        texturePositions.clear();

    }

}
