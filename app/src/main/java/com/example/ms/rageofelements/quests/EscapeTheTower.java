package com.example.ms.rageofelements.quests;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.map.EArea;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.map.TowerRuins;
import com.example.ms.rageofelements.map.VampireTowerObject;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 */

class EscapeTheTower extends AbstractQuest {

    private VampireTowerObject tower;
    private GameArea area;

    EscapeTheTower(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {
        Player.getInstance().deleteObserver(this);
        Player.getInstance().getAbilities().addExperience(300);
        tower.setActive(false);
        // replace building with ruins
        Segment[][] location = tower.getArea();
        TowerRuins ruins = new TowerRuins(location);
        area.addEntity(ruins);
        area.requestEntityDisplay(ruins);
        area.getMap().addBuilding(ruins);
        // remove all doors to area
        area.disconnectFromArea(EArea.VAMPIRE_TOWER);
        // clear area and tower attributes
        area = null;
        tower = null;
    }

    @Override
    public void onAccept() {
        Player.getInstance().addObserver(this);
        for (GameEntity entity : getManager().getQuestEntities()) {
            if (entity instanceof  VampireTowerObject){
                this.tower = (VampireTowerObject) entity;
            }
        }

        this.setNext(getManager().getQuestInstance(R.string.quest_speak_to_roshanak));
    }


    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.quest_escape_the_tower);
    }

    @Override
    public String getDescription() {
        return GameActivity.getTextFromResources(R.string.quest_escape_the_tower_text);
    }

    @Override
    public void update(Observable o, Object arg) {
        if ( o instanceof  Player && arg instanceof GameArea){
            GameArea area = (GameArea) arg;
            if (area.getName() == EArea.VAMPIRE_WOODS){
                this.area = area;
                this.setCompleted(true);
            }
        }
    }
}
