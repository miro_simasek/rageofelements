package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.base.AreaManager;

/**
 * Area of specific building interior (Roshanak's Hut)
 */
public class RoshanakHousehold extends GameArea {

    public RoshanakHousehold(AreaManager areaManager) {
        super(new Map(20,15),areaManager,EArea.ROSHANAK_HOUSEHOLD);
        for (Segment[] segments : map.getSegments()) {
            for (Segment segment : segments) {
                segment.setBackgroundType(ESegmentType.LAMINATE_FLOORING_1);
            }
        }
        map.getSegment(1,0).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(2,0).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(3,0).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(4,0).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(3,1).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(4,1).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(1,14).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(2,14).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(3,14).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(0,13).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(1,13).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(3,13).setForegroundType(ESegmentType.barrel_1);

        map.getSegment(19,13).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(19,12).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(19,11).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(19,10).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(19,9).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(19,8).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(19,7).setForegroundType(ESegmentType.barrel_1);

        map.getSegment(18,13).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(18,11).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(18,10).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(18,8).setForegroundType(ESegmentType.barrel_1);
        map.getSegment(18,7).setForegroundType(ESegmentType.barrel_1);


    }

    @Override
    public Segment getInitialPoint() {
        return map.getSegment(1,7);
    }
}
