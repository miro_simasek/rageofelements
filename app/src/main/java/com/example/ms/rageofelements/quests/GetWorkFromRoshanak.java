package com.example.ms.rageofelements.quests;

import android.util.Pair;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.character.Roshanak;

import java.util.Observable;

class GetWorkFromRoshanak extends AbstractQuest {

    private final String name;

    GetWorkFromRoshanak(QuestManager manager) {
        super(manager);
        this.name = GameActivity.getAppResources().getString(R.string.quest_get_roshanaks_work);
    }

    @Override
    public void onCompletion() {
        Player.getInstance().getAbilities().addExperience(20);
    }

    @Override
    public void onAccept() {

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return GameActivity.getAppResources().getString(R.string.quest_get_roshanaks_work_desc);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Roshanak && arg instanceof Pair) {
            Pair pair = (Pair) arg;

            if (pair.second instanceof VisitNortheastForest) {
                Roshanak roshanak = (Roshanak) o;
                roshanak.setGoldValue(roshanak.getGoldValue() + 200);
                this.setCompleted(true);
            }
        }
    }
}
