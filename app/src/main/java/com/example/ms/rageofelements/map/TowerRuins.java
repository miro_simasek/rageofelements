package com.example.ms.rageofelements.map;

import android.content.res.Resources;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;

/**
 * Created by Miroslav Simasek
 */

public class TowerRuins extends MultisegmentObject {

    /**
     * Creates new multi-segment object
     *
     * @param area            - occupied area
     */
    public TowerRuins(Segment[][] area) {
        super(area, "tower_ruins", R.drawable.broken_tower);
        Resources resources = GameActivity.getAppResources();
        this.setImageHeight(resources.getDimension(R.dimen.ruin_height));
        this.setImageWidth(resources.getDimension(R.dimen.ruin_width));
        this.setMarginBottom(resources.getDimension(R.dimen.ruin_margin_bottom));
    }


}
