package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.Abilities;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.character.NPC;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Miroslav Simasek
 *
 */

public class FireProjectileStrategy extends Strategy {

    private static final long CASTING_DELAY = 1500;
    private static final long SHOOTING_DELAY = 750;
    private static final long WAITING_TIME = 2000;
    private final int minDistance;
    private final int maxDistance;
    private boolean used;
    private final ArrayList<Skill> skills;
    private final GameArea gameArea;

    FireProjectileStrategy(NPC character, GameArea area) {
        super(character, area.getMap());
        this.gameArea = area;
        minDistance = 3;
        maxDistance = 7;
        used = false;
        skills = character.getSkills();
    }

    @Override
    public void execute() {
        if (this.checkTime())
            return;
        Character target = this.getTarget();
        Character owner = this.getOwner();
        Map area = this.getArea();
        if (owner.getActivity() != EActivities.STANDING ){
            return;
        }
        Segment center = target.getPosition();
        if (!used && (Math.abs(owner.getPosition().getXIndex()
                - center.getXIndex()) <= maxDistance
                && Math.abs(owner.getPosition().getYIndex()
                - center.getYIndex()) <= maxDistance)) {
//            get random skill
            Random gen = new Random(System.nanoTime());
            Skill selected = skills.get(gen.nextInt(skills.size()));
            Abilities abilities = owner.getAbilities();
            if (abilities.getMana() >= selected.getManaCost()){
                abilities.addMana((int) -selected.getManaCost());
                GameEntity[] entities = selected.onUse(owner,target.getPosition());

                for (GameEntity entity : entities) {
                    this.gameArea.addEntity(entity);
                    this.gameArea.requestEntityDisplay(entity);
                }
                this.used = true;
            }

        } else {
            this.used = false;
            owner.setActivity(EActivities.RUNNING);
            Random gen = new Random(System.nanoTime());
            int x = gen.nextInt(maxDistance) + minDistance;
            x *= gen.nextBoolean() ? 1 : -1;
            int y = gen.nextInt(maxDistance) + minDistance;
            y *= gen.nextBoolean() ? 1 : -1;
            center = area.getSegment(center.getXIndex() + x, center.getYIndex() + y);
            owner.setDestination(center);
            owner.switchAnimation(false);

        }

    }

    @Override
    public void onActivityChange(EActivities activities) {
        long delay = 1000;
        switch (activities) {
            case CASTING:
                delay = CASTING_DELAY;
                break;
            case SHOOTING:
                delay = SHOOTING_DELAY;
                break;
            case STANDING:
                delay = WAITING_TIME;
                break;

        }
        this.setDelay(delay);
        this.setDelayStart();
//        used = (activities == EActivities.CASTING || activities == EActivities.SHOOTING);
    }

}
