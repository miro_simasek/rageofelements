package com.example.ms.rageofelements.render;

/**
 * Created by MS on 2/9/2017.
 */

public class AnimationData implements Cloneable{

    private static final float X_MIN = 0.0f;
    static final float X_MAX = 1.0f;
//    static final float Y_MIN = 0.0f;
    static final float Y_MAX = 1.0f;

    public static final float X_RIGHT_TO_LEFT = -1.0f;
    public static final float X_LEFT_TO_RIGHT = 1.0f;
    public static final float Y_BOTTOM_TO_TOP = 1.0f;
    public static final float Y_TOP_TO_BOTTOM = -1.0f;

    private final long durationInMs;
    private final boolean single;
    private SpriteSheet spriteSheet;
    private float beginningX, beginningY, endX, endY;
    private float xDirection;
    private float yDirection;
//    private long frameDuration;
    private final long displayFrames;

    /**
     * Create new AnimationData based on sprite-sheet
     * initialize values for default parameter
     * start frame coordinates - x = 0 , y = 0
     * end frame coordinates - x = max sprite-sheet x , y = max sprite-sheet y
     * Initial directions :
     *         X = Left to Right
     *         Y = Bottom to Top
     * @param durationInMs - animation duration in milliseconds
     * @param single - if animation should run as cycle
     * @param spritesheet - sprite-sheet for animation
     */
    public AnimationData(long durationInMs,long displayFrames, boolean single, SpriteSheet spritesheet) {
        this.durationInMs = durationInMs;
        this.single = single;
        this.spriteSheet = spritesheet;
        this.setStart(0, 0);
        this.setEnd(spritesheet.getWidth()-1, spritesheet.getHeight()-1);
        this.xDirection = X_LEFT_TO_RIGHT;
        this.yDirection = Y_BOTTOM_TO_TOP;
        this.displayFrames = displayFrames;
    }

    /**
     * Constructor to create same animation data for different sprite-sheet
     *
     * @param animationData - template
     * @param spriteSheet   - new sprite-sheet object
     */
    public AnimationData(AnimationData animationData, SpriteSheet spriteSheet) {
        this.durationInMs = animationData.getDuration();
        this.single = animationData.isSingle();
        this.spriteSheet = spriteSheet;
        this.xDirection = animationData.getxDirection();
        this.yDirection = animationData.getyDirection();
        this.beginningX = animationData.getBeginningX();
        this.beginningY = animationData.getBeginningY();
        this.endX = animationData.getEndX();
        this.endY = animationData.getEndY();
        this.displayFrames = animationData.getDisplayFrames();

    }


    SpriteSheet getSpriteSheet() {
        return spriteSheet;
    }

    private long getDuration() {
        return durationInMs;
    }


    boolean isSingle() {
        return single;
    }


    float getxDirection() {
        return xDirection;
    }

    float getyDirection() {
        return yDirection;
    }

    float getBeginningX() {
        return beginningX;
    }

    float getBeginningY() {
        return beginningY;
    }

    private float getEndX() {
        return endX;
    }

    private float getEndY() {
        return endY;
    }

    /**
     * Check if point passed through x maximum/minimum for this animation
     * according to direction
     *
     * @param posX -x coordinates
     * @param posY -y coordinates
     * @return - passed through border
     */
    public boolean xBorderReached(float posX, float posY) {
        if (xDirection == X_RIGHT_TO_LEFT) {
            if (yDirection == Y_BOTTOM_TO_TOP) {
                if (posY == endY) {
                    return posX <= endX;
                } else {
                    return posX <= X_MIN;
                }
            } else {
                if (posY == endY) {
                    return posX <= endX;
                } else {
                    return posX <= X_MIN;
                }
            }
        } else {
            if (yDirection == Y_BOTTOM_TO_TOP) {
                if (posY == endY) {
                    return posX + this.spriteSheet.getFrameWidth() > endX;
                } else {
//                  assume right not left point
                    return posX + this.spriteSheet.getFrameWidth() > X_MAX;
                }
            } else {
                if (posY == endY) {
                    return posX + this.spriteSheet.getFrameWidth() > endX;
                } else {
//                    assume right not left point
                   return posX + this.spriteSheet.getFrameWidth() > X_MAX;
                }
            }
        }
    }

    /**
     * Check if point passed through y maximum/minimum for this animation
     * According to animation direction
     * @param posX - x coordinates
     * @param posY - y coordinates
     * @return - passed through border
     */
    public boolean yBorderReached(float posX, float posY) {
        return yDirection == Y_BOTTOM_TO_TOP ? posY > endY :
//              if top to bottom assume bottom point
                posY /*- spriteSheet.getFrameHeight()*/ < endY;

    }

    /**
     * Set first frame of animation
     * Frames between start and end will be rendered by rows according to directions
     * @param x - index of sprite-sheet frame starting from 0
     * @param y - index of sprite-sheet frame starting from 0
     */
    public void setStart(int x, int y) {

        beginningX = spriteSheet.getFrameWidth() * (float) (x);
        beginningY = spriteSheet.getFrameHeight() * (float) (y + 1);

    }

    /**
     * Set last frame of animation
     * Frames between start and end will be rendered by rows according to directions
     * @param x - index of sprite-sheet frame starting from 0
     * @param y - index of sprite-sheet frame starting from 0
     */
    public void setEnd(int x, int y) {

        endX = spriteSheet.getFrameWidth() * (float) (x);
        endY = spriteSheet.getFrameHeight() * (float) (y + 1);

    }

    /**
     * Set x and y direction for frames creating animation
     * Use one of the Animation data constants
     *
     * @param dx - direction X Constants: X_RIGHT_TO_LEFT , X_LEFT_TO_RIGHT
     * @param dy - direction Y Constants: Y_TOP_TO_BOTTOM , Y_BOTTOM_TO_TOP
     */
    public void setDirection(float dx, float dy) {
        this.xDirection = dx;
        this.yDirection = dy;
    }


    private void setSpriteSheet(SpriteSheet spriteSheet){
           this.spriteSheet = spriteSheet;
    }

    public AnimationData copy(int resId){
        AnimationData result = null;
        try {
            result = (AnimationData) this.clone();
            result.setSpriteSheet(new SpriteSheet(spriteSheet,resId));
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public AnimationData cloneData(){
        AnimationData result = null;
        try {
            result = (AnimationData) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public long getFrameDuration() {
        return durationInMs / displayFrames ;
    }

    public long getDisplayFrames() {
        return displayFrames;
    }
}
