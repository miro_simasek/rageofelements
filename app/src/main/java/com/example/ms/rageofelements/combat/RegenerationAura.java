package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by Miroslav Simasek
 */

public class RegenerationAura extends DefensiveSkill {

    public RegenerationAura() {
        super(GameActivity.getAppResources().getString(R.string.regeneration_aura), R.drawable.runes_royal_1);
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        this.setOwner(character);
        if (character.getActiveDefense() instanceof RegenerationAura) {
            this.disposeEffect();
            character.setActiveDefense(null);
            return null;
        } else {
            if (character.getActiveDefense()!= null){
                character.getActiveDefense().disposeEffect();
            }

            character.setActiveDefense(this);
            RegenerationAuraEntity auraEntity = new RegenerationAuraEntity(character,getLevel());
            setRelatedEffect(auraEntity);
            this.adjustCharacter(character,segment[0]);
            return new GameEntity[]{auraEntity};
        }
    }

    @Override
    public EActivities getRelatedActivity() {
        return EActivities.CASTING;
    }

    @Override
    public float filterDamage(EDamage damageType, float value) {
        // regeneration aura wont filter damage
        return value;
    }

    @Override
    public int getIcon() {
        switch (level) {
            case 2:
                return R.drawable.runes_royal_2;
            case 3:
                return R.drawable.runes_royal_3;
            default:
                return R.drawable.runes_royal_1;
        }
    }
}
