package com.example.ms.rageofelements.quests;

import android.util.Pair;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.character.Roshanak;
import com.example.ms.rageofelements.map.GameEntity;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 */
class ReportBackToRoshanak extends AbstractQuest {

    private Roshanak roshanak;
    ReportBackToRoshanak(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {
        Player player =Player.getInstance();
        roshanak.setGoldValue(roshanak.getGoldValue()+500);
        roshanak.deleteObserver(this);
        player.getAbilities().addExperience(300);
        // add increase points
        player.getAbilities().addIncreasePoints(4);
        // increase dexterity as reward
        player.getAbilities().addDexterity();
        // increase concentration as reward
        player.getAbilities().addConcentration();
        // increase vitality as reward
        player.getAbilities().addVitality();
        // increase strength as reward
        player.getAbilities().addStrenth();
        roshanak = null;
    }

    @Override
    public void onAccept() {
        for (GameEntity character : getManager().getQuestEntities()) {
            if (character instanceof  Roshanak){
                roshanak = (Roshanak) character;
                String title = GameActivity.getTextFromResources(R.string.quest_report_to_roshanak);
                String text = GameActivity.getTextFromResources(R.string.report_to_roshanak_text);
                Quest quest = getManager().getQuestInstance(R.string.quest_enter_the_tower);
                roshanak.addDialogData(title, text, quest);
                roshanak.addObserver(this);
            }
        }
    }

    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.quest_report_to_roshanak);
    }

    @Override
    public String getDescription() {
        return GameActivity.getTextFromResources(R.string.quest_report_to_roshanak_text);
    }

    @Override
    public void update(Observable o, Object arg) {
        if ( o instanceof  Roshanak && arg instanceof Pair){
            Pair pair = (Pair) arg;
            this.setCompleted(pair.second instanceof EnterTheTower);

        }
    }
}
