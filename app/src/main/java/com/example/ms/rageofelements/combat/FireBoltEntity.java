package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;

import static com.example.ms.rageofelements.combat.FireBallEntity.fireballTag;

/**
 * Created by Miroslav Simasek
 *
 */

class FireBoltEntity extends Projectile {

    private static final float imageYfit = (Segment.SEGMENT_WIDTH - Segment.SEGMENT_HEIGHT) / 2f;

    FireBoltEntity(Character owner, float damageVal, EDirection direction) {
        super(owner, 2.5f, direction);
        this.getDamages().put(EDamage.FIRE, damageVal);
    }

    @Override
    public void initGraphics() {
        if (position != null) {
            Shape shape = new Shape(createShapeData(), fireballTag);
            SpriteSheet spriteSheet = new SpriteSheet(8, 8, fireballTag, R.drawable.fireball);
            AnimationData animationData = new AnimationData(2000,8, false, spriteSheet);
            int row = getRow(getBaseDirection());
            animationData.setStart(7, row);
            animationData.setEnd(0, row);
            animationData.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_BOTTOM_TO_TOP);
            animation = new Animation<>(shape);
            animation.putData(fireballTag, getBaseDirection(), animationData);
            animation.switchAnimationByKey(fireballTag, getBaseDirection());
            animation.translateModelMatrix(position.getX(), position.getY());
        }
    }


    private float[] createShapeData(){
        return new float[]{
                -Segment.HALF_SEGMENT_WIDTH, Segment.SEGMENT_WIDTH - imageYfit,
                -Segment.HALF_SEGMENT_WIDTH, -imageYfit,
                Segment.HALF_SEGMENT_WIDTH, Segment.SEGMENT_WIDTH - imageYfit,
                Segment.HALF_SEGMENT_WIDTH, -imageYfit};
    }

}
