package com.example.ms.rageofelements.map;

/**
 * Created by MS on 2/5/2017.
 */

enum ESegmentType {

    empty_segment(false), green_tree1(true), LAMINATE_FLOORING_1(false), barrel_1(true), stone_floor_1(false);

    public final boolean obstacle;

    ESegmentType(boolean obstacle){
        this.obstacle = obstacle;
    }
}
