package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.Segment;


public abstract class Skill implements Usable {

    private final String name;
    int level;
    private int maxLevel = -1;
    float manaCost;
    private int icon;

    public Skill(String name, int resource) {
        this.name = name;
        this.icon = resource;
    }

    public float getManaCost() {
        return manaCost;
    }

    public String getName() {
        return name;
    }

    public int getIcon() {
        return icon;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        for (int i = this.level; i < level; i++) {
            this.increaseLevel();
        }

    }

    void adjustCharacter(Character character, Segment target){
        character.setActivity(getRelatedActivity());
        character.setDirection(target);
        character.setTimeOut(Character.MAGIC_TIME_OUT);
        character.switchAnimation(false);
        character.setActive(false);

    }

    void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public void increaseLevel(){
        if (level < maxLevel || maxLevel == -1){
            level++;
        }
    }

    public abstract EActivities getRelatedActivity();

    public boolean increasable(){
        //skill is increasable if current level is less then max level or no max level is specified
        return level < maxLevel || maxLevel == -1 ;
    }

    int getMaxLevel() {
        return maxLevel;
    }
}
