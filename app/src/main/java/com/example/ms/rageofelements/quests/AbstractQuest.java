package com.example.ms.rageofelements.quests;

/**
 * Abstract class for quests
 */

abstract class AbstractQuest implements Quest {

    private final QuestManager manager;
    private Quest next;
    private boolean completed;

    AbstractQuest(QuestManager manager) {
        this.manager = manager;
        this.completed = false;
    }

    public Quest getNext() {
        return next;
    }

    void setNext(Quest next) {
        this.next = next;
    }

    public QuestManager getManager() {
        return manager;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

}
