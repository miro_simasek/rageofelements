package com.example.ms.rageofelements.quests;


import java.util.Observer;

public interface Quest extends Observer{

    void onCompletion();
    void onAccept();
    String getName();
    String getDescription();
    Quest getNext();
    boolean isCompleted();
}
