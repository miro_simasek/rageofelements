package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.util.QuestAdapter;
import com.example.ms.rageofelements.quests.QuestManager;

/**
 * Created by Miroslav Simasek
 * Fragment to display list of quests
 */
public class QuestBookScreen extends Fragment {

    private DataManager dataManager;
    private QuestAdapter adapter;
    private QuestManager questManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = ((GameActivity)getActivity()).getDataManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.quest_book_layout, container, false);
        ListView questList = (ListView) v.findViewById(R.id.quest_list);
        adapter = new QuestAdapter(v.getContext(),R.layout.button_template);
        questList.setAdapter(adapter);
        RadioButton rb = (RadioButton) v.findViewById(R.id.active_selection);
        this.questManager = dataManager.getQuestManager();

        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.clear();
                adapter.addAll(questManager.getActiveQuests());
                adapter.notifyDataSetChanged();
            }
        });
//        init active quests by default
        rb.callOnClick();
        rb.setChecked(true);

        rb = (RadioButton) v.findViewById(R.id.completed_selection);
        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.clear();
                adapter.addAll(questManager.getCompletedQuests());
                adapter.notifyDataSetChanged();
            }
        });

        return v;
    }
}
