package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by MS on 3/25/2017.
 */

public interface Usable {

    /**
     *
     * @param character - character using object
//     * @param controller - object responsible for further entity management
     * @param segment - segments related to object use
     * @return - effect that will be displayed on game
     */
    GameEntity[] onUse(Character character, Segment... segment);

    /**
     * Get icon Representation of Object
     * @return - resource id of icon
     */
    int getIcon();
}
