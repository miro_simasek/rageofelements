package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ms.rageofelements.R;

/**
 * Created by Miroslav Simasek
 * Screen to display credits with attributions
 */
public class CreditsScreen extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.credits_layout, container, false);

    }



}
