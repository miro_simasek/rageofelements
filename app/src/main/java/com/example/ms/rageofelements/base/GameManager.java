package com.example.ms.rageofelements.base;

import android.opengl.GLSurfaceView;
import android.widget.Toast;

import com.example.ms.rageofelements.base.util.GamePlayView;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.render.Renderer;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Miroslav Simasek
 *
 */
public class GameManager implements Observer{

    private InputHandler inputHandler;
    // data manager transferring data between game-play screens (fragments)
    private DataManager dataManager;
    // view the game will render to
    private GamePlayView glSurfaceView;
    // renderer
    private Renderer renderer;
    //player character
    private Player player;
    //flag if data was initially uploaded to data manager
    private boolean dataUploaded;
    private AreaManager areaManager;

    public GameManager(String playerType, DataManager dataManager,
                       InputHandler inputHandler) {

        this.dataManager = dataManager;
        this.areaManager = new AreaManager(playerType, this);
        this.player = Player.getInstance();
        this.player.getAbilities().addObserver(this);
        this.dataUploaded = false;
        this.inputHandler = inputHandler;
        this.inputHandler.setAreaManager(areaManager);
        this.dataManager.setQuestManager(areaManager.getQuestManager());
        this.dataManager.setPlayer(player);
    }


    public void adjustView(GamePlayView view) {
        this.glSurfaceView = view;
        this.inputHandler.setPlayer(player);
        this.inputHandler.setMap(areaManager.getCurrent().getMap());
        this.inputHandler.setContext(glSurfaceView.getContext());

        // set input handler to game-play view to obtain events
        glSurfaceView.setOnTouchListener(inputHandler);
        glSurfaceView.setOnDragListener(inputHandler);
        glSurfaceView.setOnLongClickListener(inputHandler);
        glSurfaceView.setEGLContextClientVersion(2);

        //configuration for Android emulator
        glSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);


        if (renderer == null) {
            renderer = new Renderer(glSurfaceView.getContext());
        }
        glSurfaceView.setRenderer(renderer);
        glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        glSurfaceView.initGame(areaManager.getCurrent().getMap(), player);
    }

    public void onResume() {
        if (dataUploaded) {
            this.fetchData();
        }
        this.areaManager.onResume();
    }

    public void onPause() {
        this.uploadData();
        this.areaManager.onPause();
    }

    /**
     * Update all GameEntities directly influencing game
     * -some entities can be purely graphical so there will be no need to update them
     * e.g. environment objects like Tree etc.
     */
    public void update() {

        areaManager.update();

        if (!player.movementFetched()) {
            glSurfaceView.queueEvent(new Runnable() {
                @Override
                public void run() {
//                    renderer.setProjectionTranslation(player.getPosition().getX(),player.getPosition().getY());
                    renderer.translateProjectionMatrix(-player.fetchMovedX(), player.fetchMovedY());
                }
            });
        }
        glSurfaceView.requestRender();
    }

    /**
     * set data to data manager
     */
    private void uploadData() {
        if (player.getPosition() != null) {
            dataManager.setSegmentItems(player.getPosition().getItemList());// get items from ground
            dataUploaded = true;
        }
    }

    /**
     * fetch data from data manager
     */
    private void fetchData() {
        if (player.getPosition() != null) {
            player.getPosition().setItems(dataManager.getSegmentItems());
        }
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    synchronized void sendEntityToDisplay(GameEntity... result) {
        // set new entity for graphics initialization
        final GameEntity[] res = result;
        glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                glSurfaceView.setForDisplay(res);
            }
        });

    }

    public InputHandler getInputHandler() {
        return inputHandler;
    }


    void adjustAreaChange(final Map map) {
        inputHandler.setMap(map);

        this.glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                glSurfaceView.initGame(map, player);
                glSurfaceView.setForDisplay(map);
                glSurfaceView.getRenderer().initGameDisplay(map, player);
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof String){
            this.displayNotification((String)arg);
        }
    }

    /**
     * Display notification text for user
     * @param arg - message text
     */
    void displayNotification(String arg) {
        final String message = arg;
        glSurfaceView.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(glSurfaceView.getRootView().getContext(),message,Toast.LENGTH_SHORT).show();
            }
        });

    }

    /**
     * finish game
     */
    void finishGame(){
        glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                glSurfaceView.finisGame();
            }
        });
    }


}
