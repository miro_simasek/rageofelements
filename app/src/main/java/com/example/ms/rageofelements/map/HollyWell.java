package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.combat.Usable;

/**
 * Created by Miroslav Simasek
 */

class HollyWell extends MultisegmentObject implements Usable {

    private float cumulatedHealth;
    private float cumulatedMana;
    private float maxLife;
    private float maxMana;
    private long lastRegen;

    /**
     * Creates new multi-segment object
     *
     * @param area            - occupied area
     */
    HollyWell(Segment[][] area) {
        super(area, GameActivity.getTextFromResources(R.string.holly_well), R.drawable.well);
        this.setImageHeight(R.dimen.well_height);
        this.setImageWidth(R.dimen.well_width);
//        this.setMarginLeft(-20);
//        this.setMarginRight(-20);
//        this.setMarginBottom(30);
        cumulatedHealth = 50;
        cumulatedMana = 50;
        maxLife = 200;
        maxMana = 200;
        lastRegen = System.currentTimeMillis();
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        character.getAbilities().addMana(cumulatedMana);
        character.getAbilities().addHealth(cumulatedHealth);
        this.clearProgress();
        return null;
    }

    private synchronized void clearProgress() {
        // update will go from game thread
        // and onUse is called from GUI thread
        cumulatedHealth = 0f;
        cumulatedMana = 0f;
    }

    private synchronized void regen(){
        // increase mana
        if (cumulatedMana <= maxMana){
            cumulatedMana += 0.1f;
        }
        // increase health
        if (cumulatedHealth <= maxLife){
            cumulatedHealth += 0.1f;
        }
    }


    @Override
    public int getIcon() {
        // no need for icon in building
        return 0;
    }

    @Override
    public void update() {
        long elapsed = System.currentTimeMillis() - lastRegen;
        if (elapsed > 100) {
            this.lastRegen = System.currentTimeMillis();
            this.regen();
        }
    }
}
