package com.example.ms.rageofelements.character;

public enum EActivities {

    STANDING, RUNNING, ATTACK, CASTING, DEATH, HIT, SHOOTING

}
