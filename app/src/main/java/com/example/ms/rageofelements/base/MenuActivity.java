package com.example.ms.rageofelements.base;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.screens.MainMenu;


/**
 * Created by MS on 1/28/2017.
 */

public class MenuActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
//      Set initial activity fragment
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.main_layout, new MainMenu(), Constants.mainMenuTag);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0){
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
