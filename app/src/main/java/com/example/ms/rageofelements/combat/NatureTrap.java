package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Abilities;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;

public class NatureTrap extends Skill {

    private float manaCost;
    private float baseDamage;

    public NatureTrap() {
        super(GameActivity.getAppResources().getString(R.string.nature_trap), R.drawable.vines_acid_1);
        this.setMaxLevel(4);
        baseDamage = 20f;
        manaCost = 15f;
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        if (segment == null || segment.length == 0 || segment[0] == null) {
            return null;
        }
        Abilities abilities = character.getAbilities();
        if (abilities.getMana() < manaCost)
            return null;

        if (segment[0].isObstacle() && segment[0].getCharacter() == null){
            return null;
        }
        TrapEntity[] entity = new TrapEntity[1];

        entity[0] = new TrapEntity(character, baseDamage);
        entity[0].setPosition(segment[0]);
        abilities.addMana((int) -manaCost);
        this.adjustCharacter(character,segment[0]);
        return entity;
    }

    @Override
    public EActivities getRelatedActivity() {
        return EActivities.CASTING;
    }

    @Override
    public void increaseLevel() {
        if (level >= getMaxLevel())
            return;
        super.increaseLevel();
        manaCost += 10f;
        baseDamage += 5f;
    }

    @Override
    public int getIcon() {
        if (level < 2) {
            return R.drawable.vines_acid_1;
        } else if (level < 4) {
            return R.drawable.vines_acid_2;
        } else {
            return R.drawable.vines_acid_3;
        }
    }
}
