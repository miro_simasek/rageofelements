package com.example.ms.rageofelements.render;

import android.content.Context;

import static android.opengl.GLES20.glUseProgram;



class ShaderProgram implements ShaderConstants {


    //shader program
    final int program;

    ShaderProgram(Context context, int vertexShaderResourceID, int fragmentShaderResourceID){

        //compile the shaders and link program

        program = ShaderCompiler.buildProgram(
                ShaderSourceReader.readTextFromResource(context,vertexShaderResourceID),
                ShaderSourceReader.readTextFromResource(context,fragmentShaderResourceID));

    }

    void useProgram(){
        //Set the current OpenGL shader program to this program
        glUseProgram(program);
    }


}
