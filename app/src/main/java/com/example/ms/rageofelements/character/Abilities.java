package com.example.ms.rageofelements.character;


import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;

import java.util.Observable;

public class Abilities extends Observable{

    private static final float LEVELING_INDEX = 1.2f;
    private static final int INITIAL_REQUIREMENTS = 1000;
    private final static float VITALITY_FACTOR = 3.0f;
    private final static float CONCENTRATION_FACTOR = 5.0f;
    private int level = 0;
    private int experience;
    private int levelRequirements = INITIAL_REQUIREMENTS;
    private int pointToIncrease = 1;
    private int skillPoints = 1;
    private float strength, dexterity, vitality, concentration;
    private float maxHelth, health, maxMana, mana;
    private float healthRegen = 1.0f;
    private float manaRegen = 1.0f;
    private int killExperience;

    public void addHealth(float value) {
        if (this.health + value > maxHelth) {
            this.health = maxHelth;
            return;
        }
        this.health += value;
    }

    public void addMana(float value) {
        if (this.mana + value > maxMana) {
            this.mana = maxMana;
            return;
        }
        this.mana += value;
    }

    public void addExperience(int value) {
        this.experience += value;
        if (experience > this.levelRequirements) {
            level++;
            skillPoints++;
            pointToIncrease += 3;
            levelRequirements = (int) ((float) levelRequirements * Abilities.LEVELING_INDEX);
            this.setChanged();
            this.notifyObservers(GameActivity.getTextFromResources(R.string.next_level_reached));
        }
    }

    public void addStrenth() {
        if (pointToIncrease > 0) {
            strength++;
            pointToIncrease--;
        }
    }

    public void addVitality() {
        if (pointToIncrease > 0) {
            vitality++;
            maxHelth +=  VITALITY_FACTOR;
            healthRegen = 1.0f + vitality / 100f; // vitality increase regeneration factor by one percent
            pointToIncrease--;
        }
    }

    public void addDexterity() {
        if (pointToIncrease > 0) {
            dexterity++;
            pointToIncrease--;
        }
    }

    public void addConcentration() {
        if (pointToIncrease > 0) {
            concentration++;
            maxMana = concentration * CONCENTRATION_FACTOR;
            manaRegen = 1.0f + concentration / 100f; // concentration increase mana regen by one percent
            pointToIncrease--;
        }
    }

    public int getExperience() {
        return experience;
    }

    public int getLevelRequirements() {
        return levelRequirements;
    }

    public int getPointToIncrease() {
        return pointToIncrease;
    }

    public int getSkillPoints() {
        return skillPoints;
    }

    public float getStrength() {
        return strength;
    }

    void setStrength(float strength) {
        this.strength = strength;
    }

    public float getDexterity() {
        return dexterity;
    }

    void setDexterity(float dexterity) {
        this.dexterity = dexterity;
    }

    public float getVitality() {
        return vitality;
    }

    void setVitality(float vitality) {
        this.vitality = vitality;
        this.maxHelth += vitality * VITALITY_FACTOR;
        this.health = maxHelth;
        healthRegen = 1.0f + vitality / 100f;
    }

    public float getConcentration() {
        return concentration;
    }

    void setConcentration(float concentration) {
        this.concentration = concentration;
        this.maxMana = concentration * CONCENTRATION_FACTOR;
        this.mana = maxMana;
        manaRegen = 1.0f + concentration / 100f; // concentration increase mana regen by one percent
    }

    float getMaxHelth() {
        return maxHelth;
    }

    void setMaxHelth(float maxHelth) {
        this.maxHelth = maxHelth;
        this.health = maxHelth;
    }

    public float getHealth() {
        return health;
    }
    void setMaxMana(float value){
        maxMana = value;
        mana = maxMana;
    }

    float getMaxMana() {
        return maxMana;
    }

    public float getMana() {
        return mana;
    }

    public float getHealthRegen() {
        return healthRegen;
    }

    public int getLevel() {
        return level;
    }

    public void useSkillPoint() {
        skillPoints--;
    }

    public int getKillExperience() {
        return killExperience;
    }

    void setKillExperience(int killExperience) {
        this.killExperience = killExperience;
    }

    public float getManaRegen() {
        return manaRegen;
    }

    public void addIncreasePoints(int value){
        this.pointToIncrease += value;
    }
}
