package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.ECharacterGroup;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.TextureShader;

import java.util.HashMap;

/**
 * Created by MS on 3/17/2017.
 */

public abstract class Projectile extends GameEntity {

    private final EDirection baseDirection;
    protected Animation<String, EDirection> animation;
    private float speed;
    private HashMap<EDamage, Float> damages;
    protected final Character owner;
    private float directionX, directionY;
    private float centerX, centerY;


    static float[] getProjectileDirection(Projectile start, Segment target) {
        float[] directions = new float[2];
//        to refer array attributes more nicely
        final int x = 0;
        final int y = 1;
//      init points for calculations
        float[] startPoint = {start.getCenterX(), start.getCenterY()};
        float[] endPoint = {target.getX(), target.getY() - Segment.HALF_SEGMENT_HEIGHT};

//      calculate distance between two pints
        float distance = (float) Math.sqrt(Math.pow(endPoint[x] - startPoint[x], 2) + Math.pow(endPoint[y] - startPoint[y], 2));

        float dx = (endPoint[x] - startPoint[x]) / distance;
        float dy = (endPoint[y] - startPoint[y]) / distance;

        directions[x] = dx;
        directions[y] = dy;

        return directions;
    }


    Projectile(Character owner, float speed, EDirection baseDirection) {
        super(owner.getPosition());
        this.baseDirection = baseDirection;
        centerX = position.getX();
        centerY = position.getY() - Segment.HALF_SEGMENT_HEIGHT;
        damages = new HashMap<>();
        this.owner = owner;
        this.speed = speed;
    }

    @Override
    public void setPosition(Segment position) {
        super.setPosition(position);
        centerX = position.getX();
        centerY = position.getY() - Segment.HALF_SEGMENT_HEIGHT;
    }


    @Override
    public void update() {
        this.move();
    }

    private void move() {
        float movementX, movementY;
        movementX = directionX * speed;
        movementY = directionY * speed;
        centerX += movementX;
        centerY += movementY;
        boundaryX += movementX;
        boundaryY += movementY;

        if (animation != null) {
//            animation.translateModelMatrix(movementX,movementY);
            animation.moveAnimation(movementX, movementY);
        }
    }


    public void setSegment(Segment segment) {
        if (segment == null) {
            this.setActive(false);
            return;
        }
        if (segment.getCharacter() == owner){
            return;
        }
        if (segment.isObstacle()) {
            position.removeEntity(this);
            segment.addEntity(this);
            position = segment;
            this.onShot();
            this.setActive(false);
            return;
        }
        position.removeEntity(this);
        position = segment;
        segment.addEntity(this);
    }

    public void onShot() {
        Character character = position.getCharacter();
        if (character != null &&
                (character.getGroup() != owner.getGroup() && character.getGroup() != ECharacterGroup.neutral)) {
            for (EDamage damage : damages.keySet()) {
                character.hit(damage, damages.get(damage));
            }
            if (character.isDead()){
                owner.getAbilities().addExperience(character.getAbilities().getKillExperience());
            }

        }
    }

    void setDirections(float x, float y) {
        directionX = x;
        directionY = y;
    }


    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        if (animation != null) {
            animation.draw(matrix, shaderProgram);
        }
    }

    int getRow(EDirection direction){
        switch (direction){
            case SOUTH_WEST:
                return 7;
            case WEST:
                return 6;
            case NORTH_WEST:
                return 5;
            case NORTH:
                return 4;
            case NORTH_EAST:
                return 3;
            case EAST:
                return 2;
            case SOUTH_EAST:
                return 1;
            default:
                return 0;

        }
    }

    EDirection getBaseDirection() {
        return baseDirection;
    }

    public float getCenterX() {
        return centerX;
    }

    public float getCenterY() {
        return centerY;
    }

    @Override
    public void updateAnimation() {
        if (animation!= null){
            animation.update();
        }
    }

    public HashMap<EDamage, Float> getDamages() {
        return damages;
    }
}
