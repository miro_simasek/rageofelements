package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;

import java.util.HashMap;

/**
 * Class representing basic class for defensive category of skills
 * this skills can be attached to player to provide som effect
 */
public abstract class DefensiveSkill extends Skill {

    private final HashMap<EDamage,Float> damageResistance;
    private GameEntity relatedEffect;
    private Character owner;

    DefensiveSkill(String name, int resource) {
        super(name, resource);
        damageResistance = new HashMap<>();
    }

    /**
     * Set character the skill will be attached to
     * @param owner - character
     */
    public void setOwner(Character owner) {
        this.owner = owner;
    }

    /**
     * Filter damage. This method is called if character is using some defensive skill
     * and when is hit.
     * @param damageType - type of damage
     * @param value - value of damage
     * @return - damage after filtering
     */
    public float filterDamage(EDamage damageType, float value){
        if (damageResistance.get(damageType) != null){
            float newValue = value - damageResistance.get(damageType);
            return newValue < 0.0f ? 0f : newValue;
        }
        return value;
    }

    /**
     * Add type of damage and max value the skill could be able to filter
     * @param damageType - type of damage
     * @param value - value
     */
    void putResistence(EDamage damageType, float value){
        if (damageResistance.get(damageType) == null){
        damageResistance.put(damageType,value);
        } else {
            damageResistance.remove(damageType);
            damageResistance.put(damageType,value);
        }
    }

    /**
     * Set effect related to usage of this skill
     * @param relatedEffect - effect
     */
    void setRelatedEffect(GameEntity relatedEffect) {
        this.relatedEffect = relatedEffect;
    }

    /**
     * Dispose entity related to this skill
     *
     */
    public void disposeEffect(){
        this.relatedEffect.setTimeOut(0);
        this.relatedEffect.setActive(false);
        this.relatedEffect = null;
    }

    /**
     * If character's position was set, this function is called
     * we need to also set new position for related entity
     * @param position - new position
     */
    public void setPosition(Segment position) {
        GameEntity effect = relatedEffect;
        if (position == null){
            this.disposeEffect();
            return;
        }
        if (effect!= null){
            effect.setPosition(position);
        }
    }

    /**
     * Get owner this skill is related to
     * @return - owner
     */
    public Character getOwner() {
        return owner;
    }
}
