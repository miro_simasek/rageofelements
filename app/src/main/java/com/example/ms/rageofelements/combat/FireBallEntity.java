package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;

/**
 * Created by Miroslav Simasek
 * Class representing special type of projectile
 */
class FireBallEntity extends Projectile {

    static final String fireballTag = "fireball";
    private static final float imageYfit = (Segment.SEGMENT_WIDTH - Segment.SEGMENT_HEIGHT) / 2f;
    private static final long TIME_OUT = 1000;

    private static final float speed = 3f;
    private static final String explosion = "red_explosion";


    FireBallEntity(Character owner, float damageVal, EDirection direction) {
        super(owner, speed, direction);
        this.getDamages().put(EDamage.FIRE, damageVal);
        this.getDamages().put(EDamage.MAGIC, damageVal / 2f);

    }

    @Override
    public void onShot() {
        super.onShot();
        if (animation != null) {
            animation.switchAnimationByKey(explosion, getBaseDirection());
        }
        this.setTimeOut(TIME_OUT);
    }

    @Override
    public void initGraphics() {
        if (position != null) {
            Shape shape = new Shape(createShapeData(), fireballTag);
            SpriteSheet spriteSheet = new SpriteSheet(8, 8, fireballTag, R.drawable.fireball);
            AnimationData animationData = new AnimationData(2000,8, false, spriteSheet);
            int row = this.getRow(getBaseDirection());
            animationData.setStart(7, row);
            animationData.setEnd(0, row);
            animationData.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_BOTTOM_TO_TOP);
            animation = new Animation<>(shape);
            animation.putData(fireballTag, getBaseDirection(), animationData);

            animationData = new AnimationData(TIME_OUT,13, true, new SpriteSheet(13, 1, explosion, R.drawable.red_explosion));
            animationData.setStart(0, 0);
            animationData.setEnd(12, 0);
            animationData.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_BOTTOM_TO_TOP);
            animation.putData(explosion, getBaseDirection(), animationData);
            animation.switchAnimationByKey(fireballTag, getBaseDirection());
            animation.translateModelMatrix(position.getX(), position.getY());
        }
    }

    private float[] createShapeData() {
        return new float[]{
                -Segment.HALF_SEGMENT_WIDTH, Segment.SEGMENT_WIDTH - imageYfit,
                -Segment.HALF_SEGMENT_WIDTH, -imageYfit,
                Segment.HALF_SEGMENT_WIDTH, Segment.SEGMENT_WIDTH - imageYfit,
                Segment.HALF_SEGMENT_WIDTH, -imageYfit};
    }

}
