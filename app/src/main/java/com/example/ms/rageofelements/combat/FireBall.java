package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Abilities;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by MS on 3/18/2017.
 */

public class FireBall extends Skill {

    private final float basicMana = 7f;
    private float damage;


    public FireBall() {
        super("Fireball", R.drawable.fireball_red_2);
//        this.group = group;
        this.manaCost = basicMana;
        this.setMaxLevel(4);

    }


    @Override
    public void increaseLevel() {
        if (level >= getMaxLevel())
            return;

        super.increaseLevel();
        float manaIndex = 0.2f;
        this.manaCost = basicMana * (1.0f + manaIndex * (float) level);
        float damageIndex = 15f;
        this.damage = damageIndex * (float) level;

    }

    @Override
    public EActivities getRelatedActivity() {
        return EActivities.CASTING;
    }


    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        if (segment == null || segment.length == 0 || segment[0] == null) {
            return null;
        }
        Abilities abilities = character.getAbilities();
        if (abilities.getMana() < manaCost)
            return null;

        FireBallEntity entity = new FireBallEntity(character, this.damage,
                Map.getDirection(character.getPosition(),segment[0]));

        float[] directions;
        directions = Projectile.getProjectileDirection(entity, segment[0]);

        entity.setDirections(directions[0], directions[1]);
        abilities.addMana((int) -manaCost);
        this.adjustCharacter(character,segment[0]);
        return new GameEntity[]{entity};
    }

    @Override
    public int getIcon() {
        switch (level) {
            case 4:
                return R.drawable.fireball_red_3;
            default:
                return R.drawable.fireball_red_2;
        }
    }
}
