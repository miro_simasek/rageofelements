package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.base.AreaManager;

/**
 * Created by Miroslav Simasek
 * Area representing vampire tower
 */

public class VampireTower extends GameArea {

    public VampireTower(AreaManager areaManager) {
        super(new Map(30,30),areaManager,EArea.VAMPIRE_TOWER);
        for (Segment[] segments : map.getSegments()) {
            for (Segment segment : segments) {
                segment.setBackgroundType(ESegmentType.stone_floor_1);
            }
        }

    }

    @Override
    public Segment getInitialPoint() {
        return map.getSegment(1,15);
    }
}
