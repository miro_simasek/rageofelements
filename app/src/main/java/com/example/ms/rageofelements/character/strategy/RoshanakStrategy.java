package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.NPC;
import com.example.ms.rageofelements.map.GameArea;

/**
 * Created by MS on 4/24/2017.
 */

public class RoshanakStrategy extends StrategyManager {


    public RoshanakStrategy(NPC owner, GameArea area) {
        super(owner, area.getMap());
        this.setCurrent(new WalkingStrategy(owner,area.getMap()));
    }

    @Override
    protected void check() {

    }
}
