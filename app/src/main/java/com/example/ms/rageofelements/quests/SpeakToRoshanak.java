package com.example.ms.rageofelements.quests;

import android.util.Pair;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Roshanak;
import com.example.ms.rageofelements.map.GameEntity;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 */

class SpeakToRoshanak extends AbstractQuest {

    SpeakToRoshanak(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {
        getManager().getAreaManager().finishGame();
    }

    @Override
    public void onAccept() {
        for (GameEntity character : getManager().getQuestEntities()) {
            if (character instanceof Roshanak){
                Roshanak roshanak = (Roshanak) character;
                String title = GameActivity.getTextFromResources(R.string.vampires_is_dead);
                String text = GameActivity.getTextFromResources(R.string.vampires_is_dead_text);
                Quest quest = getManager().getQuestInstance(R.string.quest_finish_game);
                roshanak.addDialogData(title, text, quest);
                roshanak.addObserver(this);
            }
        }
    }

    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.quest_speak_to_roshanak);
    }

    @Override
    public String getDescription() {
        return GameActivity.getTextFromResources(R.string.quest_speak_to_roshanak_text);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Roshanak && arg instanceof Pair) {
            Pair pair = (Pair) arg;
            if (pair.second instanceof FinishTheGame) {
                this.setCompleted(true);
            }
        }

    }
}
