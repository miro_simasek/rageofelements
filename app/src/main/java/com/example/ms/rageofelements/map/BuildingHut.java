package com.example.ms.rageofelements.map;

import android.content.res.Resources;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;

/**
 * Created by MS on 3/22/2017.
 */

class BuildingHut extends MultisegmentObject {

    BuildingHut(Segment[][] area) {
        super(area, "hut", R.drawable.building_hut );
        Resources resources = GameActivity.getAppResources();
        this.setImageHeight(resources.getDimension(R.dimen.hut_height));
        this.setImageWidth(resources.getDimension(R.dimen.hut_width));
        this.setMarginLeft(resources.getDimension(R.dimen.hut_margin_left));
        this.setMarginRight(resources.getDimension(R.dimen.hut_margin_right));
        this.setMarginBottom(resources.getDimension(R.dimen.hut_margin_bottom));

//        Segment doorPosition = area[area.length/2][area[0].length-1];
//        this.setBoundary();
    }
}
