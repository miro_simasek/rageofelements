package com.example.ms.rageofelements.combat;

/**
 * Created by MS on 3/17/2017.
 */

public enum EDamage {

    FIRE, ICE, SHOCK, SHARP, BLUNT, MAGIC

}
