package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.ECharacterGroup;
import com.example.ms.rageofelements.character.NPC;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.Map;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 */

public class SkeletonArcherStrategy extends StrategyManager {

    private FireProjectileStrategy fireProjectileStrategy;
    private static final int VIEW_DISTANCE = 8;
    private WalkingStrategy walkingStrategy;
    private ArrayList<Character> characters;

    public SkeletonArcherStrategy(NPC owner, GameArea gameArea) {
        super(owner, gameArea.getMap());
        characters = gameArea.getCharacters();
        fireProjectileStrategy = new FireProjectileStrategy(owner,gameArea);
        walkingStrategy = new WalkingStrategy(owner,gameArea.getMap());
        setCurrent(this.walkingStrategy);
    }

    @Override
    protected void check() {
        Character owner = this.getOwner();
        for (Character character : characters) {
            ECharacterGroup group = character.getGroup();
            if (group == ECharacterGroup.neutral || group == owner.getGroup()) {
                continue;
            }
            if (Map.inReach(owner.getPosition(), character.getPosition(), VIEW_DISTANCE)) {
                if (character.isDead())
                    continue;
                fireProjectileStrategy.setTarget(character);
                setCurrent(fireProjectileStrategy);
                return;
            }
        }
        setCurrent(walkingStrategy);
    }
}
