package com.example.ms.rageofelements.character;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.util.Node;
import com.example.ms.rageofelements.combat.Archery;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.MultiShot;
import com.example.ms.rageofelements.combat.NatureTrap;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureData;

/**
 * Created by MS on 2/9/2017.
 */
class PRanger extends Player {
    PRanger(Segment position) {
        super(position);
        this.initSkillTree();
        this.initAbilities();
        this.setBasickAttack(EDamage.BLUNT, 15);
    }

    private void initSkillTree() {
        Archery skill = new Archery();
        skill.setLevel(1);

        Node<Skill> bow = new Node<Skill>(skill);
        bow.setNodeName(GameActivity.getTextFromResources(R.string.bow_category));
        getSkillRoots().add(bow);

        Node<Skill> multishot = new Node<Skill>(new MultiShot());
        bow.addChild(multishot);

        NatureTrap natureTrap = new NatureTrap();
        Node<Skill> nature = new Node<Skill>(natureTrap);
        nature.setNodeName(GameActivity.getTextFromResources(R.string.nature_skills));
        getSkillRoots().add(nature);
    }

    @Override
    protected void initAbilities() {
        abilities.setMaxHelth(170);
        abilities.setConcentration(8);
        abilities.setVitality(12);
        abilities.setDexterity(15);
        abilities.setStrength(10);
    }

    @Override
    public void initGraphics() {
        super.initGraphics();
        Shape shape = new Shape(Character.getCharacterData(0.8f,1.0f), this.getClass().getSimpleName());
        animation = new Animation<>(shape);

        this.initStanding();
        this.initAttacking();
        this.initRunning();
        this.initHit();
        this.initCasting();
        this.initShooting();
        this.initDeath();
        this.animation.switchAnimationByKey(EActivities.STANDING, EDirection.NORTH);


    }

    private void initDeath() {

        SpriteSheet death = new SpriteSheet(8, 8, TextureData.RANGER_DEATH, R.drawable.ranger_death);
        AnimationData data = new AnimationData(Character.MAGIC_TIME_OUT, 8, false, death);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        animation.putData(EActivities.DEATH, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(EActivities.DEATH, EDirection.NORTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(EActivities.DEATH, EDirection.EAST, data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(EActivities.DEATH, EDirection.SOUTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(EActivities.DEATH, EDirection.NORTH_WEST, data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(EActivities.DEATH, EDirection.WEST, data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(EActivities.DEATH, EDirection.SOUTH_WEST, data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(EActivities.DEATH, EDirection.SOUTH, data);

    }

    private void initShooting() {
        SpriteSheet shooting = new SpriteSheet(8, 8, TextureData.RANGER_SHOOTING, R.drawable.ranger_shooting);
        AnimationData data = new AnimationData(Character.SHOOTING_TIME_OUT, 8, true, shooting);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        animation.putData(EActivities.SHOOTING, EDirection.SOUTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(EActivities.SHOOTING, EDirection.SOUTH_WEST, data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(EActivities.SHOOTING, EDirection.WEST, data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(EActivities.SHOOTING, EDirection.NORTH_WEST, data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(EActivities.SHOOTING, EDirection.SOUTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(EActivities.SHOOTING, EDirection.EAST, data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(EActivities.SHOOTING, EDirection.NORTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(EActivities.SHOOTING, EDirection.NORTH, data);

    }

    private void initCasting() {
        SpriteSheet casting = new SpriteSheet(8, 8, TextureData.RANGER_CASTING, R.drawable.ranger_casting);
        AnimationData data = new AnimationData(Character.MAGIC_TIME_OUT, 8, false, casting);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        animation.putData(EActivities.CASTING, EDirection.SOUTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(EActivities.CASTING, EDirection.SOUTH_WEST, data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(EActivities.CASTING, EDirection.WEST, data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(EActivities.CASTING, EDirection.SOUTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(EActivities.CASTING, EDirection.EAST, data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(EActivities.CASTING, EDirection.NORTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(EActivities.CASTING, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(EActivities.CASTING, EDirection.NORTH_WEST, data);


    }

    private void initHit() {
        SpriteSheet hit  = new SpriteSheet(8, 8, TextureData.RANGER_HIT, R.drawable.ranger_hit);
        AnimationData data = new AnimationData(Character.HIT_TIME_OUT, 8, false, hit);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        animation.putData(EActivities.HIT, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(EActivities.HIT, EDirection.NORTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(EActivities.HIT, EDirection.EAST, data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(EActivities.HIT, EDirection.SOUTH_EAST, data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(EActivities.HIT, EDirection.NORTH_WEST, data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(EActivities.HIT, EDirection.WEST, data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(EActivities.HIT, EDirection.SOUTH_WEST, data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(EActivities.HIT, EDirection.SOUTH, data);

    }

    private void initStanding() {
        SpriteSheet standing = new SpriteSheet(8, 8, TextureData.RANGER_STANDING, R.drawable.ranger_standing);
        AnimationData data = new AnimationData(1500, 8, false, standing);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_BOTTOM_TO_TOP);
        data.setStart(0, 7);
        data.setEnd(7, 7);
        animation.putData(EActivities.STANDING, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(0,6);
        data.setEnd(7,6);
        animation.putData(EActivities.STANDING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,5);
        animation.putData(EActivities.STANDING,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(0,4);
        data.setEnd(7,4);
        animation.putData(EActivities.STANDING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,3);
        animation.putData(EActivities.STANDING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,2);
        data.setEnd(7,2);
        animation.putData(EActivities.STANDING,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,1);
        animation.putData(EActivities.STANDING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,0);
        data.setEnd(7,0);
        animation.putData(EActivities.STANDING,EDirection.SOUTH,data);
}

    private void initAttacking() {
        SpriteSheet standing = new SpriteSheet(8, 8, TextureData.RANGER_ATTACK, R.drawable.ranger_attack);
        AnimationData data = new AnimationData(Character.ATTACK_TIME_OUT, 8, false, standing);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(7, 7);
        data.setEnd(0, 7);
        animation.putData(EActivities.ATTACK, EDirection.NORTH, data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(EActivities.ATTACK,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(EActivities.ATTACK,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH,data);
    }

    private void initRunning() {
//        TextureLoader textureLoader = Renderer.getTextureLoader();
        // this spritesheet is wrong in both directions it seems that character go backwards
        // i double-checked id an after close look it is so
        SpriteSheet spriteSheet1 = new SpriteSheet(8, 8, TextureData.RANGER_RUNNING,
                R.drawable.ranger_running_1);
        AnimationData running = new AnimationData(1000, 16, false, spriteSheet1);
        running.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        running.setStart(0,7);
        running.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_EAST,running);

        running = running.cloneData();
        running.setStart(0,5);
        running.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH,running);

        running = running.cloneData();
        running.setStart(0,3);
        running.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_WEST,running);

        running = running.cloneData();
        running.setStart(0,1);
        running.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.WEST,running);

        SpriteSheet spriteSheet2 = new SpriteSheet(8, 8, TextureData.RANGER_RUNNING,
                R.drawable.ranger_running_2);
        running = new AnimationData(1000, 16, false, spriteSheet2);
        running.setDirection(AnimationData.X_RIGHT_TO_LEFT,AnimationData.Y_BOTTOM_TO_TOP);
        running.setEnd(0,7);
        running.setStart(7,6);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_WEST,running);

        running = running.cloneData();
        running.setEnd(0,5);
        running.setStart(7,4);
        animation.putData(EActivities.RUNNING,EDirection.NORTH,running);

        running = running.cloneData();
        running.setEnd(0,3);
        running.setStart(7,2);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_EAST,running);

        running = running.cloneData();
        running.setEnd(0,1);
        running.setStart(7,0);
        animation.putData(EActivities.RUNNING,EDirection.EAST,running);



    }

}
