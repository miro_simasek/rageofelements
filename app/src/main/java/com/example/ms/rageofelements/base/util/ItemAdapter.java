package com.example.ms.rageofelements.base.util;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.ms.rageofelements.combat.Item;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 * Adapter to display character inventory
 */
public class ItemAdapter extends BaseAdapter {

    private ArrayList<Item> items;
    private Context context;
    private final String tag;

    public ItemAdapter(Context context, String tag) {
        this.context = context;
        this.tag = tag;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getIconSource();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemView item;
//        if (convertView == null) {
            item = new ItemView(context);
            final int pos = position;
            View.OnTouchListener listener = new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()){
                        case MotionEvent.ACTION_DOWN:

                    }
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        ClipData.Item item = new ClipData.Item(String.valueOf(pos));

                        ClipData dragData = new ClipData((String) v.getTag(),
                                new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN},item);
                        dragData.addItem(new ClipData.Item(tag));
                        View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            v.startDragAndDrop(dragData, dragShadowBuilder, null, 0);
                        } else {
                            v.startDrag(dragData, dragShadowBuilder, null, 0);
                        }

                        return true;
                    }
                    return true;
                }
            };
            item.setOnTouchListener(listener);

        item.assignItem(items.get(position));
        return item;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public Item removeItem(int index) {
        if (items.size() == 0){
            return null;
        }
        return items.remove(index);
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public String getTag() {
        return tag;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
}
