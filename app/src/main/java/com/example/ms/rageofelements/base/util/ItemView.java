package com.example.ms.rageofelements.base.util;

import android.content.Context;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.combat.Item;

/**
 * Created by Miroslav Simasek
 * Simple view containig item
 */
public class ItemView extends FrameLayout {

    private ImageView imageView;

    public ItemView(Context context) {
        super(context);
        init();
    }

    private void init(){
        inflate(getContext(), R.layout.inventory_item_template, this);
        this.imageView = (ImageView) findViewById(R.id.item_preview);

    }

    public void assignItem(Item item) {
        imageView.setImageResource(item.getIconSource());
    }

}
