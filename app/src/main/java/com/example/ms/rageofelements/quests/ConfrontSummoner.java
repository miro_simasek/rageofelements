package com.example.ms.rageofelements.quests;

import android.util.Pair;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.ECharacterGroup;
import com.example.ms.rageofelements.character.Vampires;
import com.example.ms.rageofelements.map.GameEntity;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 */

class ConfrontSummoner extends AbstractQuest {

    private Vampires vampires;

    ConfrontSummoner(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {
        vampires.deleteObserver(this);
        vampires.setGroup(ECharacterGroup.undead);
        vampires.createSkeletons();
    }

    @Override
    public void onAccept() {
        for (GameEntity entity : getManager().getQuestEntities()) {
            if (entity instanceof Vampires){
                vampires = (Vampires) entity;
                vampires.addObserver(this);
            }
        }
    }

    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.quest_confront_summoner);
    }

    @Override
    public String getDescription() {
        return GameActivity.getTextFromResources(R.string.quest_confront_summoner_text);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Vampires && arg instanceof Pair) {
            Pair pair = (Pair) arg;
            if (pair.second instanceof KillTheSummoner) {
                this.setCompleted(true);
            }
        }
    }
}
