package com.example.ms.rageofelements.render;

import static com.example.ms.rageofelements.render.AnimationData.X_MAX;

/**
 * Created by Miroslav Simasek
 *
 *  Class representing sprite-sheet texture
 */
public class SpriteSheet extends Texture{

    private final int width, height;
    private int glTexture;

    public SpriteSheet(int width, int height, String tag, int imageRes) {
        super(new float[]{
                0f, 0f,
                0f, 1f / (float) height,
                1f / (float) width, 0f,
                1f / (float) width, 1f / (float) height
        }, tag);

        this.width = width;
        this.height = height;
        this.glTexture = Renderer.getTextureLoader().getTexture(imageRes);

    }

    public SpriteSheet(SpriteSheet spriteSheet, int imageRes) {
        super(null, spriteSheet.getTag()); // data were created with previous sprite-sheet
        this.width = spriteSheet.getWidth();
        this.height = spriteSheet.getHeight();
        this.glTexture = Renderer.getTextureLoader().getTexture(imageRes);

    }

    int getGlTexture() {
        return glTexture;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }

    float getFrameWidth() {
        return X_MAX / (float) width;
    }

    float getFrameHeight() {
        return AnimationData.Y_MAX / (float) height;
    }

}
