package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.character.NPC;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;

import java.util.Random;

/**
 * Created by MS on 4/2/2017.
 */

public class WalkingStrategy extends Strategy {

    private static final int MAX_DELAY_MS = 5000;
    private static final int MIN_DELAY_MS = 3000;

    private Segment[][] origin;

    WalkingStrategy(NPC character, Map area) {
        super(character, area);
        this.setDelay(MAX_DELAY_MS + MIN_DELAY_MS);
        origin = area.getAreaByCenter(character.getPosition(), 5, 5);
    }

    @Override
    public void execute() {
        Character owner = this.getOwner();
        if (owner.getActivity() == EActivities.STANDING ){
            if (this.checkTime()){
                return;
            }
            Random gen = new Random(System.nanoTime());
            int x, y;
            x = gen.nextInt(origin.length);
            y = gen.nextInt(origin[0].length);
            owner.setActivity(EActivities.RUNNING);
            owner.setDestination(origin[x][y]);
            owner.switchAnimation(false);
        }

    }

    @Override
    public void onActivityChange(EActivities activities) {
        if (activities == EActivities.STANDING){
            Character owner  = this.getOwner();
            Random gen = new Random(System.nanoTime());
            long delay;
            delay = gen.nextInt(MAX_DELAY_MS) + MIN_DELAY_MS;
            this.setDelay(delay);
            this.setDelayStart();
            owner.switchAnimation(true);
        }
    }

}
