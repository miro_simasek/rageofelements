package com.example.ms.rageofelements.base.util;

import android.content.Context;
import android.util.AttributeSet;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.combat.Usable;

/**
 * Created by Miroslav Simasek
 *
 */
public class InstantActionButton extends android.support.v7.widget.AppCompatImageView{


    private Usable usable;
    private int position;

    public InstantActionButton(Context context) {
        super(context);
    }

    public InstantActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InstantActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setUsabe(Usable usable) {
        this.usable = usable;
        if (usable == null){
            this.setImageResource(R.drawable.empty);
        } else {
            this.setImageResource(usable.getIcon());
        }

    }

    public Usable getUsable() {
        return usable;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
