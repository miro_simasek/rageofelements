package com.example.ms.rageofelements.character;

import android.util.Pair;

import com.example.ms.rageofelements.quests.Quest;

import java.util.HashMap;

/**
 * Created by Miroslav Simasek
 *
 */
public class DialogData {

    public static final String TRADE = "Trade";

    private final HashMap<String,Pair<String,Quest>> dataSet;
    private final String mainTitle;
    private final Adressable owner;

    DialogData(Adressable owner) {
        dataSet = new HashMap<>();
        this.owner = owner;
        this.mainTitle = owner.getName();
    }

    void addData(String title, String text, Quest relatedQuest){
        dataSet.put(title, new Pair<>(text, relatedQuest));
    }

    public Pair<String,Quest> getDataByTile(String title){
        return dataSet.get(title);
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public Adressable getOwner() {
        return owner;
    }

    public HashMap<String, Pair<String, Quest>> getDataSet() {
        return dataSet;
    }

    public void removeDialogOption(String title) {
        this.dataSet.remove(title);
    }
}
