package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ClipData;
import android.content.ClipDescription;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.Constants;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.GameManager;
import com.example.ms.rageofelements.base.InputHandler;
import com.example.ms.rageofelements.base.util.GamePlayView;
import com.example.ms.rageofelements.base.util.InstantActionButton;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.combat.Usable;

/**
 * Created by Miroslav Simasek
 *
 */
public class GamePlayScreen extends Fragment {

    public static final int activeButtonsCount = 5;
    private static long FPS = 45;
    public static final long MS_PER_UPDATE = 1000 / FPS;

    private Thread gameThread;
    private GamePlayView gamePlayView;
    private ProgressBar loadingAnimation;
    private GameManager gameManager;
    private DataManager dataManager;
    private boolean running = true;
    private InstantActionButton[] activeUsables;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String playerType = getArguments().getString(Constants.characterKey);
        dataManager = ((GameActivity) getActivity()).getDataManager();
        this.gameManager = new GameManager(playerType, dataManager,
                new InputHandler(getActivity().getFragmentManager()));
        activeUsables = new InstantActionButton[activeButtonsCount];
        gameThread = createThread(gameManager);
    }


    private Thread createThread(final GameManager gameManager) {
        return new Thread() {
            @Override
            public void run() {
                super.run();
                long beginning;
                long delta;
                long sleepTime;
                while (running) {

                    beginning = System.currentTimeMillis();
                    gameManager.update();
                    delta = System.currentTimeMillis() - beginning;
                    sleepTime = MS_PER_UPDATE - delta;

                    if (sleepTime > 0) {
                        try {
                            sleep(sleepTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        };
    }


    @Override
    public void onPause() {
        super.onPause();
        gamePlayView.onPause();
        gameManager.onPause();
        // upload data from game control to data manger
        Usable[] usables = dataManager.getActionButtonsContent();
        for (int i = 0; i < activeUsables.length; i++) {
            usables[i] = activeUsables[i].getUsable();
        }
        dataManager.setActionButtonsContent(usables);
        running = false;

    }

    @Override
    public void onResume() {
        super.onResume();
        gameManager.onResume();
        gameManager.getInputHandler().setActionButtons(this.activeUsables);
        this.gamePlayView.onResume();
        gameThread = this.createThread(gameManager);
        running = true;
        loadingAnimation.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gameplay_layout, container, false);
        gamePlayView = (GamePlayView) v.findViewById(R.id.game_play_view);
        gameManager.adjustView(gamePlayView);
        gamePlayView.setParent(this);
        loadingAnimation = (ProgressBar) v.findViewById(R.id.loading_animation);


        this.initGameControls(v);
        this.initNavigationControls(v);
        return v;
    }

    public void startGame() {
        if (!gameThread.isAlive()) {
            gameThread.start();
        }
            loadingAnimation.setVisibility(View.GONE);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (gameThread.isAlive()) {
            gameThread.interrupt();
            gameThread = null;
        }
    }

    /**
     * Init controls for game play
     *
     * @param v - parent view
     */
    private void initGameControls(View v) {
        View.OnTouchListener listener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent me) {

                if (me.getAction() == MotionEvent.ACTION_DOWN) {

                    InstantActionButton view = ((InstantActionButton) v);
                    ClipData.Item item = new ClipData.Item(String.valueOf(view.getPosition()));
                    ClipData dragData =
                            new ClipData((String) v.getTag(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                    View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v);
                    // this statement is deprecated but the next version would require API 24 or higher
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        v.startDragAndDrop(dragData, dragShadowBuilder, null, 0);
                    } else {
                        v.startDrag(dragData, dragShadowBuilder, null, 0);
                    }

                    return true;
                }
                return false;
            }
        };

        Usable[] usables = dataManager.getActionButtonsContent();
        // control buttons for skill an items usage
        activeUsables[0] = (InstantActionButton) v.findViewById(R.id.activeView1);
        activeUsables[1] = (InstantActionButton) v.findViewById(R.id.activeView2);
        activeUsables[2] = (InstantActionButton) v.findViewById(R.id.activeView3);
        activeUsables[3] = (InstantActionButton) v.findViewById(R.id.activeView4);
        activeUsables[4] = (InstantActionButton) v.findViewById(R.id.activeView5);

        // init active buttons properties
        for (int i = 0; i < activeUsables.length; i++) {
            activeUsables[i].setOnTouchListener(listener);
            activeUsables[i].setUsabe(usables[i]);
            activeUsables[i].setPosition(i);
        }
    }


    /**
     * Init controls for screen navigation
     *
     * @param v - parent view
     */
    private void initNavigationControls(View v) {

        // switch to ability overview screen
        Button button = (Button) v.findViewById(R.id.ability_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Player.getInstance() == null || Player.getInstance().getPosition() == null)
                    return;
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, new AbilityScreen(), Constants.abilityScreen);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        //switch to QuestBook screen
        button = (Button) v.findViewById(R.id.quest_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Player.getInstance() == null || Player.getInstance().getPosition() == null)
                    return;
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, new QuestBookScreen(), Constants.questScreen);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        //switch to SkillTree screen
        button = (Button) v.findViewById(R.id.skill_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Player.getInstance() == null || Player.getInstance().getPosition() == null)
                    return;
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, new SkillTreeScreen(), Constants.skillTreeScreen);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        //switch to Inventory screen
        button = (Button) v.findViewById(R.id.inventory_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Player.getInstance() == null || Player.getInstance().getPosition() == null)
                    return;
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, new InventoryScreen(), Constants.inventoryScreen);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
