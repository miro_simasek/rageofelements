package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureData;

/**
 * Created by Miroslav Simasek
 * Map object Tree
 */
class Tree extends com.example.ms.rageofelements.render.VBO {

    private static final String treeTag = "tree";
    private static final float TREE_HEIGHT = Segment.SEGMENT_HEIGHT * 6f;
    private static final float TREE_WIDTH = Segment.SEGMENT_HEIGHT * 4f;
    private static final float TREE_IMAGE_Y_FIT = Segment.SEGMENT_HEIGHT *1.5f;

    private static final float[] treeShape = new float[]{

            - TREE_WIDTH / 2f, TREE_HEIGHT - TREE_IMAGE_Y_FIT,
            - TREE_WIDTH / 2f, - TREE_IMAGE_Y_FIT,
            TREE_WIDTH / 2f, TREE_HEIGHT - TREE_IMAGE_Y_FIT,
            TREE_WIDTH / 2f, - TREE_IMAGE_Y_FIT
    };

    Tree(int textureId) {
        super(new Shape(treeShape, treeTag), new Texture(TextureData.whole, TextureData.WHOLE_TEXTURE));
        this.setTextureID(textureId);
    }
}
