package com.example.ms.rageofelements.character;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.util.Node;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.FireBall;
import com.example.ms.rageofelements.combat.FireBolt;
import com.example.ms.rageofelements.combat.FireStorm;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureData;

/**
 * Created by Miroslav Simasek
 *
 */
class PNecromancer extends Player {

    PNecromancer(Segment position) {
        super(position);
        this.initSkillTree();
        this.initAbilities();
        this.setBasickAttack(EDamage.BLUNT, 10);
    }


    private void initSkillTree() {
        FireBolt fireBolt = new FireBolt();
        fireBolt.setLevel(1);

        Node<Skill> fireRoot = new Node<Skill>(fireBolt);
        fireRoot.setNodeName(GameActivity.getTextFromResources(R.string.fire_category));
        getSkillRoots().add(fireRoot);

        Node<Skill> parent = fireRoot;
        Node<Skill> child = new Node<Skill>(new FireBall());
        parent.addChild(child);

        parent = child;
        child = new Node<Skill>(new FireStorm());
        parent.addChild(child);

    }

    protected void initAbilities() {
        abilities.setMaxHelth(150);
        abilities.setConcentration(15);
        abilities.setVitality(8);
        abilities.setDexterity(5);
        abilities.setStrength(5);
    }

    @Override
    public void initGraphics() {
        super.initGraphics();
//        AnimationData[] dataSet;
        Shape shape = new Shape(Character.getCharacterData(1.0f,0f), this.getClass().getSimpleName());
        animation = new Animation<>(shape);

            //            we don't want the animation to be updated before initialisation finished;
            this.initStanding();
            this.initAttacking();
            this.initRunning();
             this.initHit();
            this.initDeath();
            this.animation.switchAnimationByKey(EActivities.STANDING, EDirection.NORTH);


    }

    private void initStanding() {

//        TextureLoader textureLoader = Renderer.getTextureLoader();
        SpriteSheet spriteSheet = new SpriteSheet(5, 4, TextureData.NECROMANCER_STANDING, R.drawable.necromancer_standing_n);
        AnimationData standing = new AnimationData(2000,20, false, spriteSheet);
        standing.setStart(4, 0);
        standing.setEnd(0, 3);
        standing.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_BOTTOM_TO_TOP);

        animation.putData(EActivities.STANDING, EDirection.NORTH, standing);
        animation.putData(EActivities.STANDING, EDirection.NORTH_EAST,standing.copy(R.drawable.necromancer_standing_ne));
        animation.putData(EActivities.STANDING, EDirection.NORTH_WEST,standing.copy(R.drawable.necromancer_standing_nw));
        animation.putData(EActivities.STANDING, EDirection.EAST,standing.copy(R.drawable.necromancer_standing_e));
        animation.putData(EActivities.STANDING, EDirection.SOUTH_EAST,standing.copy(R.drawable.necromancer_standing_se));
        animation.putData(EActivities.STANDING, EDirection.SOUTH,standing.copy(R.drawable.necromancer_standing_s));
        animation.putData(EActivities.STANDING, EDirection.SOUTH_WEST,standing.copy(R.drawable.necromancer_standing_sw));
        animation.putData(EActivities.STANDING, EDirection.WEST,standing.copy(R.drawable.necromancer_standing_w));
    }

    private void initRunning() {
//        TextureLoader textureLoader = Renderer.getTextureLoader();
        SpriteSheet spriteSheet = new SpriteSheet(8, 4, TextureData.NECROMANCER_RUNNING,
                R.drawable.necromancer_running_n);
        AnimationData running = new AnimationData(1000,30, false, spriteSheet);
        running.setStart(0, 3);
        running.setEnd(5, 0);
        running.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_TOP_TO_BOTTOM);
        animation.putData(EActivities.RUNNING, EDirection.NORTH, running);
        animation.putData(EActivities.RUNNING, EDirection.NORTH_EAST,running.copy(R.drawable.necromancer_running_ne));
        animation.putData(EActivities.RUNNING, EDirection.NORTH_WEST,running.copy(R.drawable.necromancer_running_nw));
        animation.putData(EActivities.RUNNING, EDirection.EAST,running.copy(R.drawable.necromancer_running_e));
        animation.putData(EActivities.RUNNING, EDirection.SOUTH_EAST,running.copy(R.drawable.necromancer_running_se));
        animation.putData(EActivities.RUNNING, EDirection.SOUTH,running.copy(R.drawable.necromancer_running_s));
        animation.putData(EActivities.RUNNING, EDirection.SOUTH_WEST,running.copy(R.drawable.necromancer_running_sw));
        animation.putData(EActivities.RUNNING, EDirection.WEST,running.copy(R.drawable.necromancer_running_w));
    }

    private void initAttacking() {
//        TextureLoader textureLoader = Renderer.getTextureLoader();
        SpriteSheet spriteSheet = new SpriteSheet(8, 4, TextureData.NECROMANCER_ATTACKING, R.drawable.necromancer_attack_n);
        AnimationData attack = new AnimationData(1000,30, true, spriteSheet);
        attack.setStart(5, 0);
        attack.setEnd(0, 3);
        attack.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_BOTTOM_TO_TOP);
        animation.putData(EActivities.ATTACK, EDirection.NORTH, attack);
        animation.putData(EActivities.ATTACK, EDirection.NORTH_EAST,attack.copy(R.drawable.necromancer_attack_ne));
        animation.putData(EActivities.ATTACK, EDirection.NORTH_WEST,attack.copy(R.drawable.necromancer_attack_nw));
        animation.putData(EActivities.ATTACK, EDirection.EAST,attack.copy(R.drawable.necromancer_attack_e));
        animation.putData(EActivities.ATTACK, EDirection.SOUTH_EAST,attack.copy(R.drawable.necromancer_attack_se));
        animation.putData(EActivities.ATTACK, EDirection.SOUTH,attack.copy(R.drawable.necromancer_attack_s));
        animation.putData(EActivities.ATTACK, EDirection.SOUTH_WEST,attack.copy(R.drawable.necromancer_attack_sw));
        animation.putData(EActivities.ATTACK, EDirection.WEST,attack.copy(R.drawable.necromancer_attack_w));
    }

    private void initHit(){

        // we don't have separate sprite-sheet for necromancer casting
        // but the hit looks similar so we will use it also for casting
        SpriteSheet spritesheet = new SpriteSheet(5, 4, TextureData.NECROMANCER_HIT, R.drawable.necromancer_casting_n);
        AnimationData hit = new AnimationData(Character.HIT_TIME_OUT,20, true, spritesheet);
        AnimationData cast = new AnimationData(Character.MAGIC_TIME_OUT,20, true, spritesheet);
        hit.setStart(0, 3);
        hit.setEnd(4, 0);

        hit.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_TOP_TO_BOTTOM);
        cast.setStart(0, 3);
        cast.setEnd(4, 0);
        cast.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_TOP_TO_BOTTOM);

        animation.putData(EActivities.HIT, EDirection.NORTH, hit);
        // the sprite-sheets for necromancer has the same data but use other texture source
        animation.putData(EActivities.HIT, EDirection.NORTH_EAST,hit.copy(R.drawable.necromancer_casting_ne));
        animation.putData(EActivities.HIT, EDirection.NORTH_WEST,hit.copy(R.drawable.necromancer_casting_nw));
        animation.putData(EActivities.HIT, EDirection.EAST,hit.copy(R.drawable.necromancer_casting_e));
        animation.putData(EActivities.HIT, EDirection.SOUTH_EAST,hit.copy(R.drawable.necromancer_casting_se));
        animation.putData(EActivities.HIT, EDirection.SOUTH,hit.copy(R.drawable.necromancer_casting_s));
        animation.putData(EActivities.HIT, EDirection.SOUTH_WEST,hit.copy(R.drawable.necromancer_casting_sw));
        animation.putData(EActivities.HIT, EDirection.WEST,hit.copy(R.drawable.necromancer_casting_w));

        animation.putData(EActivities.CASTING, EDirection.NORTH, cast);
        // the sprite-sheets for necromancer has the same data but use other texture source
        animation.putData(EActivities.CASTING, EDirection.NORTH_EAST,cast.copy(R.drawable.necromancer_casting_ne));
        animation.putData(EActivities.CASTING, EDirection.NORTH_WEST,cast.copy(R.drawable.necromancer_casting_nw));
        animation.putData(EActivities.CASTING, EDirection.EAST,cast.copy(R.drawable.necromancer_casting_e));
        animation.putData(EActivities.CASTING, EDirection.SOUTH_EAST,cast.copy(R.drawable.necromancer_casting_se));
        animation.putData(EActivities.CASTING, EDirection.SOUTH,cast.copy(R.drawable.necromancer_casting_s));
        animation.putData(EActivities.CASTING, EDirection.SOUTH_WEST,cast.copy(R.drawable.necromancer_casting_sw));
        animation.putData(EActivities.CASTING, EDirection.WEST,cast.copy(R.drawable.necromancer_casting_w));

    }

    private void initDeath(){
        SpriteSheet deathsheet = new SpriteSheet(8, 4, TextureData.NECROMANCER_DEATH, R.drawable.necromancer_death_n);
        AnimationData death = new AnimationData(Character.DYING_TIME,30, true, deathsheet);
        death.setStart(0, 3);
        death.setEnd(5, 0);
        death.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_TOP_TO_BOTTOM);
        animation.putData(EActivities.DEATH, EDirection.NORTH, death);
        // the sprite-sheets for necromancer has the same data but use other texture source
        animation.putData(EActivities.DEATH, EDirection.NORTH_EAST,death.copy(R.drawable.necromancer_death_ne));
        animation.putData(EActivities.DEATH, EDirection.NORTH_WEST,death.copy(R.drawable.necromancer_death_nw));
        animation.putData(EActivities.DEATH, EDirection.EAST,death.copy(R.drawable.necromancer_death_e));
        animation.putData(EActivities.DEATH, EDirection.SOUTH_EAST,death.copy(R.drawable.necromancer_death_se));
        animation.putData(EActivities.DEATH, EDirection.SOUTH,death.copy(R.drawable.necromancer_death_s));
        animation.putData(EActivities.DEATH, EDirection.SOUTH_WEST,death.copy(R.drawable.necromancer_death_sw));
        animation.putData(EActivities.DEATH, EDirection.WEST,death.copy(R.drawable.necromancer_death_w));

    }

}
