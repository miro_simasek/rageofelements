package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by MS on 4/18/2017.
 */

public class ManaPotion extends Item implements Usable {

    private static final int value = 200;
    private static final int manaAdd = 40;

    public ManaPotion() {
        super(R.drawable.mana_potion, value, GameActivity.getAppResources().getString(R.string.mana_potion));
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        character.getAbilities().addMana(manaAdd);
        return null;
    }

    @Override
    public int getIcon() {
        return R.drawable.mana_potion;
    }
}
