package com.example.ms.rageofelements.render;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.example.ms.rageofelements.base.Constants;


public class VBO implements Constants {
    //    protected FloatBuffer vertexBuffer;
//    protected FloatBuffer textureBuffer;
//    protected int textureBufferGLID, vertexBufferGLID;
    protected int textureID;
    float[] textureMatrix = new float[16];
    float[] modelMatrix = new float[16];

    private Shape shape;
    private Texture texture;

    public VBO(Shape shape, Texture texture) {
        Matrix.setIdentityM(textureMatrix, 0);
        Matrix.setIdentityM(modelMatrix, 0);
        this.texture = texture;
        this.shape = shape;
    }

    public VBO(Shape shape) {
        Matrix.setIdentityM(textureMatrix, 0);
        Matrix.setIdentityM(modelMatrix, 0);
        this.shape = shape;
        // creates texture for whole imag
        this.texture = new Texture(TextureData.whole, TextureData.WHOLE_TEXTURE);
    }

    public void draw(float[] matrix, TextureShader textureShader) {

        textureShader.setUniforms(matrix, modelMatrix, textureMatrix, textureID);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, shape.getGlShape());
        GLES20.glEnableVertexAttribArray(textureShader.getPositionLocation());
        GLES20.glVertexAttribPointer(textureShader.getPositionLocation(), 2, GLES20.GL_FLOAT, false, 0, 0);

//      bind buffer containing texture data (maybe one data for all common textures will be sufficient )
//      e.g. Grass Textures, trees, buildings, spells ... ) each category will need the same texture position data for all objects
//      but each will have their own matrix ( will see what is more efficient, maybe static data in buffers will be better for textures)
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, texture.getGlTextureData());
        GLES20.glEnableVertexAttribArray(textureShader.getTextureCoordinatesLocation());
        GLES20.glVertexAttribPointer(textureShader.getTextureCoordinatesLocation(), 2, GLES20.GL_FLOAT, false, 0, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

    }

    public void setTextureID(int textureRes) {
        this.textureID = Renderer.getTextureLoader().getTexture(textureRes);
    }

    public void translateModelMatrix(float x, float y) {
        Matrix.translateM(modelMatrix, 0, x, y, 0f);
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public void translateTextureMatrix(float x, float y) {
        Matrix.translateM(textureMatrix, 0, x, y, 0f);
    }

    public float[] getModelMatrix() {
        return modelMatrix;
    }

    public void setModelMatrix(float[] modelMatrix) {
        this.modelMatrix = modelMatrix;
    }
}
