package com.example.ms.rageofelements.quests;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.combat.Key;
import com.example.ms.rageofelements.map.EArea;
import com.example.ms.rageofelements.map.GameArea;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 */
class EnterTheTower extends AbstractQuest {


    EnterTheTower(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {
        Player.getInstance().deleteObserver(this);
        Player.getInstance().getAbilities().addExperience(100);
    }

    @Override
    public void onAccept() {
        Player.getInstance().addObserver(this);
        Player.getInstance().getInventory().add(new Key(EArea.VAMPIRE_TOWER));
        this.setNext(getManager().getQuestInstance(R.string.quest_confront_summoner));
    }


    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.quest_enter_the_tower);
    }

    @Override
    public String getDescription() {
        return GameActivity.getTextFromResources(R.string.quest_enter_the_tower_text);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Player){
            if (arg instanceof GameArea){
                this.setCompleted(((GameArea) arg).getName() == EArea.VAMPIRE_TOWER);
            }
        }
    }
}
