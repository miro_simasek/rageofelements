package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Abilities;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;


/**
 * Created by Miroslav Simasek
 *
 */
public class FireStorm extends Skill {

    private final float basicMana = 7f;
    private float damage;

    public FireStorm() {
        super(GameActivity.getAppResources().getString(R.string.fire_storm), R.drawable.fire_arrows_3);
        this.manaCost = basicMana;
        this.setMaxLevel(4);
    }

    @Override
    public void increaseLevel() {
        if (level >= getMaxLevel())
            return;

        super.increaseLevel();
        float manaIndex = 0.2f;
        this.manaCost = basicMana * (1.0f + manaIndex * (float) level);
        float damageIndex = 10f;
        this.damage = damageIndex * (float) level;
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        if (segment == null || segment.length == 0 || segment[0] == null) {
            return null;
        }

        Abilities abilities = character.getAbilities();
        if (abilities.getMana() < manaCost * getLevel())
            return null;

        Projectile[] entities = new Projectile[getLevel()];
        for (int i = 0; i < getLevel(); i++) {
            FireBallEntity entity = new FireBallEntity(character, this.damage,
                    Map.getDirection(character.getPosition(),segment[0]));
            entities[i] = entity;
        }

        switch (entities.length) {
            case 1: {
                float[] directions;
                directions = Projectile.getProjectileDirection(entities[0], segment[0]);
                entities[0].setDirections(directions[0], directions[1]);
                break;
            }
            case 2: {
                float[] directions;
                directions = Projectile.getProjectileDirection(entities[0], segment[0]);
                entities[0].setDirections(directions[0] + 0.1f, directions[1] + 0.1f);
                entities[1].setDirections(directions[0], directions[1]);
                break;
            }
            case 3: {
                float[] directions;
                directions = Projectile.getProjectileDirection(entities[0], segment[0]);
                entities[0].setDirections(directions[0] + 0.1f, directions[1] + 0.1f);
                entities[1].setDirections(directions[0], directions[1]);
                entities[2].setDirections(directions[0] - 0.1f, directions[1] - 0.1f);
                break;
            }
            case 4:{
                float[] directions;
                directions = Projectile.getProjectileDirection(entities[0], segment[0]);
                entities[0].setDirections(directions[0] + 0.05f, directions[1] + 0.5f);
                entities[1].setDirections(directions[0] + 0.01f, directions[1] + 0.1f);
                entities[2].setDirections(directions[0] - 0.1f, directions[1] - 0.1f);
                entities[3].setDirections(directions[0] - 0.05f, directions[1] - 0.05f);
                break;
            }
        }
        abilities.addMana(-manaCost * getLevel());
        this.adjustCharacter(character, segment[0]);
        return entities;
    }

    @Override
    public EActivities getRelatedActivity() {
        return EActivities.CASTING;
    }
}
