package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Abilities;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;


public class ManaShield extends DefensiveSkill {

    private final float basicResistence = 3f;
    private final float magicResistence = 2f;
    private float manaIndex = 4f;

    public ManaShield() {
        super(GameActivity.getAppResources().getString(R.string.mana_shield), R.drawable.protect_blue_1);
        this.putResistence(EDamage.BLUNT, basicResistence);
        this.putResistence(EDamage.SHARP, basicResistence);
        this.putResistence(EDamage.MAGIC, magicResistence);
        this.setMaxLevel(3);
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        this.setOwner(character);
        if (character.getActiveDefense() instanceof ManaShield) {
            this.disposeEffect();
            character.setActiveDefense(null);
            return null;
        } else {
            if (character.getActiveDefense()!= null){
                character.getActiveDefense().disposeEffect();
            }

            character.setActiveDefense(this);
            ManaShieldEntity manaShieldEntity = new ManaShieldEntity(character);
            setRelatedEffect(manaShieldEntity);
            this.adjustCharacter(character,segment[0]);
            return new GameEntity[]{manaShieldEntity};
        }

    }

    @Override
    public float filterDamage(EDamage damageType, float value) {
        float filtered = super.filterDamage(damageType, value);
        Abilities abilities = getOwner().getAbilities();
        if (abilities.getMana() > filtered * manaIndex) {
            abilities.addMana((int) (-((value - filtered) * manaIndex)));
            return filtered;
        } else {
            this.disposeEffect();
            getOwner().setActiveDefense(null);
            return value;
        }

    }

    @Override
    public void increaseLevel() {
        if (level < this.getMaxLevel()) {
            this.level++;
            this.putResistence(EDamage.BLUNT, basicResistence + basicResistence * level / 0.5f);
            this.putResistence(EDamage.SHARP, basicResistence + basicResistence * level / 0.5f);
            this.putResistence(EDamage.MAGIC, magicResistence + magicResistence * level / 0.5f);
            manaIndex = 3f / level;
        }
    }

    @Override
    public EActivities getRelatedActivity() {
        return EActivities.CASTING;
    }

    @Override
    public int getIcon() {
        switch (level) {
            case 2:
                return R.drawable.protect_blue_2;
            case 3:
                return R.drawable.protect_blue_3;
            default:
                return R.drawable.protect_blue_1;
        }
    }
}
