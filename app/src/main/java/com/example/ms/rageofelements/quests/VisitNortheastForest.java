package com.example.ms.rageofelements.quests;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.map.EArea;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.Segment;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 *
 */
class VisitNortheastForest extends AbstractQuest {

    private final String name;
    private boolean correctArea;
    private boolean correctPosition;
    private final int x,y, width, height;


    VisitNortheastForest(QuestManager manager) {
        super(manager);
        this.name = GameActivity.getAppResources().getString(R.string.Dialog_visitNortheastForest);
        x = 0;
        y = 30;
        width = 20;
        height = 15;
    }

    @Override
    public void onCompletion() {
        Player player = Player.getInstance();
        player.deleteObserver(this);
        player.getAbilities().addExperience(200);
    }

    @Override
    public void onAccept() {
        Player player = Player.getInstance();
        player.addObserver(this);
        this.setNext(getManager().getQuestInstance(R.string.quest_report_to_roshanak));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return GameActivity.getAppResources().getString(R.string.quest_visit_northeast_forest);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof  Player){
            if (arg instanceof GameArea){
                this.correctArea = ((GameArea) arg).getName()  == EArea.VAMPIRE_WOODS;
            }
            if (arg instanceof Segment){
                Segment s = (Segment) arg;
                int sX,sY;
                sX = s.getXIndex();
                sY = s.getYIndex();
                // check if coordinates match with predefined
                this.correctPosition = sX > x && sY > y && sX < x+width && sY < y + height;
            }
            this.setCompleted(this.correctPosition && this.correctArea);
        }
    }
}
