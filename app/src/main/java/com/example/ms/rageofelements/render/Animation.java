package com.example.ms.rageofelements.render;

import java.util.HashMap;



public class Animation<AK, DK> extends VBO {

    private AnimationData currentData; // currently displayed data
    private final HashMap<AK, HashMap<DK, AnimationData>> dataSet; // various data for animation

    private float textureX, textureY; // current position of left top point in texture coordinates
    private boolean updateTexture; // flag if texture should be updated
    private long frameStarted; // time when frame was changed
    private long timeToChange; // remaining time before changing frame
    private long frameDuration; // time duration of each frame

    public Animation(Shape shape) {
        super(shape);
        updateTexture = false;
        dataSet = new HashMap<>();
    }

    public void putData(AK animationkey, DK dataKey, AnimationData data) {

        if (dataSet.get(animationkey) == null) {
//            if no such a animation was added, create new container for data
            HashMap<DK, AnimationData> newData = new HashMap<>();
            newData.put(dataKey, data);
            dataSet.put(animationkey, newData);
        } else {
//            if such a animation was already added, put new data to container
            dataSet.get(animationkey).put(dataKey, data);
        }

    }

    public void update() {
        // if texture is single, perform update only once
        if (!updateTexture)
            return;
        // move animation matrix

        timeToChange -= (System.currentTimeMillis() - frameStarted);
        //set frame start time
        frameStarted = System.currentTimeMillis();
        if (timeToChange <= 0) {
            // increase time to change frame
            timeToChange += frameDuration;

//          get current sprite-sheet
            SpriteSheet spriteSheet = currentData.getSpriteSheet();
            float xMove;
            xMove = spriteSheet.getFrameWidth() * currentData.getxDirection();
            textureX += xMove;
            if (currentData.xBorderReached(textureX, textureY)) {
                //reset X
                textureX = currentData.getBeginningX();
                // move Y
                float yMove = spriteSheet.getFrameHeight() * currentData.getyDirection();
                textureY += yMove;
                //both border were passed, reset texture matrix
                if (currentData.yBorderReached(textureX, textureY)) {
                    // if animation is single leave last frame to be displayed
                    updateTexture = !currentData.isSingle();
                    if (currentData.isSingle())
                        return;
                    // reset texture matrix because of possible float inaccuracy
                    // reset y
                    textureY = currentData.getBeginningY();
                }
            }
            this.setTexturePosition(textureX, textureY);
        }
    }

    private void setTexturePosition(float startX, float startY) {
        // based on android.opengl.Matrix
        for (int i = 0; i < 4; i++) {
            textureMatrix[12 + i] = textureMatrix[i] * startX + textureMatrix[4 + i] * (1.0f - startY);
        }
    }

    public void setModelMatrixPosition(float startX, float startY){
        // based on android.opengl.Matrix
        for (int i = 0; i < 4; i++) {
            modelMatrix[12 + i] = modelMatrix[i] * startX + modelMatrix[4 + i] * (1.0f - startY);
        }
    }

    public synchronized void moveAnimation(float dx, float dy) {
        // move model matrix causing animation to move in game world
        translateModelMatrix(dx,dy);
    }


    public synchronized void switchAnimationByKey(AK key, DK textureKey) {
        if (dataSet.get(key) == null || dataSet.get(key).get(textureKey) == null) {
            return;
        }
        if (dataSet.get(key).get(textureKey) == currentData) {
//            we dont want to switch animation to be the same
            return;
        }
        // switch animation
        currentData = dataSet.get(key).get(textureKey);
        // calculate new frame duration according to texture data
        frameDuration = currentData.getFrameDuration();
        // reset flag for texture update (always after switching texture ww want to update it)
        updateTexture = true;
        //reset texture translation
        textureX = currentData.getBeginningX();
        textureY = currentData.getBeginningY();
        setTexturePosition(textureX, textureY);
        // set sprite-sheet to animate
        textureID = currentData.getSpriteSheet().getGlTexture();
        //set start time for frame
        frameStarted = System.currentTimeMillis();
        timeToChange = frameDuration;
        this.setTexture(currentData.getSpriteSheet());

    }


}
