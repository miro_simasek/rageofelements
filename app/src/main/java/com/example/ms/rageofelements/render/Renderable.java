package com.example.ms.rageofelements.render;

/**
 * Created by Miroslav Simasek
 *
 */
public interface Renderable {

    /**
     * Method for initialization of graphical part of object
     */
    void initGraphics();

    /**
     * Methods to drawing objects using texture shader program
     * @param matrix - projection matrix
     * @param shaderProgram - texture shader
     */
    void draw( float[] matrix, TextureShader shaderProgram );

}
