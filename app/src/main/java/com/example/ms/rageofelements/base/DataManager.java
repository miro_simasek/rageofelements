package com.example.ms.rageofelements.base;

import com.example.ms.rageofelements.base.screens.GamePlayScreen;
import com.example.ms.rageofelements.base.util.Node;
import com.example.ms.rageofelements.character.Abilities;
import com.example.ms.rageofelements.character.DialogData;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.combat.Item;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.combat.Usable;
import com.example.ms.rageofelements.quests.QuestManager;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 * Class for transferring data among fragments
 */
public class DataManager {


    private Usable[] actionButtonsContent;
    private ArrayList<Item> otherItems;
    private ArrayList<Item> segmentItems;
    private DialogData dialogData;
    private Player player;
    private QuestManager questManager;

    public DataManager(){
        actionButtonsContent = new Usable[GamePlayScreen.activeButtonsCount];
    }
    public ArrayList<Node<Skill>> getSkillRoots(){
      return player.getSkillRoots();
    }

    public ArrayList<Item> getOtherItems() {
        return otherItems;
    }

    public void setOtherItems(ArrayList<Item> items){
        otherItems = items;
    }

    public ArrayList<Item> getPlayerItems() {
        return player.getInventory();
    }

    public ArrayList<Item> getSegmentItems() {
        return segmentItems;
    }

    public void setSegmentItems(ArrayList<Item> segmentItems) {
        this.segmentItems = segmentItems;
    }

    public Usable[] getActionButtonsContent() {
        return actionButtonsContent;
    }

    public void setActionButtonsContent(Usable[] actionButtonsContent) {
        this.actionButtonsContent = actionButtonsContent;
    }

    public DialogData getDialogData() {
        return dialogData;
    }

    public void setDialogData(DialogData dialogData) {
        this.dialogData = dialogData;
    }

    public Abilities getAbilities() {
        return player.getAbilities();
    }



    public int getPlayerGold() {
        return player.getGoldValue();
    }

    public void setPlayerGold(int playerGold) {
        player.setGoldValue(playerGold);
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    void setQuestManager(QuestManager questManager) {
        this.questManager = questManager;
    }

    public QuestManager getQuestManager(){
        return this.questManager;
    }
}
