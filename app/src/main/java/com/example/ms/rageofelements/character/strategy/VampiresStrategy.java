package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.ECharacterGroup;
import com.example.ms.rageofelements.character.NPC;
import com.example.ms.rageofelements.character.Skeleton;
import com.example.ms.rageofelements.character.SkeletonArcher;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 */

public class VampiresStrategy extends StrategyManager {

    private static final int CLOSE_COMBAT_DISTANCE = 4;
    private static final int FIRE_PROJECTILE_DISTANCE = 9;
    private final GameArea gameArea;
    private FireProjectileStrategy fireProjectileStrategy;
    private WalkingStrategy walkingStrategy;
    private BasicAttackStrategy basicAttackStrategy;
    private ArrayList<Character> characters;

    public VampiresStrategy(NPC owner, GameArea gameArea) {
        super(owner, gameArea.getMap());
        this.gameArea = gameArea;
        this.characters = gameArea.getCharacters();
        this.basicAttackStrategy = new BasicAttackStrategy(owner, gameArea.getMap());
        this.walkingStrategy = new WalkingStrategy(owner, gameArea.getMap());
        this.fireProjectileStrategy = new FireProjectileStrategy(owner,gameArea);
        setCurrent(walkingStrategy);
    }

    @Override
    protected void check() {
        Character owner = this.getOwner();
        for (Character character : characters) {
            ECharacterGroup group = character.getGroup();
            if (character.isDead() || owner.getGroup() == ECharacterGroup.neutral || group == owner.getGroup())
                continue;

            if (Map.inReach(owner.getPosition(), character.getPosition(), CLOSE_COMBAT_DISTANCE)) {
                basicAttackStrategy.setTarget(character);
                setCurrent(basicAttackStrategy);
                return;
            } else if (Map.inReach(owner.getPosition(),character.getPosition(), FIRE_PROJECTILE_DISTANCE)){
                fireProjectileStrategy.setTarget(character);
                setCurrent(fireProjectileStrategy);
                return;
            }
        }
        setCurrent(walkingStrategy);
    }

    public void summonSkeletonArmy() {
        ArrayList<GameEntity> entities = new ArrayList<>();
        int positionX = getOwner().getPosition().getXIndex();
        int positionY = getOwner().getPosition().getYIndex();
        for (int i = 0; i < 6; i++){
            Segment segment = getArea().getSegment(positionX+i,positionY);
            if (segment != null && !segment.isObstacle()){
                NPC character;
                if (i % 2 == 0) {
                    character = new Skeleton(segment);
                    character.setStrategyManager(new SkeletonStrategyManager(character,this.gameArea));
                } else {
                    character = new SkeletonArcher(segment);
                    character.setStrategyManager(new SkeletonArcherStrategy(character,this.gameArea));
                }
                entities.add(character);
            }
        }
        for (GameEntity entity : entities) {
            this.gameArea.addEntity(entity);
            this.gameArea.requestEntityDisplay(entity);
        }

    }
}
