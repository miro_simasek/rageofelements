package com.example.ms.rageofelements.base;

/**
 * Created by Miroslav Simasek
 *
 */

public interface Messages {
    String noCharacter = "No character was selected. Application will stop.";
    String notUsable = "This item cannot be used this way.";
    String newQuest = "Quest &1 started";
    String noQuest = "No quest available";
}
