package com.example.ms.rageofelements.quests;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 */

class FinishTheGame extends AbstractQuest {

    FinishTheGame(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {

    }

    @Override
    public void onAccept() {
        // finish game activity

    }

    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.quest_finish_game);
    }

    @Override
    public String getDescription() {
        return GameActivity.getTextFromResources(R.string.quest_finish_game);
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}
