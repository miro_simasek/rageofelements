package com.example.ms.rageofelements.character;


import com.example.ms.rageofelements.base.Constants;
import com.example.ms.rageofelements.base.util.Node;
import com.example.ms.rageofelements.combat.Diamond;
import com.example.ms.rageofelements.combat.HealingPotion;
import com.example.ms.rageofelements.combat.ManaPotion;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Renderer;
import com.example.ms.rageofelements.render.TextureShader;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 *
 */

public abstract class Player extends Character {

    public static final long ATTACK_TIME = 600;
    private static final int PLAYER_INITIAL_GOLD = 300;
    private static Player instance;
    private float realMovementX;
    private float realMovementY;
    private ArrayList<Node<Skill>> skillRoots;
    private PlayerBar playerBar;

    protected Player(Segment position) {
        super(position, ECharacterGroup.player_group);
        skillRoots = new ArrayList<>();
        this.goldValue = PLAYER_INITIAL_GOLD;
        this.playerBar = new PlayerBar(this);
    }

    public static void loadInstance(Player player){
        if (instance == null){
            instance = player;
        }
    }

    public static void respawn(GameArea area){
        if (instance != null){
            Segment s = area.getInitialPoint();
            instance.setPosition(s);
            instance.getAbilities().addHealth(100);
            instance.getAbilities().addMana(100);
            instance.setActivity(EActivities.STANDING);
            instance.switchAnimation(false);
            area.addEntity(instance);
            Renderer.setProjectionMatrixT(s.getX(),s.getY());
        }
    }

    public static Player getInstance() {
        return instance;
    }

    public static void createPlayer(String tag, Segment position) {
        if (instance != null)
            return;
        switch (tag) {
            case Constants.necromancerTag:
                instance = new PNecromancer(position);
                break;
            case Constants.warriorTag:
                instance = new PWarrior(position);
                break;
            case Constants.rangerTag:
                instance = new PRanger(position);
                break;

        }
    }

    public static void clearInstance() {
        instance = null;
    }

    @Override
    protected void adjustAnimation(boolean partMovement) {
        if (partMovement) {
            // count how many times is step in one segment
            float count;
            // how many whole steps is in segment
            float wholePart;
            // remaining part
            float factor;
            if (dX != 0) {
                // if we were moving along x axis
                // adjust movement along X axis
                count = Segment.SEGMENT_WIDTH / dX;
                wholePart = (int) count;
                factor = count - wholePart;
                dX = factor == 0 ? dX : factor*dX;
            }

            if (dY != 0) {
                // if we were moving along y axis a
                // djust movement along y axis
                count = Segment.SEGMENT_HEIGHT / dY;
                wholePart = (int) count;
                factor = count - wholePart;
                dY = factor == 0 ? dY : factor*dY;
            }
            realMovementX += dX;
            realMovementY += dY;
            this.switchAnimation(false);
        } else {
            realMovementX += dX;
            realMovementY += dY;
        }

    }

    public boolean movementFetched() {
        return realMovementX == 0 && realMovementY == 0;
    }

    public float fetchMovedX() {
        float toRet = realMovementX;
        realMovementX = 0;
        return toRet;
    }

    public float fetchMovedY() {
        float toRet = realMovementY;
        realMovementY = 0;
        return toRet;
    }

    @Override
    public void initGraphics() {
        playerBar.initGraphics();
    }

    public ArrayList<Node<Skill>> getSkillRoots() {
        return skillRoots;
    }

    @Override
    public void setPosition(Segment segment) {
        super.setPosition(segment);
        // this movement will be fetched by renderer during first animation
        this.realMovementX = 0f;
        this.realMovementY = 0f;
    }

    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        super.draw(Renderer.getPlayerMatrix(), shaderProgram);
    }

    @Override
    protected void initInventory() {
//        inventory.add(new Key(EArea.VAMPIRE_TOWER));
        inventory.add(new HealingPotion());
        inventory.add(new HealingPotion());
        inventory.add(new ManaPotion());
        inventory.add(new Diamond());
    }

    public PlayerBar getPlayerBar() {
        return playerBar;
    }
}
