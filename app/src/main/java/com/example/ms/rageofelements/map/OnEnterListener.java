package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.character.Character;

/**
 * Interface for objects executing character transactions between areas
 */
public interface OnEnterListener {

    /**
     * Specify method for handling iir enter
     * @param door - object the character entered
     * @param character - character that entered door
     */
    void onEnter(Door door, Character character);

}
