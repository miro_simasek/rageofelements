package com.example.ms.rageofelements.base.util;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.combat.Skill;


/**
 * Created by Miroslav Simasek
 * View displaing skill
 */
public class SkillNodeView extends RelativeLayout {

    private DataManager dataManager;
    // icon of the skill
    private Button overview;
    // name of the skill to be displayed
    private TextView title;
    // tree node containing skill
    private Node<Skill> skill;
    private OnSkillChangeListener onSkillChangeListener;
    private int index;

    public SkillNodeView(Context context) {
        super(context);
        this.init();
    }

    public SkillNodeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.init();
    }

    public SkillNodeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init();
    }

    private void init() {
        inflate(getContext(), R.layout.skill_tree_node, this);

        overview = (Button) findViewById(R.id.skill_preview);
        title = (TextView) findViewById(R.id.node_title);

    }


    public void setSkillNode(Node<Skill> node, int myIndex) {
        this.index = myIndex;
        this.skill = node;
        final Skill nodeSkill = node.getData();
        // set title for skill
        title.setText(nodeSkill.getName());
        // set level of skill
        overview.setText(String.valueOf(nodeSkill.getLevel()));
        //set skill icon
        overview.setBackgroundResource(nodeSkill.getIcon());
        // if has some parent and its level is 0, grey out the skill

        if (skill.getData().getLevel() == 0) {
            overview.getBackground().mutate().setColorFilter(0x55555500, PorterDuff.Mode.MULTIPLY);
        }

        // set onSkillChangeListener for frag and drop if skill has level 1 or grater (make it available for use)
        if (nodeSkill.getLevel() > 0) {
            OnLongClickListener listener = new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //InstantActionButton view = ((InstantActionButton) v);
                    ClipData.Item item = new ClipData.Item(String.valueOf(index));

                    ClipData dragData = new ClipData((String) v.getTag(),
                            new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                    View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        v.startDragAndDrop(dragData, dragShadowBuilder, null, 0);
                    } else {
                        v.startDrag(dragData, dragShadowBuilder, null, 0);
                    }

                    return true;
                }
            };
            overview.setOnLongClickListener(listener);
        }

        if (node.getParent() == null || node.getParent().getData().getLevel() > 0) {
            // make node available for increasing
            // set on click onSkillChangeListener for overview
            overview.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dataManager != null && dataManager.getAbilities().getSkillPoints() > 0) {
                        if (nodeSkill.increasable()) {
                            dataManager.getAbilities().useSkillPoint();
                            nodeSkill.increaseLevel();
                            overview.setBackgroundResource(nodeSkill.getIcon());
                            onSkillChangeListener.onSkillChange(skill);
                        }
                    }
                }
            });
        }
    }

    public void setDataManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public void setOnSkillChangeListener(OnSkillChangeListener onSkillChangeListener) {
        this.onSkillChangeListener = onSkillChangeListener;
    }

    public Skill getSkill() {
        return skill.getData();
    }
}
