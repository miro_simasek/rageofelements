package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.Constants;

/**
 * Created by Miroslav Simasek
 * Main menu fragment
 */
public class MainMenu extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_menu_layout, container, false);
//        set onClick actions for buttons
        this.setControllers(view);
        return view;
    }


    private void setControllers(View view) {
        Button button = (Button) view.findViewById(R.id.NewGameButton);
//        add handling of new game button click
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//          switch to character selection fragment
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, new CharacterSelection(), Constants.character_selection_tag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
//        add credits button click
        button = (Button) view.findViewById(R.id.credits_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                swtich to load game fragment
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, new CreditsScreen(), Constants.credits_tag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

}
