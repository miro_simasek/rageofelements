package com.example.ms.rageofelements.character;

import android.opengl.Matrix;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.render.Renderable;
import com.example.ms.rageofelements.render.Renderer;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureData;
import com.example.ms.rageofelements.render.TextureShader;
import com.example.ms.rageofelements.render.VBO;

/**
 * Created by Miroslav Simasek
 * Visual representation of Player's mana and health points
 */

public class PlayerBar implements Renderable{

    private static final String name = "hud_bar";

    private final Player player;
    private VBO lifeBar, manaBar;

    PlayerBar(Player player) {
        this.player = player;
    }

    private float[] getBarShape() {
        return new float[]{-Renderer.scaleFactor * Renderer.getAspectRatio(), 100f,
                -Renderer.scaleFactor * Renderer.getAspectRatio(), 90f,
                Renderer.scaleFactor * Renderer.getAspectRatio(), 100f,
                Renderer.scaleFactor * Renderer.getAspectRatio(), 90f};
    }

    @Override
    public void initGraphics() {
        Texture texture = new Texture(TextureData.whole, TextureData.WHOLE_TEXTURE);
        Shape shape = new Shape(getBarShape(), name);

        lifeBar = new VBO(shape, texture);
        lifeBar.setTextureID(R.drawable.health);

        manaBar = new VBO(shape, texture);
        manaBar.setTextureID(R.drawable.mana);

        manaBar.translateModelMatrix(0f, -10f);
    }


    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        if (lifeBar != null) {
            lifeBar.draw(Renderer.getPlayerMatrix(), shaderProgram);
            manaBar.draw(Renderer.getPlayerMatrix(), shaderProgram);
        }
    }

    /**
     * Transform bars visual representation to reflect amount of players life
     */
    public synchronized void updateStatus() {
        // scaling cannot be easily reverted, so we reset the matrix
        Matrix.setIdentityM(manaBar.getModelMatrix(), 0);
        // translate mana bar to its position
        Matrix.translateM(manaBar.getModelMatrix(),0,0f,-10f,0f);
        // reset life bar scaling
        Matrix.setIdentityM(lifeBar.getModelMatrix(), 0);
        // we don't want to display negative mana so if scale is < 0, set it to 0
        float scale = player.getAbilities().getMana() / player.getAbilities().getMaxMana();
        if (scale < 0) scale = 0;
        Matrix.scaleM(manaBar.getModelMatrix(), 0, scale , 1f, 1f);
        // we don't want to display negative life so if scale is < 0, set it to 0
        scale = player.getAbilities().getHealth() / player.getAbilities().getMaxHelth();
        if (scale < 0) scale = 0;
        Matrix.scaleM(lifeBar.getModelMatrix(), 0, scale , 1f, 1f);
    }

}
