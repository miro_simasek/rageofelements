package com.example.ms.rageofelements.quests;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.character.Vampires;
import com.example.ms.rageofelements.map.GameEntity;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 */

class KillTheSummoner extends AbstractQuest {
    private Vampires vampires;

    KillTheSummoner(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {
        vampires.deleteObserver(this);
        vampires = null;
        Player.getInstance().getAbilities().addExperience(300);

    }

    @Override
    public void onAccept() {
        for (GameEntity entity : getManager().getQuestEntities()) {
            if (entity instanceof Vampires){
                vampires = (Vampires) entity;
                vampires.addObserver(this);
            }
        }
        this.setNext(getManager().getQuestInstance(R.string.quest_escape_the_tower));
    }


    @Override
    public String getName() {
        return GameActivity.getTextFromResources(R.string.quest_kill_the_summoner);
    }

    @Override
    public String getDescription() {
        return GameActivity.getTextFromResources(R.string.quest_kill_the_summoner_text);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Vampires){
            this.setCompleted(arg == EActivities.DEATH);
        }
    }
}
