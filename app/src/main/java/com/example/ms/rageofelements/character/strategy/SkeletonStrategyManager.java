package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.ECharacterGroup;
import com.example.ms.rageofelements.character.NPC;
import com.example.ms.rageofelements.map.GameArea;
import com.example.ms.rageofelements.map.Map;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 *
 */
public class SkeletonStrategyManager extends StrategyManager {

    private static final int VIEW_DISTANCE = 8;
    private WalkingStrategy walkingStrategy;
    private BasicAttackStrategy basicAttackStrategy;
    private ArrayList<Character> characters;

    public SkeletonStrategyManager(NPC owner, GameArea gameArea) {
        super(owner, gameArea.getMap());
        this.characters = gameArea.getCharacters();
        basicAttackStrategy = new BasicAttackStrategy(owner, gameArea.getMap());
        walkingStrategy = new WalkingStrategy(owner, gameArea.getMap());
        setCurrent(walkingStrategy);

    }


    @Override
    protected void check() {
        Character owner= this.getOwner();
        for (Character character : characters) {
            ECharacterGroup group = character.getGroup();
            if (group == ECharacterGroup.neutral || group == owner.getGroup()) {
                continue;
            }
            if (Map.inReach(owner.getPosition(), character.getPosition(), VIEW_DISTANCE)) {
                if (character.isDead())
                    continue;
                basicAttackStrategy.setTarget(character);
                setCurrent(basicAttackStrategy);
                return;
            }
        }
        setCurrent(walkingStrategy);
    }
}
