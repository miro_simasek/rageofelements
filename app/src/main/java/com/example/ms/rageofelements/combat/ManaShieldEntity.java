package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Renderer;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureShader;

/**
 * Created by MS on 4/24/2017.
 */

public class ManaShieldEntity extends GameEntity {
    private final String shapeName = ManaShieldEntity.class.getName();
    private Character owner;
    private Animation<String, String> animation;

    ManaShieldEntity(Character owner) {
        super(owner.getPosition());
        this.owner = owner;
    }

    @Override
    public void initGraphics() {
        animation = new Animation<>(this.createShape());
        animation.setModelMatrix(owner.getAnimation().getModelMatrix());
        AnimationData animationData = new AnimationData(400, 4, false, new SpriteSheet(4, 1, "shield", R.drawable.magic_shield));
        animationData.setDirection(AnimationData.X_RIGHT_TO_LEFT, AnimationData.Y_BOTTOM_TO_TOP);
        animationData.setStart(0, 0);
        animationData.setEnd(3, 0);
        animation.putData(shapeName, shapeName, animationData);
        animation.switchAnimationByKey(shapeName, shapeName);
        if (!(owner instanceof Player)) {
            animation.setModelMatrix(owner.getAnimation().getModelMatrix());
        }
    }

    private Shape createShape() {
        return new Shape(new float[]{
                -Shape.humanImageWidth / 2f, Shape.humanImageHeight - Shape.humanImageYFit,
                -Shape.humanImageWidth / 2f, -Shape.humanImageYFit,
                Shape.humanImageWidth / 2f, Shape.humanImageHeight - Shape.humanImageYFit,
                Shape.humanImageWidth / 2f, -Shape.humanImageYFit
        }, shapeName);
    }

    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        if (animation != null) {
            animation.draw(owner instanceof Player ? Renderer.getPlayerMatrix() : matrix, shaderProgram);
        }

    }


    @Override
    public void update() {
        // no need to update shield entity
    }

    @Override
    public void updateAnimation() {
        if (animation!= null){
            animation.update();
        }
    }

}
