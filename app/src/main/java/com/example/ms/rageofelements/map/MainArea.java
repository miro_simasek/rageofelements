package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.base.AreaManager;
import com.example.ms.rageofelements.character.NPC;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.character.Skeleton;
import com.example.ms.rageofelements.character.SkeletonArcher;
import com.example.ms.rageofelements.character.strategy.SkeletonArcherStrategy;
import com.example.ms.rageofelements.character.strategy.SkeletonStrategyManager;
import com.example.ms.rageofelements.character.strategy.StrategyManager;

import java.util.Random;

/**
 * Created by Miroslav Simasek
 * Game area representing initial area
 */
public class MainArea extends GameArea {

    public MainArea(String playerType, AreaManager areaManager) {
        super(new Map(50, 50),areaManager, EArea.VAMPIRE_WOODS);
        //        temporaly before mapfilecreation
        Random r = new Random(System.nanoTime());
        Player.createPlayer(playerType, map.getSegment(38, 12));
        for (int i = 15; i < map.getWidth() -10; i++) {
            for (int j = 15; j < map.getHeight() -10; j++) {
                if (r.nextInt(100) < 5) {
                    map.getSegment(i,j).setForegroundType(ESegmentType.green_tree1);
                }
            }
        }

        this.initCharacters();
        this.initBuildings();

    }

    private void initBuildings() {
        Segment builingStart = map.getSegment(35, 5);
        map.addBuilding(new BuildingHut(map.getAreaByStart(builingStart, 8, 4)));

        builingStart = map.getSegment(5,40);
        map.addBuilding(new VampireTowerObject(map.getAreaByStart(builingStart,5,5)));

        builingStart = map.getSegment(10,25);
        HollyWell well = new HollyWell(map.getAreaByStart(builingStart,2,2));
        map.addBuilding(well);
        this.addEntity(well);

    }

    private void initCharacters() {
        this.createSkeleton(8,15);
        this.createSkeleton(30,27);
        this.createSkeleton(30,17);
        this.createSkeleton(17,27);
        this.createSkeleton(49,27);
        this.createSkeletonArcher(17,20);
        this.createSkeletonArcher(18,30);
        this.createSkeletonArcher(19,40);
        this.createSkeletonArcher(20,49);
        // because this is initial area we will add player here
        characters.add(Player.getInstance());

    }

    private void createSkeleton(int x, int y){
        NPC character = new Skeleton(map.getSegment(x, y));
        StrategyManager strategyManager = new SkeletonStrategyManager(character, this);
        character.setStrategyManager(strategyManager);
        characters.add(character);
    }

    private void createSkeletonArcher(int x, int y){
        NPC character = new SkeletonArcher(map.getSegment(x,y));
        StrategyManager strategyManager = new SkeletonArcherStrategy(character,this);
        character.setStrategyManager(strategyManager);
        characters.add(character);
    }


    @Override
    public Segment getInitialPoint() {
        return map.getSegment(38,12);
    }
}
