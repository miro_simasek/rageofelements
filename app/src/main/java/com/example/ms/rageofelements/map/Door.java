package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.combat.Key;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureData;
import com.example.ms.rageofelements.render.TextureShader;
import com.example.ms.rageofelements.render.VBO;

/**
 * Class representing access point to another area
 */
public class Door extends GameEntity {

    private final EArea targetArea;
    private VBO boundary;
    private OnEnterListener onEnterListener;
    private final Segment accessPoint;
    private Door targetPoint;
    private boolean innerDoor;
    private boolean locked;

    public Door(EArea area, Segment accessPoint, Segment position) {
        super(position);
        this.targetArea = area;
        this.accessPoint = accessPoint;
        boundaryHeight = Segment.SEGMENT_HEIGHT * 3f;
        boundaryWidth = Segment.SEGMENT_WIDTH;
        boundaryX = accessPoint.getX() - boundaryWidth / 2f;
        boundaryY = accessPoint.getY() + boundaryHeight / 2f;
        this.setChanged();
        locked = false;
    }



    public boolean unlock(Key key){
        if (key == null){
            return !locked;
        }
        return !locked || key.getArea() == this.targetArea;
    }

    public void setInner(boolean innerDoor) {
        this.innerDoor = innerDoor;
    }

    public void connectTo(Door targetPoint) {
        this.targetPoint = targetPoint;
    }

    public void setOnEnterListener(OnEnterListener listener) {
        this.onEnterListener = listener;
    }

    public void entered(Character character) {
        onEnterListener.onEnter(this, character);
    }

    public EArea getDestination() {
        return targetArea;
    }

    public Segment getTargetPoint() {
        return targetPoint.getAccessPoint();
    }

    public Segment getAccessPoint() {
        return accessPoint;
    }

    @Override
    public void initGraphics() {
        // create door boundary
        if (innerDoor) {
            boundary = new VBO(new Shape(new float[]{
                    -Shape.humanImageWidth / 2f + Segment.HALF_SEGMENT_WIDTH, Shape.humanImageHeight - Shape.humanImageYFit + Segment.HALF_SEGMENT_HEIGHT,
                    -Shape.humanImageWidth / 2f + Segment.HALF_SEGMENT_WIDTH, -Shape.humanImageYFit + Segment.HALF_SEGMENT_HEIGHT,
                    Shape.humanImageWidth / 2f + Segment.HALF_SEGMENT_WIDTH, Shape.humanImageHeight - Shape.humanImageYFit + Segment.HALF_SEGMENT_HEIGHT,
                    Shape.humanImageWidth / 2f + Segment.HALF_SEGMENT_WIDTH, -Shape.humanImageYFit + Segment.HALF_SEGMENT_HEIGHT}, "boundary_door"), new Texture(TextureData.whole, TextureData.WHOLE_TEXTURE));
            boundary.translateModelMatrix(accessPoint.getX(), accessPoint.getY());
            boundary.setTextureID(R.drawable.door);
        }
    }


    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        if (boundary != null) {
            boundary.draw(matrix, shaderProgram);
        }
    }

    @Override
    public void update() {
        // currently there is nothing that should be updated in door object
    }

    @Override
    public void updateAnimation() {
        // currently we have no animation for Door object
    }

    public void setLock(boolean lock) {
        this.locked = lock;
    }

    public boolean isLocked() {
        return locked;
    }
}
