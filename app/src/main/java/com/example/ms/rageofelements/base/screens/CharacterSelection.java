package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.Constants;
import com.example.ms.rageofelements.base.GameActivity;

/**
 * Created by Miroslav Simasek
 *
 */
public class CharacterSelection extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.character_selection_layout,container, false);

//        set description text to be scrollable
        final TextView textView = (TextView) v.findViewById(R.id.charDescText);
        textView.setMovementMethod(new ScrollingMovementMethod());
//        init controllers
        this.initControllers(v);

        return v;
    }

    private void initControllers(View v){
        //      handle necromancer selection
        RadioButton radioButton = (RadioButton) v.findViewById(R.id.necromancer);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v = v.getRootView();
//                update image
                ImageView imageView = (ImageView) v.findViewById(R.id.characterPreview);
                imageView.setImageResource(R.drawable.necromancer_preview);
//                update text
                TextView textView = (TextView) v.findViewById(R.id.charDescText);
                textView.setText(R.string.necromancerDesc);
            }
        });
        radioButton.setChecked(true);
        radioButton.callOnClick();

//        handle ranger selection
        radioButton = (RadioButton) v.findViewById(R.id.ranger);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v = v.getRootView();
//                update image
                ImageView imageView = (ImageView) v.findViewById(R.id.characterPreview);
                imageView.setImageResource(R.drawable.ranger_preview);
//                update text
                TextView textView = (TextView) v.findViewById(R.id.charDescText);
                textView.setText(R.string.rangerDesc);
            }
        });

        //        handle warrior selection
        radioButton = (RadioButton) v.findViewById(R.id.warrior);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v = v.getRootView();
//                update image
                ImageView imageView = (ImageView) v.findViewById(R.id.characterPreview);
                imageView.setImageResource(R.drawable.warrior_preview);
//                update text
                TextView textView = (TextView) v.findViewById(R.id.charDescText);
                textView.setText(R.string.warriorDesc);
            }
        });

        Button button = (Button) v.findViewById(R.id.charConfirmButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v = v.getRootView();
                int checked;
//              get selected RadioButton
                RadioGroup radioGroup = (RadioGroup) v.findViewById(R.id.selection_group);
//               get selecte radio button
                checked = radioGroup.getCheckedRadioButtonId();
//              get tag of selected character
                RadioButton radioButton = (RadioButton) v.findViewById(checked);
                String characterTag = radioButton.getText().toString();
//               start game with character selection
                Intent intent = new Intent(getActivity().getBaseContext(),GameActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.characterKey,characterTag);
                intent.putExtras(bundle);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }
}
