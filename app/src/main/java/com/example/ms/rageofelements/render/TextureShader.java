package com.example.ms.rageofelements.render;

import android.content.Context;

import com.example.ms.rageofelements.R;

import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniformMatrix4fv;

/**
 * Created by MS on 1/7/2017.
 */

public class TextureShader extends ShaderProgram {

    //uniform locations
    private final int uMatrixLocation;
    private final int uTextureUnitLocation;
    private final int uTextureMatrixLocation;
    private final int uModelMatrixLocation;

    // Attribute locations
    private final int aPositionLocation;
    private final int aTextureCoordinatesLocation;

    TextureShader(Context context) {
        super(context, R.raw.vertex_shader,R.raw.fragment_shader);

        //get uniforms locations for shader program
        uMatrixLocation = glGetUniformLocation(program,U_MATRIX);
        uTextureUnitLocation = glGetUniformLocation(program,U_TEXTURE_UNIT);
        uTextureMatrixLocation = glGetUniformLocation(program,U_TEXTURE_MATRIX);
        uModelMatrixLocation = glGetUniformLocation(program,U_MODEL_MATRIX);

        // get attributes locations for shader program
        aPositionLocation = glGetAttribLocation(program, A_POSITION);
        aTextureCoordinatesLocation = glGetAttribLocation(program, A_TEXTURE_COORDINATES);
    }

    void setUniforms(float[] projectionMatrix, float[] modelMatrix,
                        float[] textureMatrix, int textureID){
        
        glUniformMatrix4fv(uMatrixLocation,1,false,projectionMatrix,0);
        glUniformMatrix4fv(uTextureMatrixLocation,1,false,textureMatrix,0);
        glUniformMatrix4fv(uModelMatrixLocation,1,false,modelMatrix,0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D,textureID);
        glUniform1i(uTextureUnitLocation,0);

    }

    int getPositionLocation() {
        return aPositionLocation;
    }

    int getTextureCoordinatesLocation() {
        return aTextureCoordinatesLocation;
    }
}
