package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.combat.Item;
import com.example.ms.rageofelements.render.Renderable;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureData;
import com.example.ms.rageofelements.render.TextureShader;
import com.example.ms.rageofelements.render.VBO;

import java.util.ArrayList;

/**
 * Map Segment
 * Smallest part of map
 *
 */
public class Segment extends PathNode implements Renderable{

    public static final float SEGMENT_HEIGHT = 20f;
    public static final float HALF_SEGMENT_HEIGHT = SEGMENT_HEIGHT / 2.0f;
    public static final float SEGMENT_WIDTH = 2.0f * SEGMENT_HEIGHT;
    public static final float HALF_SEGMENT_WIDTH = SEGMENT_WIDTH / 2.0f;
    public static final float SEGMENT_SIZE = (float) Math.sqrt(Math.pow(HALF_SEGMENT_HEIGHT, 2.0f) + Math.pow(HALF_SEGMENT_WIDTH, 2.0f));

    //verticles defining shape of segment
    private static final float[] segmentVerticles = new float[]{
            //bottom x,y
            0f, 0f - SEGMENT_HEIGHT,
            //left x,y
            0f - SEGMENT_WIDTH / 2f, 0f - SEGMENT_HEIGHT / 2f,
            //right x,y
            0f + SEGMENT_WIDTH / 2f, 0f - SEGMENT_HEIGHT / 2f,
            //top x,y
            0f, 0f
    };
    // entities of segment
    private final ArrayList<GameEntity> entities;
    // graphical parts of segment
    private VBO background, foreground;
    // location of segment
    private float X, Y;
    // default type
    private ESegmentType backgroundType = ESegmentType.empty_segment;
    private ESegmentType foregroundType = ESegmentType.empty_segment;
    // multisegment object whose part can segment contain
    private MultisegmentObject multisegmentObject;
    // items placed on segment
    private ArrayList<Item> items;


    public Segment(int row, int column) {
        super(row, column);
        // suradnica X vzhladom na izometricky posun segmentu kde berieme do uvahu poziciu v poli
        this.X = (yIndex - xIndex) * SEGMENT_WIDTH / 2f;
        // suradnica Y vzhladom na izometricky posun segmentu a poziciu v poli segmentov
        this.Y = -(yIndex + xIndex) * SEGMENT_HEIGHT / 2f;
        //create list for entities
        entities = new ArrayList<>();
        items = new ArrayList<>();
    }

    @Override
    public void initGraphics() {
        // init graphics to segment and all its related objects
        this.initBackground();
        this.initForeground();
        for (Item item : items) {
            item.initGraphics();
            break;
        }
        synchronized (entities) {
            for (GameEntity entity : entities) {
                entity.initGraphics();
            }
        }

    }

    private void initBackground() {
        Shape shape = new Shape(segmentVerticles, Shape.SEGMENT_SHAPE);
        Texture texture = new Texture(TextureData.tenth, TextureData.TENTH);

        background = new VBO(shape, texture);
        //move segment to its coordinates
        background.translateModelMatrix(this.X, this.Y);

        switch (backgroundType) {
            case LAMINATE_FLOORING_1:
                background.setTextureID(R.drawable.laminate_flooring_1);
                break;
            case stone_floor_1:
                background.setTextureID(R.drawable.stone_floor_1);
                break;
            default:
                background.setTextureID(R.drawable.grass_texture1);
                break;
        }

        background.translateTextureMatrix(0.1f * (float) (xIndex % 10), 0.1f * (float) (yIndex % 10));
    }


    private void initForeground() {

        switch (foregroundType) {
            case green_tree1:
                foreground = new Tree(R.drawable.green_tree1);
                foreground.translateModelMatrix(X, Y);
                setObstacle(true);
                break;
            case barrel_1:
                foreground = new Barrel();
                foreground.translateModelMatrix(X,Y);
                setObstacle(true);
                break;
            default:
                break;
        }


    }

    /**
     * Set background type for segment
     * @param type - background type
     */
    void setBackgroundType(ESegmentType type) {
        this.backgroundType = type;
    }


    /**
     * Method for dynamic removal of entities from segment
     * @param entity - entity to be removed
     */
    public void removeEntity(GameEntity entity) {
        synchronized (entities) {
            this.entities.remove(entity);
        }
    }

    /**
     * Method for dynamic adding entities to segment
     * @param entity - new entity
     */
    public void addEntity(GameEntity entity) {
        synchronized (entities) {
            if (entity instanceof MultisegmentObject) {
                this.multisegmentObject = (MultisegmentObject) entity;
            } else {
                this.entities.add(entity);
            }
        }
    }

    /**
     * Draw background of the segment
     * @param matrix - projection matrix
     * @param shaderProgram - shader program
     */
    void drawBackground(float[] matrix, TextureShader shaderProgram) {
        background.draw(matrix, shaderProgram);
    }

    /**
     * Draw foreground items of segment
     * @param matrix - projection matrix
     * @param shaderProgram - shader program
     */
    void drawForeground(float[] matrix, TextureShader shaderProgram) {

        // we don't need to draw all items because their would be overdrawn anyway
        for (Item item : items) {
            item.draw(matrix, shaderProgram);
            break;
        }

        if (multisegmentObject != null) {
            multisegmentObject.drawPart(this, matrix, shaderProgram);
        }

        synchronized (entities) {
            for (GameEntity ge : entities) {
                ge.draw(matrix, shaderProgram);
            }
        }

        if (foreground != null) {
            foreground.draw(matrix, shaderProgram);
        }

    }

    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        background.draw(matrix, shaderProgram);
    }

    public float getX() {
        return X;
    }

    public float getY() {
        return Y;
    }

    /**
     * Get item list of segment
     * @return - item list
     */
    public ArrayList<Item> getItemList() {
        return items;
    }

    /**
     * Return character if it is one of the segment's entities
     * @return - character
     */
    public Character getCharacter() {
        for (GameEntity entity : entities) {
            if (entity instanceof Character) {
                return (Character) entity;
            }
        }
        return null;
    }

    /**
     * set items to segments
     * @param items - new items
     */
    public void setItems(ArrayList<Item> items) {
        this.items = items;
        for (Item item : items) {
            item.setPosition(this);
        }
    }

    void setForegroundType(ESegmentType foregroundType) {
        this.foregroundType = foregroundType;
        setObstacle(foregroundType.obstacle);
    }

    public MultisegmentObject getMultiSegmentObject() {
        return this.multisegmentObject;
    }
}
