package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.util.DialogAdapter;
import com.example.ms.rageofelements.character.DialogData;

/**
 * Created by Miroslav Simasek
 * Fragment for player's dialog with characters
 *
 */
public class DialogScreen extends Fragment {

    private DataManager dataManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = ((GameActivity) getActivity()).getDataManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_screen_layout, container, false);
        DialogData data = dataManager.getDialogData();

        TextView title = (TextView) v.findViewById(R.id.dialog_screen_title);
        title.setText(data.getMainTitle());

        ListView listView = (ListView) v.findViewById(R.id.option_list);
        DialogAdapter adapter = new DialogAdapter(v.getContext(), R.layout.button_template);
        adapter.setDataManager(dataManager);
        adapter.setFragmentManager(getFragmentManager());

        adapter.addAll(data.getDataSet().keySet());
        listView.setAdapter(adapter);


        return v;
    }

}
