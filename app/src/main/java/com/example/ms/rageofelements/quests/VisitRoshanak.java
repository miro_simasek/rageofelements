package com.example.ms.rageofelements.quests;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.map.EArea;
import com.example.ms.rageofelements.map.GameArea;

import java.util.Observable;

class VisitRoshanak extends AbstractQuest{

    VisitRoshanak(QuestManager manager) {
        super(manager);
    }

    @Override
    public void onCompletion() {
        // stop player observation
        Player.getInstance().deleteObserver(this);
        // add character experience
        Player.getInstance().getAbilities().addExperience(1000);
    }

    @Override
    public void onAccept() {
        // start player observation
        Player.getInstance().addObserver(this);
        this.setNext(getManager().getQuestInstance(R.string.quest_get_roshanaks_work));
    }

    @Override
    public String getName() {
        return GameActivity.getAppResources().getString(R.string.quest_visit_roshanaks_hut);
    }

    @Override
    public String getDescription() {
        return GameActivity.getAppResources().getString(R.string.quest_visit_roshanaks_hut_desc);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof  Player){
            if (arg instanceof GameArea){
                this.setCompleted(((GameArea) arg).getName() == EArea.ROSHANAK_HOUSEHOLD);
            }
        }
    }
}
