package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by Miroslav Simasek
 */

public class MultiShot extends Skill {

    private float damageIndex;
    private float damage;

    public MultiShot() {
        super(GameActivity.getTextFromResources(R.string.multi_shot), R.drawable.needles_sky_1);
        this.setMaxLevel(3);
        damage = 10f;
        damageIndex = 0.05f;
    }

    @Override
    public void increaseLevel() {
        if (getLevel() >= getMaxLevel())
            return;
        super.increaseLevel();
        damage += 2f;
    }

    @Override
    public EActivities getRelatedActivity() {
        return EActivities.SHOOTING;
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        if (segment == null || segment.length == 0 || segment[0] == null) {
            return null;
        }
        float finalDamage = damage + damageIndex * character.getAbilities().getDexterity();
        Projectile[] entities = new Projectile[getLevel()];
        for (int i = 0; i < getLevel(); i++) {
            ArrowEntity entity = new ArrowEntity(character, finalDamage, Map.getDirection(character.getPosition(), segment[0]));
            entities[i] = entity;
        }

        switch (entities.length) {
            case 1: {
                float[] directions;
                directions = Projectile.getProjectileDirection(entities[0], segment[0]);
                entities[0].setDirections(directions[0], directions[1]);
                break;
            }
            case 2: {
                float[] directions;
                directions = Projectile.getProjectileDirection(entities[0], segment[0]);
                entities[0].setDirections(directions[0] + 0.1f, directions[1] + 0.1f);
                entities[1].setDirections(directions[0], directions[1]);
                break;
            }
            case 3: {
                float[] directions;
                directions = Projectile.getProjectileDirection(entities[0], segment[0]);
                entities[0].setDirections(directions[0] + 0.1f, directions[1] + 0.1f);
                entities[1].setDirections(directions[0], directions[1]);
                entities[2].setDirections(directions[0] - 0.1f, directions[1] - 0.1f);
                break;
            }
        }

        this.adjustCharacter(character, segment[0]);
        return entities;
    }
}
