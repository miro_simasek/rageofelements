package com.example.ms.rageofelements.render;

import android.content.Context;
import android.content.res.Resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by MS on 1/6/2017.
 */

class ShaderSourceReader {

    static String readTextFromResource(Context context, int resourceID){
        StringBuilder body = new StringBuilder();
        try{
            InputStream inputStream = context.getResources().openRawResource(resourceID);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String nextLine;

            while ((nextLine = bufferedReader.readLine())!= null){
                body.append(nextLine);
                body.append('\n');
            }
        }catch (IOException ex){
            throw new RuntimeException("Could not open resource "+resourceID,ex);
        } catch (Resources.NotFoundException ex){
            throw new RuntimeException("Resource not found : "+ resourceID,ex);
        }

        return body.toString();
    }

}
