package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.character.Abilities;
import com.example.ms.rageofelements.character.Character;

/**
 * Created by Miroslav Simasek
 */

class RegenerationAuraEntity extends ManaShieldEntity{

    private long lastRegen;
    private Abilities abilities;
    private int level;

    RegenerationAuraEntity(Character owner, int level) {
        super(owner);
        lastRegen = System.currentTimeMillis();
        abilities = owner.getAbilities();
        this.level = level;
    }

    @Override
    public void update() {
        // remove mana and add health points
        long elapsed = System.currentTimeMillis() - lastRegen;
        if (elapsed > 100) {
            float regen = level;
            this.lastRegen = System.currentTimeMillis();
//            float ratio = elapsed / 1000;
            this.abilities.addMana( -1.5f*abilities.getManaRegen());
            this.abilities.addHealth( (regen+1.5f)* abilities.getHealthRegen());
        }
    }


}
