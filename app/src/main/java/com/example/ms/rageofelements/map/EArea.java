package com.example.ms.rageofelements.map;

/**
 * Created by MS on 4/14/2017.
 */

public enum EArea {

    VAMPIRE_WOODS(false,false), VAMPIRE_TOWER(true,true), ROSHANAK_HOUSEHOLD(true,false);

    private final boolean inner;
    private final boolean defaultLock;

    EArea(boolean inner, boolean defaultLock) {
        this.inner = inner;
        this.defaultLock = defaultLock;
    }

    public boolean isInner() {
        return inner;
    }

    public boolean isDefaultLock() {
        return defaultLock;
    }
}
