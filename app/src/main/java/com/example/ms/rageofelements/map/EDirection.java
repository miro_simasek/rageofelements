package com.example.ms.rageofelements.map;


public enum EDirection {

    NORTH(-1f, 1f, "n", 2f), // ok
    NORTH_EAST(-1f, 0f, "ne", 1f), // flipped
    EAST(-1f, -1f, "e", 2f), // ok
    SOUTH_EAST(0f, -1f, "se", 1f), // flipped
    SOUTH(1f, -1f, "s", 2f), // ok
    SOUTH_WEST(1f, 0f, "sw", 1f), // flipped
    WEST(1f, 1f, "w", 2f), // ok
    NORTH_WEST(0f,1f, "nw", 1f);// flipped
//    STANDING(0f, 0f, "stand", 0f);

    public final float xCourse, yCourse; // movement direction along isometric axes
    public final float diagonalDivision; // if we are not moving diagonal we need to divide movement by 2
                            // we need to make half movement in graphics that should behave as regular movement
    public String abbreviation;

    EDirection(float xCourse, float yCourse, String abbreviation, float diagonalDivision) {
        this.xCourse = xCourse;
        this.yCourse = yCourse;
        this.abbreviation = abbreviation;
        this.diagonalDivision = diagonalDivision;
    }


}
