package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureData;
import com.example.ms.rageofelements.render.TextureShader;
import com.example.ms.rageofelements.render.VBO;

/**
 * Created by Miroslav Simasek
 *
 */
public abstract class Item extends GameEntity {

    private static final String itemShapeTag = "item";
    private final String name;
    private int iconSource;
    private int value;
    private VBO vbo;
    private Segment position;


    public Item(int iconSource, int value, String name) {
        super();
        this.iconSource = iconSource;
        this.value = value;
        this.name = name;

    }

    public int getIconSource() {
        return iconSource;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    @Override
    public void initGraphics() {

        if (position != null) {
            Shape item = new Shape(getItemShape(), itemShapeTag);
            Texture texture = new Texture(TextureData.whole, TextureData.WHOLE_TEXTURE);
            vbo = new VBO(item, texture);
            vbo.translateModelMatrix(position.getX(), position.getY());
            vbo.setTextureID(iconSource);
        }

    }


    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        if (vbo != null) {
            vbo.draw(matrix, shaderProgram);
        }
    }

    private float[] getItemShape() {
        float yImageFit = Segment.SEGMENT_HEIGHT;
        float size = Segment.SEGMENT_SIZE;
        return new float[]{
                -size / 2f, size - yImageFit,
                -size / 2f, -yImageFit,
                size / 2f, size - yImageFit,
                size / 2f, -yImageFit

        };
    }

    public void setPosition(Segment position) {
        this.position = position;
    }

    public void update(){
        // there is nothing to update on basic item
    }

    @Override
    public void updateAnimation() {
        // no animation to be updated for standard item
    }
}
