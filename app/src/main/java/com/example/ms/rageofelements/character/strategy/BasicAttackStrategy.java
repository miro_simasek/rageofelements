package com.example.ms.rageofelements.character.strategy;

import android.util.Pair;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.Segment;

import java.util.Random;

/**
 * Created by MS on 4/6/2017.
 */

public class BasicAttackStrategy extends Strategy {

    private static final long WAIT_TIME = 750;
    private static final long WAIT_TIME_LONG = 1500;
    private final int searchRadius;
    private boolean attacked;


    BasicAttackStrategy(Character character, Map area) {
        super(character, area);
        searchRadius = 4;
        attacked = false;
    }

    @Override
    public void execute() {
        if (this.checkTime())
            return;
        Character target = this.getTarget();
        Character owner = this.getOwner();
        Map area = this.getArea();

        Segment center = target.getPosition();
        if (attacked) {
            owner.setActivity(EActivities.RUNNING);
            Random gen = new Random(System.nanoTime());
            int x = gen.nextInt(searchRadius);
            x *= gen.nextBoolean() ? 1 : -1;
            int y = gen.nextInt(searchRadius);
            y *= gen.nextBoolean() ? 1 : -1;
            center = area.getSegment(center.getXIndex() + x, center.getYIndex() + y);
            owner.setDestination(center);
            owner.switchAnimation(false);
            return;
        }
        if (Math.abs(owner.getPosition().getXIndex()
                - center.getXIndex()) <= 1
                && Math.abs(owner.getPosition().getYIndex()
                - center.getYIndex()) <= 1 && owner.getActivity() == EActivities.STANDING) {



            Pair<EDamage, Float> attack = owner.getBasickAttack();

            target.hit(attack.first, attack.second);
            target.setTimeOut(Character.HIT_TIME_OUT);
            target.setActivity(EActivities.HIT);
            target.switchAnimation(false);
            target.setActive(false);

            owner.setTimeOut(Character.ATTACK_TIME_OUT);
            owner.setActivity(EActivities.ATTACK);
            owner.setDirection(target.getPosition());
            owner.switchAnimation(false);
            owner.setActive(false);

        } else if (owner.getActivity() == EActivities.STANDING) {

            owner.setActivity(EActivities.RUNNING);
            owner.setDestination(center);
            owner.switchAnimation(false);
        }
    }

    @Override
    public void onActivityChange(EActivities activities) {
        long delay = this.getDelay();
        switch (activities) {
            case STANDING:
                delay = WAIT_TIME_LONG;
                break;
            case ATTACK:
                delay = WAIT_TIME + Character.ATTACK_TIME_OUT;
                break;
            default:
                break;
        }
        this.setDelay(delay);
        this.setDelayStart();
        this.attacked = activities == EActivities.ATTACK;
    }


}
