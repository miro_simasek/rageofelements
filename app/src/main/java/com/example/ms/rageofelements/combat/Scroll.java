package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by Miroslav Simasek
 */

public class Scroll extends Item implements Usable{

    private final Skill relatedSkill;

    public Scroll(Skill relatedSkill) {
        super(relatedSkill.getIcon(), 500, relatedSkill.getName()+" Scroll");
        this.relatedSkill = relatedSkill;
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
        return relatedSkill.onUse(character,segment);
    }

    @Override
    public int getIcon() {
        return relatedSkill.getIcon();
    }

    public Skill getSkill() {
        return relatedSkill;
    }
}
