package com.example.ms.rageofelements.combat;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;

import static com.example.ms.rageofelements.map.Segment.SEGMENT_HEIGHT;
import static com.example.ms.rageofelements.map.Segment.SEGMENT_WIDTH;

/**
 * Created by Miroslav Simasek
 *
 */
class TrapEntity extends Projectile {

    private final String explosion = "explosion_trap";


    TrapEntity(Character owner, float damageVal) {
        super(owner, 0f,EDirection.NORTH);
        getDamages().put(EDamage.MAGIC, damageVal * 0.25f);
        getDamages().put(EDamage.SHARP, damageVal * 0.75f);
    }

    @Override
    public void initGraphics() {
        if (position != null) {
            animation = new Animation<>(new Shape(createAnimationData(), this.getClass().getName()));
            String effectName = "trap_effect";
            AnimationData data = new AnimationData(2000, 16, false, new SpriteSheet(4, 4, effectName, R.drawable.trap_effect));
            data.setStart(0, 0);
            data.setEnd(3, 3);
            data.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_BOTTOM_TO_TOP);
            animation.putData(effectName, getBaseDirection(), data);

            data = new AnimationData(1000, 6, true, new SpriteSheet(1, 6, explosion, R.drawable.blue_explosion));
            data.setDirection(AnimationData.X_LEFT_TO_RIGHT, AnimationData.Y_BOTTOM_TO_TOP);
            data.setStart(0, 0);
            data.setEnd(0, 5);
            animation.putData(explosion, getBaseDirection(), data);
            animation.translateModelMatrix(position.getX(), position.getY());
            animation.switchAnimationByKey(effectName, getBaseDirection());
        }
    }

    @Override
    public void onShot() {
        super.onShot();
        if (animation != null) {
            animation.switchAnimationByKey(explosion, getBaseDirection());
        }
        this.setTimeOut(1000);
    }

    @Override
    public void setSegment(Segment segment) {
        if (segment.getCharacter() != null ){
            this.onShot();
            this.setTimeOut(1000);
            this.setActive(false);
        }
    }

    private float[] createAnimationData() {
        return new float[]{
                //bottom x,y
                0f, 0f - SEGMENT_HEIGHT,
                //left x,y
                0f - SEGMENT_WIDTH / 2f, 0f - SEGMENT_HEIGHT / 2f,
                //right x,y
                0f + SEGMENT_WIDTH / 2f, 0f - SEGMENT_HEIGHT / 2f,
                //top x,y
                0f, 0f
        };
    }


}
