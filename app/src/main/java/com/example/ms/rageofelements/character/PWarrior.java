package com.example.ms.rageofelements.character;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.util.Node;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.ManaShield;
import com.example.ms.rageofelements.combat.RegenerationAura;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.AnimationData;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.SpriteSheet;
import com.example.ms.rageofelements.render.TextureData;

/**
 * Created by Miroslav Simasek
 *
 */
class PWarrior extends Player {

    PWarrior(Segment position) {
        super(position);
        this.initSkillTree();
        this.initAbilities();
        this.setBasickAttack(EDamage.SHARP, 20);
    }

    private void initSkillTree() {
        ManaShield shield = new ManaShield();

        Node<Skill> root = new Node<Skill>(shield);
        root.setNodeName(GameActivity.getAppResources().getString(R.string.shield_root));
        getSkillRoots().add(root);

        Node<Skill> regenAura = new Node<Skill>(new RegenerationAura());
        root.addChild(regenAura);
    }

    @Override
    protected void initAbilities() {
        abilities.setMaxHelth(220);
        abilities.setConcentration(5);
        abilities.setVitality(15);
        abilities.setDexterity(8);
        abilities.setStrength(15);
    }

    @Override
    protected void initInventory() {
        super.initInventory();
    }

    @Override
    public void initGraphics() {
        super.initGraphics();
//        AnimationData[] dataSet;
        Shape shape = new Shape(Character.getCharacterData(1.0f,0f), this.getClass().getSimpleName());
        animation = new Animation<>(shape);

        //we don't want the animation to be updated before initialisation finished;
        this.initStanding();
        this.initAttacking();
        this.initRunning();
        this.initHit();
        this.initDeath();
        this.animation.switchAnimationByKey(EActivities.STANDING, EDirection.NORTH);

    }

    private void initDeath() {
        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.WARRIOR_DEATH,R.drawable.warrior_death_1);
        AnimationData data = new AnimationData(Character.DEATH_ANIMATION_TIME,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.DEATH,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.DEATH,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.DEATH,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.DEATH,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.WARRIOR_STANDING,R.drawable.warrior_death_2);
        data = new AnimationData(Character.STANDING_ANIMATION_TIME,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.DEATH,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.DEATH,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.DEATH,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.DEATH,EDirection.EAST,data);
    }

    private void initHit() {
        SpriteSheet hit = new SpriteSheet(8,8, TextureData.WARRIOR_HIT,R.drawable.warrior_hit);
        AnimationData data = new AnimationData(Character.HIT_TIME_OUT,8,true,hit);
        data.setDirection(AnimationData.X_RIGHT_TO_LEFT,AnimationData.Y_BOTTOM_TO_TOP);
        data.setStart(7,0);
        data.setEnd(0,0);
        animation.putData(EActivities.HIT,EDirection.EAST,data);

        data = data.cloneData();
        data.setStart(7,1);
        data.setEnd(0,1);
        animation.putData(EActivities.HIT,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(7,2);
        data.setEnd(0,2);
        animation.putData(EActivities.HIT,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(7,3);
        data.setEnd(0,3);
        animation.putData(EActivities.HIT,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,4);
        data.setEnd(0,4);
        animation.putData(EActivities.HIT,EDirection.WEST,data);

        data = data.cloneData();
        data.setStart(7,5);
        data.setEnd(0,5);
        animation.putData(EActivities.HIT,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(7,6);
        data.setEnd(0,6);
        animation.putData(EActivities.HIT,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(7,7);
        data.setEnd(0,7);
        animation.putData(EActivities.HIT,EDirection.SOUTH_EAST,data);
    }

    private void initStanding() {

        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.WARRIOR_STANDING,R.drawable.warrior_standing_1);
        AnimationData data = new AnimationData(Character.STANDING_ANIMATION_TIME,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.STANDING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.STANDING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.STANDING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.STANDING,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.WARRIOR_STANDING,R.drawable.warrior_standing_2);
        data = new AnimationData(Character.STANDING_ANIMATION_TIME,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.STANDING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.STANDING,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.STANDING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.STANDING,EDirection.EAST,data);

    }

    private void initAttacking() {
        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.WARRIOR_ATTACK,R.drawable.warrior_attack1);
        AnimationData data = new AnimationData(Character.ATTACK_TIME_OUT,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_EAST,data);
        animation.putData(EActivities.CASTING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH,data);
        animation.putData(EActivities.CASTING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.ATTACK,EDirection.SOUTH_WEST,data);
        animation.putData(EActivities.CASTING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.ATTACK,EDirection.WEST,data);
        animation.putData(EActivities.CASTING,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.WARRIOR_ATTACK,R.drawable.warrior_attack2);
        data = new AnimationData(Character.ATTACK_TIME_OUT,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_WEST,data);
        animation.putData(EActivities.CASTING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.ATTACK,EDirection.NORTH,data);
        animation.putData(EActivities.CASTING,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.ATTACK,EDirection.NORTH_EAST,data);
        animation.putData(EActivities.CASTING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.ATTACK,EDirection.EAST,data);
        animation.putData(EActivities.CASTING,EDirection.EAST,data);

    }

    private void initRunning() {

        SpriteSheet spriteSheet1 = new SpriteSheet(8,8, TextureData.WARRIOR_RUNNING,R.drawable.warrior_running1);
        AnimationData data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet1);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);
        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.SOUTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.WEST,data);

        SpriteSheet spriteSheet2 = new SpriteSheet(8,8, TextureData.WARRIOR_RUNNING,R.drawable.warrior_running2);
        data = new AnimationData(Character.RUNNING_ANIMATION_TIME,16,false,spriteSheet2);
        data.setDirection(AnimationData.X_LEFT_TO_RIGHT,AnimationData.Y_TOP_TO_BOTTOM);

        data.setStart(0,7);
        data.setEnd(7,6);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_WEST,data);

        data = data.cloneData();
        data.setStart(0,5);
        data.setEnd(7,4);
        animation.putData(EActivities.RUNNING,EDirection.NORTH,data);

        data = data.cloneData();
        data.setStart(0,3);
        data.setEnd(7,2);
        animation.putData(EActivities.RUNNING,EDirection.NORTH_EAST,data);

        data = data.cloneData();
        data.setStart(0,1);
        data.setEnd(7,0);
        animation.putData(EActivities.RUNNING,EDirection.EAST,data);

    }

}
