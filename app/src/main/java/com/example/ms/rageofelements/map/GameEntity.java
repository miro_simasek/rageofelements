package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.render.Renderable;

import java.util.Observable;

/**
 * Created by Miroslav Simasek
 * Base class for all objects in game that has some logical meaning
 * It contains interface for drawing objects and also basic
 * method for entity controlling
 *
 */
public abstract class GameEntity extends Observable implements Renderable {
    // position on map
    protected Segment position;
    // in game coordinates
    protected float boundaryX, boundaryY, boundaryHeight, boundaryWidth; // left right corner
    private boolean active;
    private long inactivityStart;
    private long timeOut;

    public GameEntity(Segment position) {
        this.position = position;
        position.addEntity(this);
        active = true;
    }

    public GameEntity() {
        active = true;
    }

    public Segment getPosition() {
        return position;
    }


    public void setPosition(Segment position) {
        if (this.position != null) {
            this.position.removeEntity(this);
        }

        this.position = position;
        if (position !=null){
            this.position.addEntity(this);
        }
    }

    public abstract void update();


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
        if (!active) {
            this.inactivityStart = System.currentTimeMillis();
        }
    }

    public boolean checkTimeOut() {
        return System.currentTimeMillis() > inactivityStart + this.timeOut;
    }

    public boolean inBoundary(float x, float y) {
        return x > boundaryX && x < boundaryX + boundaryWidth && y < boundaryY && y > boundaryY - boundaryHeight;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    public abstract void updateAnimation();

    public void setObservableChanged(boolean changed){
        if (changed){
            super.setChanged();
        } else {
            super.clearChanged();
        }
    }
}
