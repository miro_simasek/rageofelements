package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.render.Renderable;
import com.example.ms.rageofelements.render.TextureShader;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 * <p>
 * Game Map, also representing some sort of container for all game object
 */
public class Map implements Renderable {

    private static final int VIEW_RADIUS = 12;
    private final ArrayList<MultisegmentObject> buildings;
    private final Segment[][] segments;
    private int lastX;
    private int lastY;
    private Player player;
    private int width, height;

    public Map(int width, int height) {
        lastX = 0;
        lastY = 0;
        buildings = new ArrayList<>();
        segments = new Segment[width][height];
        this.width = width;
        this.height = height;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                segments[i][j] = new Segment(i, j);
            }
        }
    }

    public static EDirection getDirection(Segment position, Segment target) {
        int currentX, currentY, nextX, nextY;
        currentX = position.getXIndex();
        currentY = position.getYIndex();
        nextX = target.getXIndex();
        nextY = target.getYIndex();

        if (nextY > currentY) {
            //EAST
            if (nextX > currentX) {
                //SOUTH
                return EDirection.SOUTH_EAST;
            } else if (nextX < currentX) {
                //NORTH
                return EDirection.NORTH_EAST;
            } else {
                return EDirection.EAST;
            }
        } else if (nextY < currentY) {
            //WEST
            if (nextX > currentX) {
                //SOUTH
                return EDirection.SOUTH_WEST;
            } else if (nextX < currentX) {
                //NORTH
                return EDirection.NORTH_WEST;
            } else {
                return EDirection.WEST;
            }
        } else {
            // NORTH OR SOUTH
            if (nextX > currentX) {
                //SOUTH
                return EDirection.SOUTH;
            } else {
                //NORTH
                return EDirection.NORTH;
            }
        }
    }

    public static boolean inReach(Segment position, Segment target, int maxDistance) {
        return (Math.abs(position.getXIndex() - target.getXIndex()) <= maxDistance
                && Math.abs(position.getYIndex() - target.getYIndex()) <= maxDistance);
    }

    public void addBuilding(MultisegmentObject building) {
        synchronized (buildings) {
            buildings.add(building);
        }
    }

    public void removeBuilding(MultisegmentObject building) {
        synchronized (buildings) {
            buildings.remove(building);
        }
    }

    public Segment[][] getSegments() {
        return segments;
    }

    public Segment getSegment(int x, int y) {
        if (x >= 0 && y >= 0 && x < width && y < height) {
            return segments[x][y];
        }
        return null;
    }


    @Override
    public void initGraphics() {
        synchronized (buildings) {
            for (MultisegmentObject building : buildings) {
                building.initGraphics();
            }
        }

        for (Segment[] segment : segments) {
            for (Segment segment1 : segment) {
                segment1.initGraphics();
            }
        }

    }

    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        Segment position = player.getPosition();
        int xStart, xEnd, yStart, yEnd;
        if (position == null) {
            xStart = lastX - VIEW_RADIUS;
            xEnd = lastX + VIEW_RADIUS + 1;
            yStart = lastY - VIEW_RADIUS;
            yEnd = lastY + VIEW_RADIUS + 1;
        } else {
            lastX = position.getXIndex();
            lastY = position.getYIndex();
            xStart = position.getXIndex() - VIEW_RADIUS;
            xEnd = position.getXIndex() + VIEW_RADIUS + 1;
            yStart = position.getYIndex() - VIEW_RADIUS;
            yEnd = position.getYIndex() + VIEW_RADIUS + 1;
        }

        if (xStart < 0) xStart = 0;
        if (xEnd >= width) xEnd = width;
        if (yStart < 0) yStart = 0;
        if (yEnd >= height) yEnd = height;

        for (int j = yStart; j < yEnd; j++) {
            for (int i = xStart; i < xEnd; i++) {
                segments[i][j].drawBackground(matrix, shaderProgram);
            }
        }

        for (int j = yStart; j < yEnd; j++) {
            for (int i = xStart; i < xEnd; i++) {
                segments[i][j].drawForeground(matrix, shaderProgram);
            }
        }

    }


    Segment[][] getAreaByStart(Segment start, int width, int height) {
        if (width == 0 || height == 0) {
            return null;
        }

        Segment[][] area = new Segment[width][height];
        int startX = start.getXIndex();
        int startY = start.getYIndex();
//        int ax = 0;
//        int ay = 0;
        for (int i = 0; i < width; i++) {
            if (i + startX < this.width) {
//                ax++;
                for (int j = 0; j < height; j++) {
                    if (j + startY < this.height) {
//                        ay++;
                        area[i][j] = segments[i + startX][j + startY];
                    }
                }
//                ay = -1;
            }
        }

        return area;

    }

    public Segment[][] getAreaByCenter(Segment position, int xRadius, int yRadius) {
        if (xRadius <= 0 || yRadius <= 0) {
            return null;
        }

        Segment[][] area = new Segment[(xRadius * 2) + 1][(yRadius * 2) + 1];
        int centerX = position.getXIndex();
        int centerY = position.getYIndex();
        int ax, ay;
        ax = ay = -1;

        for (int i = centerX - xRadius; i <= centerX + xRadius; i++) {
            if (i >= 0 && i < width) {
                ax++;
                for (int j = centerY - yRadius; j <= centerY + yRadius; j++) {
                    if (j >= 0 && j < height) {
                        ay++;
                        area[ax][ay] = segments[i][j];
                    }
                }
                ay = -1;
            }
        }
        return area;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ArrayList<MultisegmentObject> getBuildings() {
        return buildings;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
