package com.example.ms.rageofelements.character;

import android.util.Pair;

import com.example.ms.rageofelements.combat.DefensiveSkill;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.Item;
import com.example.ms.rageofelements.map.EDirection;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Map;
import com.example.ms.rageofelements.map.PathNode;
import com.example.ms.rageofelements.map.Pathfinder;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Animation;
import com.example.ms.rageofelements.render.Renderable;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.TextureShader;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

/**
 * Base class representing each character
 */
public abstract class Character extends GameEntity implements Renderable {

    public static final long HIT_TIME_OUT = 300;
    public static final long MAGIC_TIME_OUT = 800;
    public static final long ATTACK_TIME_OUT = 800;
    public static final long DYING_TIME = 1000;
    static final long SHOOTING_TIME_OUT = 300;
    static final long RUNNING_ANIMATION_TIME = 1000;
    static final long STANDING_ANIMATION_TIME = 1000;
    static final long DEATH_ANIMATION_TIME = 1500;
    private static final float baseSpeed = 2.0f;

    /**
     * Get vertex data for character with small animation
     * @return - vertex data
     */
    static float[] getCharacterData(float scaling, float fit){
        float width = Shape.humanImageWidth * scaling;
        float height = Shape.humanImageHeight * scaling;
        float imageFit = Shape.humanImageYFit - fit*Segment.SEGMENT_HEIGHT;
        return new float[]{

                -width / 2f, height - imageFit,
                -width / 2f, -imageFit,
                width/ 2f, height - imageFit,
                width/ 2f, -imageFit

        };
    }


    //    animation
    protected Animation<EActivities, EDirection> animation;
    protected final Abilities abilities;
    //    moved in map coordinates
    float dX, dY;
    //inventory
    protected ArrayList<Item> inventory;
    int goldValue;
    //character activity (what is character doing)
    private EActivities activity;

    private long lastRegen;
    private ECharacterGroup group;
    private EDirection direction;
    private EDirection previous;
    //variables for path finding
    private LinkedList<PathNode> path;
    private Segment target;
    private Segment next;

    // used to measure distance on path between segments
    private float fromSegmentX, fromSegmentY;
    private boolean onHalfWay;
    private Pair<EDamage, Float> basickAttack;
    private EActivities previousActivity;
    private DefensiveSkill activeDefense;


    /**
     * Create new character
     *
     * @param position - initial position
     * @param group    - group for combat
     */
    public Character(Segment position, ECharacterGroup group) {
        super(position);
        position.setObstacle(true);
        this.group = group;
        this.activity = EActivities.STANDING;
        this.previous = EDirection.NORTH;
        this.direction = EDirection.NORTH;
        this.previousActivity = EActivities.STANDING;
        this.dX = 0f;
        this.dY = 0f;
        this.inventory = new ArrayList<>();
        this.path = new LinkedList<>();
        this.next = null;
        this.initInventory();
        this.abilities = new Abilities();
        // by default let characer always notify it's observers
        this.setChanged();
    }

    public void update() {
        // if character is running move character
        if (activity == EActivities.RUNNING) {
            this.move();
        }
        // if we have nou mana and we are using defensive skill, dispose defensive skill
        if (activeDefense != null && abilities.getMana() <= 0.0f) {
            activeDefense.disposeEffect();
            activeDefense = null;
        }
        // regenerate character properties
        this.regen();
    }

    protected void regen() {
        long elapsed = System.currentTimeMillis() - lastRegen;
        if (elapsed > 100) {
            this.lastRegen = System.currentTimeMillis();

            float manaRegen = 0.2f;
            this.abilities.addMana(manaRegen * abilities.getManaRegen());
            float lifeRegen = 0.1f;
            this.abilities.addHealth(lifeRegen * abilities.getHealthRegen());
        }
    }

    private void move() {
        if (animation == null || next == null) {
            this.setActivity(EActivities.STANDING);
            return;
        }

        float directionX, directionY;
//      move character according to direction
        float speed = baseSpeed;
        directionX = speed * direction.xCourse; // xCourse * 2 because of isometric view
        directionY = speed * direction.yCourse / 2f;
        // move boundary
        boundaryX -= directionX;
        boundaryY += directionY;
        // increase traveled distance
        fromSegmentX += directionX;
        fromSegmentY += directionY;

        dX += directionX;
        dY += directionY;

//      round values because of float inaccuracy
        int round;
        round = Math.round(dX * 100000);
        dX = ((float) round) / 100000;
        round = Math.round(dY * 100000);
        dY = ((float) round) / 100000;

//      check if next segment reached
//      if reached change segment an update segment information
        if (Math.abs(fromSegmentX) >= Segment.HALF_SEGMENT_WIDTH / direction.diagonalDivision
                || Math.abs(fromSegmentY) >= Segment.HALF_SEGMENT_HEIGHT / direction.diagonalDivision) {
//           switch on half way flag
            this.onHalfWay = !this.onHalfWay;
            fromSegmentX = 0f;
            fromSegmentY = 0f;
            // if we were on half way to next segment, we reached the position
            if (this.onHalfWay) {
                //change segments
                position.setObstacle(false);
                position.removeEntity(this);

                next.addEntity(this);
                position = next;
                // notify observers about position change
                this.setChanged();
                this.notifyObservers(position);
                if (activeDefense != null) {
                    this.activeDefense.setPosition(position);
                }

            } else {
                // came to segment
                //clear control distance

                if (path == null || path.isEmpty()) {
                    // if no segments in path clear target and next segment
                    next = null;
                    target = null;
                    this.setActivity(EActivities.STANDING);
                    this.switchAnimation(false);
                    if (activeDefense != null) {
                        this.activeDefense.setPosition(position);
                    }

                } else {
                    next = (Segment) path.poll();
                    //if next segment is occupied recalculate path
                    if (next.isObstacle()) {
                        path = Pathfinder.calculatePath(position, target);
                        if (path == null || path.isEmpty()) {
                            // no available path exists switch to standing
                            this.setActivity(EActivities.STANDING);
                            this.adjustAnimation(false);
                            this.clearDirection();
                            return;
                        }
                        // set next segment from recalculated path
                        next = (Segment) path.poll();

                    }
                    next.setObstacle(true);// prevent next segment to be occupied
                }
                //center character on segment and change direction and destination
                this.setDirection(next);
                // adjust animation for part movement (character is centered to segment
                // speed based movement is not complete)
                adjustAnimation(true);
                this.clearDirection();
                // we don't need to make any further adjustments if player reached segment
                return;
            }
        }
        // move animation

        // adjust animation for complete movement (character was moved for whole speed length)
        adjustAnimation(false);
        this.clearDirection();

    }

    protected void adjustAnimation(boolean partMovement) {
        if (animation == null)
            return;
        if (partMovement) {
            // count how many times is step in one segment
            float count;
            // how many whole steps is in segment
            float wholePart;
            // remaining part
            float factor;
            if (dX != 0) {
                // if we were moving along x axis
                // adjust movement along X axis
                count = Segment.SEGMENT_WIDTH / dX;
                wholePart = (int) count;
                factor = count - wholePart;
                dX = factor == 0 ? dX : factor * dX;
            }

            if (dY != 0) {
                // if we were moving along y axis a
                // djust movement along y axis
                count = Segment.SEGMENT_HEIGHT / dY;
                wholePart = (int) count;
                factor = count - wholePart;
                dY = factor == 0 ? dY : factor * dY;
            }
            // adjust animation considering possible step not fitting into segment dimensions
            animation.moveAnimation(-dX, dY);
            this.switchAnimation(false);
        } else {
            animation.moveAnimation(-dX, dY);
        }
        // cler step counting variables
        this.dX = 0;
        this.dY = 0;

    }

    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        if (animation != null) {
//            animation.update();
            animation.draw(matrix, shaderProgram);
        }
    }

    /**
     * Set new destination for character
     *
     * @param segment - destination on map
     */
    public void setDestination(Segment segment) {
        this.target = segment;
        if (next == null) {
            path = Pathfinder.calculatePath(position, segment);
        } else {
            path = Pathfinder.calculatePath(next, segment);
            return;
        }
        if (path == null || path.size() == 0) {
            //if running was set it was no previous activity we need to override it
            this.previousActivity = EActivities.STANDING;
            this.setActivity(EActivities.STANDING);

            return;
        }
        next = (Segment) path.poll();
        next.setObstacle(true);
        this.setDirection(next);

    }

    public ECharacterGroup getGroup() {
        return group;
    }

    protected abstract void initInventory();

    protected abstract void initAbilities();

    public ArrayList<Item> getInventory() {
        return inventory;
    }

    public void hit(EDamage type, float value) {
        Random r = new Random(System.nanoTime());
        // get random number to avoid damage
        int number = r.nextInt(100) + 1;
        // number for comaprison
        int cmp = abilities.getDexterity() < 150 ? (int) abilities.getDexterity() : 150;
        cmp /= 5;
        // if character is lucky (or have high dexterity) avoid damage
        if (number < cmp) {
            return;
        }

        int damageValue = (int) value;
        // if some defensive skill is used filter damage based on skill
        if (activeDefense != null) {
            damageValue = (int) activeDefense.filterDamage(type, value);
            abilities.addHealth(-damageValue);
        } else {
            abilities.addHealth(-damageValue);
        }

        if (damageValue > 0) {
            this.setActivity(EActivities.HIT);
            this.switchAnimation(false);
            this.setTimeOut(Character.HIT_TIME_OUT);
            this.setActive(false);
            this.setDirection(next);
        }

    }

    public boolean isDead() {
        return abilities.getHealth() <= 0;
    }

    public Abilities getAbilities() {
        return abilities;
    }

    public void switchAnimation(boolean fromPrevious) {
        if (animation != null) {
            animation.switchAnimationByKey(this.activity, (fromPrevious ? previous : direction));
        }
    }

    public int getGoldValue() {
        return goldValue;
    }

    public void setGoldValue(int goldValue) {
        this.goldValue = goldValue;
    }

    @Override
    public boolean checkTimeOut() {
        if (super.checkTimeOut()) {
            this.setActive(true);

            if (previousActivity == EActivities.RUNNING && next == null) {
                this.setActivity(EActivities.STANDING); // return to default activity because we cannot move without path
            } else if (previousActivity == EActivities.RUNNING) {
                // if we were running our direction could be changed, we need to set direction to next segment
                // now we also know that next segment is not null
                this.direction = previous;
                this.setActivity(previousActivity);
            } else {
                this.setActivity(previousActivity);
            }
            this.switchAnimation(false);
        }
        return this.isActive();
    }

    public Pair<EDamage, Float> getBasickAttack() {
        float attackValue = basickAttack.second + 0.05f * basickAttack.second * getAbilities().getStrength();
        return new Pair<>(basickAttack.first, attackValue);
    }

    void setBasickAttack(EDamage damage, float value) {
        this.basickAttack = new Pair<>(damage, value);
    }

    public EActivities getActivity() {
        return activity;
    }

    public void setActivity(EActivities activity) {
        if (this.activity == EActivities.STANDING
                || this.activity == EActivities.RUNNING) {
            // we want to revert character state only if it was running or standing
            this.previousActivity = this.activity;
        }
        this.activity = activity;
    }

    public void setPosition(Segment segment) {
        //remove this entity from previous position
        if (position != null) {
            this.position.removeEntity(this);
        }
        // if next position is not null
        if (segment != null) {
            // add entity to new position
            segment.addEntity(this);
            // actualize position attribute
            this.position = segment;
            // set correct boundary
            this.boundaryX = position.getX() - boundaryWidth / 2f;
            this.boundaryY = position.getY() + boundaryHeight / 2f;

        } else {
            // clear position
            position = null;
        }
        // if we are using some defensive skill, dispose it
        if (this.activeDefense != null) {
            activeDefense.disposeEffect();
            activeDefense = null;
        }
        this.setActivity(EActivities.STANDING);
        // clear path
        path = null;
        // if character was moving during this action
        if (next != null) {
            // reset status of next segment
            this.next.setObstacle(false);
            this.next = null;
        }
        // clear target segment
        this.target = null;
        // clear movement control variables
        this.fromSegmentX = 0.0f;
        this.fromSegmentY = 0.0f;
        // switch animation to reflect new state
        this.switchAnimation(false);

    }

    public DefensiveSkill getActiveDefense() {
        return activeDefense;
    }

    public void setActiveDefense(DefensiveSkill activeDefense) {
        this.activeDefense = activeDefense;
    }

    public Animation<EActivities, EDirection> getAnimation() {
        return animation;
    }

    public void setDirection(Segment next) {

        if (next == null) {
            return;
        }
        this.previous = this.direction;
        this.direction = Map.getDirection(position, next);

    }

    private void clearDirection() {
        // cler step counting variables
        this.dX = 0;
        this.dY = 0;
    }

    @Override
    public void updateAnimation() {
        if (animation != null) {
            animation.update();
        }
    }

    public void setGroup(ECharacterGroup group) {
        this.group = group;
    }
}
