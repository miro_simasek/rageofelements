package com.example.ms.rageofelements.render;

public class TextureData {

    public static final float[] tenth = new float[]{
            0.1f, 0.1f,
            0.1f, 0f,
            0f, 0.1f,
            0f, 0f,
    };

    public static final float[] whole = new float[]{
            1f, 0f,
            1f, 1f,
            0f, 0f,
            0f, 1f
    };


    //register tags for texture files or groups
    public static final String NECROMANCER_STANDING = "necromancer_standing";
    public static final String NECROMANCER_ATTACKING = "necromancer_attacking";
    public static final String SKELETON_STANDING = "skeleton_standing";
    public static final String WHOLE_TEXTURE = "whole_texture";
    public static final String NECROMANCER_RUNNING = "necromancer_running";
    public static final String TENTH = "TENTH_TEXTURE";
    public static final String WARRIOR_STANDING = "WARRIOR_STANDING";
    public static final String RANGER_STANDING = "RANGER_STANDING";
    public static final String RANGER_ATTACK = "RANGER_ATTACK";
    public static final String RANGER_RUNNING = "RANGER_RUNNING";
    public static final String RANGER_CASTING = "RANGER_CASTING";
    public static final String RANGER_HIT = "RANGER_HIT";
    public static final String RANGER_SHOOTING = "RANGER_SHOOTING";
    public static final String RANGER_DEATH = "RANGER_DEATH";
    public static final String NECROMANCER_DEATH = "NECROMANCER_DEATH";
    public static final String NECROMANCER_HIT = "NECROMANCER_HIT";
    public static final String WARRIOR_ATTACK = "WARRIOR_ATTACK";
    public static final String WARRIOR_RUNNING = "WARRIOR_RUNNING";
    public static final String WARRIOR_DEATH = "WARRIOR_DEATH";
    public static final String WARRIOR_HIT = "WARRIOR_HIT";
    public static final String VAMPIRES_RUNNING = "VAMPIRES_RUNNING";
    public static final String SKELETON_ARCHER_STANDING = "SKELETON_ARCHER_STANDING";
    public static final String SKELETON_ARCHER_SHOOTING = "SKELETON_ARCHER_SHOOTING";
    public static final String SKELETON_ARCHER_HIT = "SKELETON_ARCHER_HIT";
    public static final String SKELETON_ARCHER_DEATH = "SKELETON_ARCHER_DEATH";
    public static final String SKELETON_ARCHER_RUNNING = "SKELETON_ARCHER_RUNNING";
    public static final String VAMPIRES_ATTACK = "VAMPIRES_ATTACK";
    public static final String VAMPIRES_CASTING = "VAMPIRES_CASTING";
    public static final String VAMPIRES_STANDING = "VAMPIRES_STANDING";
    public static final String VAMPIRES_HIT = "VAMPIRES_HIT";
    public static final String VAMPIRES_DEATH = "VAMPIRES_DEATH";
    public static final String SKELETON_RUNNING = "SKELETON_RUNNING";
    public static final String SKELETON_ATTACK = "SKELETON_ATTACK";
    public static final String SKELETON_HIT = "SKELETON_HIT";
    public static final String SKELETON_DEATH = "SKELETON_DEATH";
}
