package com.example.ms.rageofelements.base;

/**
 * Created by Miroslav Simasek on 1/6/2017.
 */

public interface Constants {

    String characterKey = "CharacterSelected";
    String character_selection_tag = "CharacterSelection";
    String mainMenuTag = "MainMenu";

    String necromancerTag = "Necromancer";
    String warriorTag = "Warrior";
    String rangerTag = "Ranger";

    String abilityScreen = "AbilityScreen";
    String credits_tag = "CreditsScreen";

    String questScreen = "QuestScreen";
    String skillTreeScreen = "SkillTreeScreen";
    String inventoryScreen = "InventoryScreen";
    String tradeScreen = "TradeScreen";
}
