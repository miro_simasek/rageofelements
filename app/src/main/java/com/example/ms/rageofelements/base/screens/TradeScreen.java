package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.util.ItemAdapter;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.combat.Item;
import com.example.ms.rageofelements.combat.Key;

/**
 * Created by MS on 4/1/2017.
 */

public class TradeScreen extends Fragment {

    private static final String PLAYER_ADAPTER = "player_adapter";
    private static final String OTHER_ADAPTER = "other_adapter";
    private static final float EXCHANGE_INDEX = 0.5f;

    private DataManager dataManager;
    private TextView otherGold, playerGold, traderNameLabel;
    private GridView otherInventory, playerInventory;
    private ItemAdapter otherAdapter, playerAdapter;
    private int traderGoldValue, playerGoldValue;
    private String traderName;
    private String baseGoldLabel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = ((GameActivity) getActivity()).getDataManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.trade_screen_layout, container, false);

        playerGoldValue = dataManager.getPlayerGold();
        traderGoldValue = ((Character) dataManager.getDialogData().getOwner()).getGoldValue();

        baseGoldLabel = v.getContext().getString(R.string.character_god_base);
        String text;

        this.traderName = dataManager.getDialogData().getOwner().getName();

        this.traderNameLabel = (TextView) v.findViewById(R.id.trader_name);
        this.traderNameLabel.setText(this.traderName);

        this.playerInventory = (GridView) v.findViewById(R.id.i_my_content);
        this.playerAdapter = new ItemAdapter(v.getContext(), PLAYER_ADAPTER);
        this.playerAdapter.setItems(this.dataManager.getPlayerItems());
        this.playerInventory.setAdapter(this.playerAdapter);
        this.playerGold = (TextView) v.findViewById(R.id.player_gold);

        text = baseGoldLabel.replace("&1", ((GameActivity) getActivity()).getPlayerType())
                .replace("&2", String.valueOf(this.playerGoldValue));
        this.playerGold.setText(text);

        this.otherInventory = (GridView) v.findViewById(R.id.i_other_content);
        this.otherAdapter = new ItemAdapter(v.getContext(), OTHER_ADAPTER);
        this.otherAdapter.setItems(this.dataManager.getOtherItems());
        this.otherInventory.setAdapter(this.otherAdapter);
        this.otherGold = (TextView) v.findViewById(R.id.trader_gold);
        text = baseGoldLabel.replace("&1", this.traderName)
                .replace("&2", String.valueOf(this.traderGoldValue));
        this.otherGold.setText(text);

        this.initControls();

        return v;
    }

    private void initControls() {

        otherInventory.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int index;
                GridView itemsView = (GridView) v;
                switch (event.getAction()) {
                    case DragEvent.ACTION_DROP:

                        index = Integer.valueOf(event.getClipData().getItemAt(0).getText().toString());
                        if (itemsView.getAdapter() instanceof ItemAdapter) {
                            String tag = event.getClipData().getItemAt(1).getText().toString();
                            if (tag.equals(OTHER_ADAPTER)) {
                                return true;
                            }
                            Item item = playerAdapter.getItem(index);
                            // we don't want to allow character to drop keys
                            if (item instanceof Key){
                                Toast.makeText(itemsView.getContext(),R.string.keys_cannot_be_sold,Toast.LENGTH_SHORT)
                                        .show();
                                return true;
                            }
                            int itemValue = (int) (((float) item.getValue()) * EXCHANGE_INDEX);
                            if (traderGoldValue >= itemValue) {
//                                exchange items
                                otherAdapter.addItem(item);
                                playerAdapter.removeItem(index);
//                                notify adapters
                                otherAdapter.notifyDataSetChanged();
                                playerAdapter.notifyDataSetChanged();
//                                remove traders gold
                                traderGoldValue -= itemValue;
                                playerGoldValue += itemValue;

                                otherGold.setText(baseGoldLabel.replace("&1", traderName)
                                        .replace("&2", String.valueOf(traderGoldValue)));
                                playerGold.setText(baseGoldLabel.replace("&1", ((GameActivity) getActivity()).getPlayerType())
                                        .replace("&2", String.valueOf(playerGoldValue)));
                            } else {
                                String text = v.getContext().getString(R.string.trader_out_of_gold)
                                        .replace("&1", traderName)
                                        .replace("&2", item.getName());
                                Toast.makeText(v.getContext(), text, Toast.LENGTH_SHORT).show();
                            }

                        }
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;
                    default:
                        break;
                }

                return false;
            }
        });

        playerInventory.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                GridView itemsView = (GridView) v;
                int index;
                switch (event.getAction()) {
                    case DragEvent.ACTION_DROP:
                        index = Integer.valueOf(event.getClipData().getItemAt(0).getText().toString());
                        if (itemsView.getAdapter() instanceof ItemAdapter) {
                            String tag = event.getClipData().getItemAt(1).getText().toString();
                            if (tag.equals(PLAYER_ADAPTER)) {
                                return true;
                            }
                            Item item = otherAdapter.getItem(index);
                            int itemValue = item.getValue();
                            if (playerGoldValue >= itemValue) {
//                                exchange item
                                playerAdapter.addItem(item);
                                otherAdapter.removeItem(index);
//                                notify adapters
                                otherAdapter.notifyDataSetChanged();
                                playerAdapter.notifyDataSetChanged();

                                // remove players's gold
                                traderGoldValue += itemValue;
                                playerGoldValue -= itemValue;

                                otherGold.setText(baseGoldLabel.replace("&1", traderName)
                                        .replace("&2", String.valueOf(traderGoldValue)));
                                playerGold.setText(baseGoldLabel.replace("&1", ((GameActivity) getActivity()).getPlayerType())
                                        .replace("&2", String.valueOf(playerGoldValue)));
                            } else {
                                String text = v.getContext().getString(R.string.not_enough_gold)
                                        .replace("&1", item.getName());
                                Toast.makeText(v.getContext(), text, Toast.LENGTH_SHORT).show();
                            }

                        }
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;
                    default:
                        break;
                }

                return false;
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        dataManager.setPlayerGold(playerGoldValue);
        ((Character) (dataManager.getDialogData().getOwner())).setGoldValue(traderGoldValue);
//        dataManager.setPlayerItems(playerAdapter.getItems());
    }
}
