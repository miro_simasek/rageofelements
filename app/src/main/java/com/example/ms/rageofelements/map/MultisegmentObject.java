package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureShader;
import com.example.ms.rageofelements.render.VBO;

import java.util.HashMap;

/**
 * Created by Miroslav Simasek
 * Class representing object that is placed on multiple map segments
 */
class MultisegmentObject extends GameEntity{

    private static final String tag = "building";
    private static final String _part = "_part";
    private static final String _corner = "_corner";
    private static final String _first = "_first";
    private static final String _last = "_last";

    // segments on x axis and y axis
    private final Segment[][] area;
    // graphical parts of object
    private HashMap<Segment, VBO> parts;
    // base name to create VBO tags
    private final String name;
    // length in segments
    private final int xLength, yLength;
    // normalized margins
    private float n_marginLeft;
    private float n_marginRight;
    private float n_marginBottom;
    // texture width for single, basic column in normalized size
    private float column_textureWidth;
    // image propreties
    private float imageWidth;
    private float imageHeight;
    // last image part height and width
    private float real_lastWidth, real_firstWidth;
    // margin values
    private float marginLeft, marginRight;
    private float marginBottom;
    // height calculated to keep image proportions
    private float realHeight;
    // texture file
    private int textureResource;

    /**
     * Creates new multi-segment object
     * @param area- occupied area
     * @param name - name of object
     * @param textureResource - texture file
     */
    MultisegmentObject(Segment[][] area, String name, int textureResource) {
        super(area[0][0]);
        this.area = area;
        this.name = name;
        xLength = area.length;
        yLength = area[0].length;
        for (Segment[] anArea : area) {
            for (int j = 0; j < area[0].length; j++) {
                anArea[j].setObstacle(true);
                anArea[j].addEntity(this);
            }
        }
        parts = new HashMap<>();
        this.textureResource = textureResource;

    }


    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        // we dont use classic draw method for multisegment object
    }


    /**
     * Draw part of multi-segment object concerning concrete segment
     * @param segment - segment that requested draw
     * @param matrix - projection matrix
     * @param shaderProgram- shader program
     */
    void drawPart(Segment segment, float[] matrix, TextureShader shaderProgram) {
        VBO part = parts.get(segment);
        if (part != null) {
            part.draw(matrix, shaderProgram);
        }
    }

    private String getPartTag() {
        return tag + this.name + _part;
    }

    private String getCornerTag() {
        return tag + this.name + _corner;
    }

    private String getLastTag() {
        return tag + this.name + _last;
    }

    private String getFirstTag() {
        return tag + this.name + _first;
    }

    /**
     * Set image width
     * @param width - value
     */
    final void setImageWidth(float width) {
        this.imageWidth = width;
        this.recalcDimensions();
    }

    /**
     * Set image height
     * @param height - value
     */
    final void setImageHeight(float height) {
        this.imageHeight = height;
        this.recalcDimensions();
    }

    /**
     * Set left margin
     * @param marginLeft - value
     */
    final void setMarginLeft(float marginLeft) {
        this.marginLeft = marginLeft;
        this.recalcDimensions();
    }

    /**
     * Set right margin
     * @param marginRight - value
     */
    final void setMarginRight(float marginRight) {
        this.marginRight = marginRight;
        this.recalcDimensions();
    }

    /**
     * Set image margin bottom
     * @param marginBottom - margin value
     */
    final void setMarginBottom(float marginBottom) {
        this.marginBottom = marginBottom;
        this.recalcDimensions();
    }

    /**
     * We need to recalculate dimensions of building if some property has changed
     * to provide correct display of building
     */
    private void recalcDimensions() {
        this.n_marginBottom = marginBottom / imageHeight;
        this.n_marginRight = marginRight / imageWidth;
        this.n_marginLeft = marginLeft / imageWidth;
        this.column_textureWidth = (Texture.TEXTURE_WIDTH - (n_marginLeft + n_marginRight)) / (float) (xLength + yLength);
        // calculate how wide the building must be to contain all area and
        // HALF_SEGMENT_WIDTH should contain one column (HALF_SEGMENT_WIDTH ~ column_textureWidth)
        this.real_firstWidth = ((n_marginLeft + column_textureWidth) / column_textureWidth) * Segment.HALF_SEGMENT_WIDTH;
        this.real_lastWidth = ((n_marginRight + column_textureWidth) / column_textureWidth) * Segment.HALF_SEGMENT_WIDTH;

        float width = real_firstWidth + real_lastWidth + ((float) (xLength + yLength) - 1) * Segment.HALF_SEGMENT_WIDTH;
        this.realHeight = width * imageHeight / imageWidth;
    }

    private float[] getFirstData() {

        return new float[]{
                -real_firstWidth, +realHeight - Segment.SEGMENT_HEIGHT,
                -real_firstWidth, -Segment.SEGMENT_HEIGHT,
                0f, +realHeight - Segment.SEGMENT_HEIGHT,
                0f, -Segment.SEGMENT_HEIGHT,


        };

    }

    private float[] getLastData() {
        return new float[]{
                -Segment.HALF_SEGMENT_WIDTH, +realHeight - Segment.SEGMENT_HEIGHT,
                -Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT,
                real_lastWidth - Segment.HALF_SEGMENT_WIDTH, +realHeight - Segment.SEGMENT_HEIGHT,
                real_lastWidth - Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT,


        };
    }

    private float[] getCornerData() {
        return new float[]{
                -Segment.HALF_SEGMENT_WIDTH, +realHeight - Segment.SEGMENT_HEIGHT,
                -Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT,
                Segment.HALF_SEGMENT_WIDTH, +realHeight - Segment.SEGMENT_HEIGHT,
                Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT

        };
    }

    private float[] getPartData() {
        return new float[]{

                -Segment.HALF_SEGMENT_WIDTH, +realHeight - Segment.SEGMENT_HEIGHT,
                -Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT,
                0f, +realHeight - Segment.SEGMENT_HEIGHT,
                0f, -Segment.SEGMENT_HEIGHT,


        };
    }

    private float[] getFirstTextureData() {
        return new float[]{
                0f, n_marginBottom,
                0f, 1f,
                column_textureWidth + n_marginLeft, n_marginBottom,
                column_textureWidth + n_marginLeft, 1f,


        };
    }

    private float[] getLastTextureData() {
        return new float[]{
                1f - (column_textureWidth + n_marginRight), n_marginBottom,
                1f - (column_textureWidth + n_marginRight), 1f,
                1f, n_marginBottom,
                1f, 1f


        };
    }

    private float[] getCornerTextureData() {
        return new float[]{
                0f, n_marginBottom,
                0f, 1f,
                column_textureWidth * 2.0f, n_marginBottom,
                column_textureWidth * 2.0f, 1f
        };
    }

    private float[] getPartTextureData() {
        return new float[]{
                0f, n_marginBottom,
                0f, 1f,
                column_textureWidth, n_marginBottom,
                column_textureWidth, 1f
        };
    }

    @Override
    public void initGraphics() {

        // create graphics for each segment part
        Shape first = new Shape(getFirstData(), getFirstTag());
        Texture t_first = new Texture(getFirstTextureData(), getFirstTag());

        Shape corner = new Shape(getCornerData(), getCornerTag());
        Texture t_corner = new Texture(getCornerTextureData(), getCornerTag());

        Shape part = new Shape(getPartData(), getPartTag());
        Texture t_part = new Texture(getPartTextureData(), getPartTag());

        Shape last = new Shape(getLastData(), getLastTag());
        Texture t_last = new Texture(getLastTextureData(), getLastTag());

        VBO vbo;

        // init first part of building
        float baseY;
        // get first x and y
        Segment position = area[xLength - 1][yLength - 1];
        baseY = position.getY();

        // init first part (from left)
        position = area[xLength - 1][0];
        vbo = new VBO(first, t_first);
        parts.put(position, vbo);
        vbo.translateModelMatrix(position.getX(), baseY);
        vbo.setTextureID(textureResource);

        float textureTranslation = column_textureWidth + n_marginLeft;
        // init parts between first an corner
        for (int i = 1; i < yLength - 1; i++) {

            position = area[xLength - 1][i];
            vbo = new VBO(part, t_part);
            vbo.setTextureID(textureResource);
            vbo.translateModelMatrix(position.getX(), baseY);
            vbo.translateTextureMatrix(textureTranslation, 0f);
            textureTranslation += column_textureWidth;
            parts.put(position, vbo);
        }

        //init corner
        position = area[xLength - 1][yLength - 1];
        vbo = new VBO(corner, t_corner);
        vbo.translateModelMatrix(position.getX(), baseY);
        vbo.translateTextureMatrix(textureTranslation, 0f);
        vbo.setTextureID(textureResource);
        textureTranslation += column_textureWidth * 2.0f;
        parts.put(position, vbo);

        // init remaining parts
        for (int i = xLength - 2; i > 0; i--) {
            position = area[i][yLength - 1];
            vbo = new VBO(part, t_part);
            vbo.translateModelMatrix(position.getX() + Segment.HALF_SEGMENT_WIDTH, baseY);
            vbo.translateTextureMatrix(textureTranslation, 0f);
            vbo.setTextureID(textureResource);
            textureTranslation += column_textureWidth;
            parts.put(position, vbo);
        }

        // init last part
        position = area[0][yLength - 1];
        vbo = new VBO(last, t_last);
        vbo.setTextureID(textureResource);
        vbo.translateModelMatrix(position.getX() + Segment.HALF_SEGMENT_WIDTH, baseY);
        parts.put(position, vbo);


    }

    @Override
    public void update() {
        // nothing to update in this object
    }

    @Override
    public void updateAnimation() {
        // in standard multisegment object we have no animation
    }

    public Segment[][] getArea(){
        return  this.area;
    }

}
