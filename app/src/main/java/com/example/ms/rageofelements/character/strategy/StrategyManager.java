package com.example.ms.rageofelements.character.strategy;

import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.character.EActivities;
import com.example.ms.rageofelements.map.Map;

/**
 * Created by MS on 3/27/2017.
 */

public abstract class StrategyManager {

    private final static long CHECK_TIME = 2000;

    private Map area;
    private Strategy current;
    private Character owner;
    private long lastCheck;


    StrategyManager(Character owner, Map map) {
        this.owner = owner;
        this.area = map;
    }

    public void update() {
        current.execute();
        if (System.currentTimeMillis() - lastCheck >= CHECK_TIME){
            // for performance reasons we will execute checks for strategy change each 2 seconds
            this.check();
            lastCheck = System.currentTimeMillis();
        }
    }

    public void onChange(EActivities newActivity) {
        current.onActivityChange(newActivity);
    }

    protected abstract void check();


    public Map getArea() {
        return area;
    }

    void setCurrent(Strategy current) {
        this.current = current;
    }

    public Character getOwner() {
        return owner;
    }

}
