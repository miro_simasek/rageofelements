package com.example.ms.rageofelements.base.util;

import java.util.ArrayList;


/**
 * Generic class representing tree node
 * @param <T> - data
 */
public class Node<T> {

    private String nodeName;
    private T data;
    private Node<T> parent;
    private ArrayList<Node<T>> children;

    public Node(T data) {
        this.data = data;
        this.children = new ArrayList<>();
    }

    public T getData() {
        return data;
    }

    public ArrayList<Node<T>> getChildren(){
        return children;
    }

    public Node<T> getParent() {
        return parent;
    }

    public void addChild(Node<T> child){
        child.setParent(this);
        this.children.add(child);
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String categoryName) {
        this.nodeName = categoryName;
    }

    public Node<T> getRoot() {

        Node<T> node = this;
        // find root node and return it
        while (node.getParent() != null){
            node = node.getParent();
        }
        return node;
    }
}
