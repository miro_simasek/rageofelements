package com.example.ms.rageofelements.map;

/**
 * Class containing data for pathfinder
 */
public class PathNode {

    //variables for path finding
    private boolean obstacle = false;
    private int cost;
    private int distance;
    private PathNode parent;

    final int yIndex;
    final int xIndex;

    PathNode(int row, int column){
        this.xIndex = row;
        this.yIndex = column;
    }

    public boolean isObstacle() {
        return this.obstacle;
    }

    int getCost() {
        return cost;
    }

    void setCost(int cost) {
        this.cost = cost;
    }

    public void setObstacle(boolean obstacle) {
        this.obstacle = obstacle;
    }

    void setDistance(int distance) {
        this.distance = distance;
    }

    int getF() {
        return distance + cost;
    }

    public void setParent(PathNode parent) {
        this.parent = parent;
    }

    public PathNode getParent() {
        return parent;
    }

    public int getYIndex() {
        return yIndex;
    }

    public int getXIndex() {
        return xIndex;
    }
}
