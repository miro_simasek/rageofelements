package com.example.ms.rageofelements.render;

import android.opengl.GLES20;

import com.example.ms.rageofelements.base.Constants;
import com.example.ms.rageofelements.map.Segment;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.HashMap;

/**
 * Created by MS on 3/17/2017.
 */

public class Shape implements Constants {

    private static HashMap<String, Integer> shapes = new HashMap<>();

    //shape constants
    public static final float humanImageWidth = Segment.SEGMENT_WIDTH * 3;
    public static final float humanImageHeight = Segment.SEGMENT_HEIGHT * 6;
    public static final float humanImageYFit = Segment.SEGMENT_HEIGHT*2.5f;

    // register tags for each shape
    public static final String SEGMENT_SHAPE = "segment_shape";

    private int glShape = -1;

    public Shape(float[] vertexData, String tag){
        if (shapes.get(tag) == null){
            final int[] buffers = new int[1];
            GLES20.glGenBuffers(buffers.length,buffers,0);
            glShape = buffers[0];
            // prepare vertex Buffer Data
            FloatBuffer vertexBuffer = ByteBuffer.allocateDirect(vertexData.length * Renderer.bytesPerFloat)
                    .order(ByteOrder.nativeOrder())
                    .asFloatBuffer();

            vertexBuffer.put(vertexData);
            vertexBuffer.position(0);
            // send data for vertex buffer to GPU
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER,glShape);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER,vertexBuffer.capacity()*Renderer.bytesPerFloat,vertexBuffer,GLES20.GL_STATIC_DRAW);
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER,0);
            shapes.put(tag,glShape);

        }else{
            glShape = shapes.get(tag);
        }
    }

    int getGlShape() {
        return glShape;
    }


    static void disposeAll() {

        int[] toDispose = new int[shapes.size()];
        int i = 0;
        for (Integer integer : shapes.values()) {
            toDispose[i] = integer;
            i++;
        }

        GLES20.glDeleteBuffers(toDispose.length,toDispose,0);
        shapes.clear();

    }
}
