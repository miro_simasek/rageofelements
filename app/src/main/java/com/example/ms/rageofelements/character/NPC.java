package com.example.ms.rageofelements.character;


import android.opengl.Matrix;
import android.util.Log;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.character.strategy.StrategyManager;
import com.example.ms.rageofelements.combat.EDamage;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.map.Segment;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureData;
import com.example.ms.rageofelements.render.TextureShader;
import com.example.ms.rageofelements.render.VBO;

import java.util.ArrayList;

/**
 * Created by Miroslav Simasek
 *
 */

public abstract class NPC extends Character {

    private static final String LIFE_BAR_TAG = "npc_lifebar";
    private VBO lifeBar;
    private ArrayList<Skill> skills;
    private StrategyManager strategyManager;

    NPC(Segment position, ECharacterGroup group) {
        super(position, group);
        skills = new ArrayList<>();
        this.initSkills();
        this.initAbilities();
    }

    protected abstract void initSkills();

    @Override
    public void setPosition(Segment segment) {
        super.setPosition(segment);
        if (animation != null && position != null) {
            animation.setModelMatrixPosition(position.getX(), position.getY());
        }
    }

    @Override
    public void initGraphics() {
        Shape shape = new Shape(getLifeBarData(), LIFE_BAR_TAG);
        Texture texture = new Texture(TextureData.whole, TextureData.WHOLE_TEXTURE);
        lifeBar = new VBO(shape, texture);
        lifeBar.setTextureID(R.drawable.health);
        lifeBar.translateModelMatrix(this.position.getX(), this.position.getY());
    }

    @Override
    public void draw(float[] matrix, TextureShader shaderProgram) {
        super.draw(matrix, shaderProgram);
        if (lifeBar != null)
            lifeBar.draw(matrix, shaderProgram);

    }

    private float[] getLifeBarData() {
        return new float[]{
                -Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT + Segment.HALF_SEGMENT_HEIGHT / 2.0f,
                -Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT,
                Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT + Segment.HALF_SEGMENT_HEIGHT / 2.0f,
                Segment.HALF_SEGMENT_WIDTH, -Segment.SEGMENT_HEIGHT
        };


    }

    @Override
    public void hit(EDamage type, float value) {
        super.hit(type, value);
        Log.w(this.getClass().getSimpleName(), "HEALT : " + abilities.getHealth());
        this.adjustLifeBar();
    }

    private void adjustLifeBar() {
        if (lifeBar == null || animation == null)
            return;
        synchronized (lifeBar) {
            float[] lifeBarM = lifeBar.getModelMatrix();
            System.arraycopy(animation.getModelMatrix(), 0, lifeBarM, 0, lifeBarM.length);
            float scale = abilities.getHealth() / abilities.getMaxHelth();
            // if scale < 0 npc is dead we don't want to display life-bar
            if (scale < 0) scale = 0f;
            Matrix.scaleM(lifeBarM, 0, scale , 1f, 1f);
        }
    }

    @Override
    protected void adjustAnimation(boolean partMovement) {
        super.adjustAnimation(partMovement);
        lifeBar.translateModelMatrix(-dX, dY);

    }

    @Override
    protected void regen() {
        super.regen();
        this.adjustLifeBar();
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    @Override
    public void update() {
        super.update();
        strategyManager.update();
    }

    @Override
    public void setActivity(EActivities activity) {
        super.setActivity(activity);
        this.strategyManager.onChange(activity);
    }

    public void setStrategyManager(StrategyManager strategyManager) {
        this.strategyManager = strategyManager;
    }

    public StrategyManager getStrategyManager() {
        return strategyManager;
    }
}
