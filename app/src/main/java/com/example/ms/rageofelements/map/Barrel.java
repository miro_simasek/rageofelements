package com.example.ms.rageofelements.map;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.render.Shape;
import com.example.ms.rageofelements.render.Texture;
import com.example.ms.rageofelements.render.TextureData;
import com.example.ms.rageofelements.render.VBO;

/**
 * Created by Miroslav Simasek
 *
 */
class Barrel extends VBO {
    private static final String barrel_tag = "barrel";
    private static final float BARREL_HEIGHT = Segment.SEGMENT_HEIGHT * 6f;
    private static final float BARREL_WIDTH = Segment.SEGMENT_HEIGHT * 6f;
    private static final float BARREL_IMAGE_Y_FIT = Segment.SEGMENT_HEIGHT *2.5f;

    private static final float[] barelShape = new float[]{

            -BARREL_WIDTH / 2f, BARREL_HEIGHT - BARREL_IMAGE_Y_FIT,
            -BARREL_WIDTH / 2f, -BARREL_IMAGE_Y_FIT,
            BARREL_WIDTH / 2f, BARREL_HEIGHT - BARREL_IMAGE_Y_FIT,
            BARREL_WIDTH / 2f, -BARREL_IMAGE_Y_FIT
    };
    Barrel() {
        super(new Shape(barelShape,barrel_tag),new Texture(TextureData.whole, TextureData.WHOLE_TEXTURE));
        this.setTextureID(R.drawable.barrel_1);
    }
}
