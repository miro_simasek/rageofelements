package com.example.ms.rageofelements.render;


interface ShaderConstants {

    //uniform constants
    //  model matrix
    String U_MODEL_MATRIX = "u_ModelMatrix";
    //  projection matrix
    String U_MATRIX = "u_ProjectionMatrix";
    //  texture unit
    String U_TEXTURE_UNIT = "u_TextureUnit";
    //  texture transformation matrix
    String U_TEXTURE_MATRIX = "u_TextureMatrix";
    //Attribute constants
    String A_POSITION = "a_Position";
    //    Texture Coordinates Attribute
    String A_TEXTURE_COORDINATES = "a_TextureCoordinates";


}
