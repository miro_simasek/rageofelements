package com.example.ms.rageofelements.quests;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.AreaManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.map.GameEntity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by MS on 4/16/2017.
 */
public class QuestManager{

    private HashMap<String, Quest> quests;
    private final ArrayList<Quest> activeQuests;
    private final ArrayList<Quest> completedQuests;
    private ArrayList<GameEntity> questEntities;
    private AreaManager areaManager;

    public QuestManager(AreaManager areaManager) {
        this.areaManager = areaManager;
        activeQuests = new ArrayList<>();
        completedQuests = new ArrayList<>();
        questEntities = new ArrayList<>();
        quests = new HashMap<>();
        // create quest visit Roshanak
        Quest quest = new VisitRoshanak(this);
        String key = GameActivity.getTextFromResources(R.string.quest_visit_roshanaks_hut);
        quests.put(key, quest);

        //create get work from Roshanak
        quest = new GetWorkFromRoshanak(this);
        key = GameActivity.getTextFromResources(R.string.quest_get_roshanaks_work);
        quests.put(key, quest);

        // create quest visit southwest forest
        quest = new VisitNortheastForest(this);
        key = GameActivity.getTextFromResources(R.string.Dialog_visitNortheastForest);
        quests.put(key, quest);

        // create report back to roshanak quest
        quest = new ReportBackToRoshanak(this);
        key = GameActivity.getTextFromResources(R.string.quest_report_to_roshanak);
        quests.put(key,quest);

        // enter the tower
        quest = new EnterTheTower(this);
        key = GameActivity.getTextFromResources(R.string.quest_enter_the_tower);
        quests.put(key,quest);

        // confront summoner
        quest = new ConfrontSummoner(this);
        key = GameActivity.getTextFromResources(R.string.quest_confront_summoner);
        quests.put(key,quest);

        // kill the summoner
        quest = new KillTheSummoner(this);
        key = GameActivity.getTextFromResources(R.string.quest_kill_the_summoner);
        quests.put(key,quest);

        // escape the tower
        quest = new EscapeTheTower(this);
        key = GameActivity.getTextFromResources(R.string.quest_escape_the_tower);
        quests.put(key,quest);

        // speak to roshanak
        quest = new SpeakToRoshanak(this);
        key = GameActivity.getTextFromResources(R.string.quest_speak_to_roshanak);
        quests.put(key,quest);

        // finish the game
        quest = new FinishTheGame(this);
        key = GameActivity.getTextFromResources(R.string.quest_finish_game);
        quests.put(key,quest);

    }

    public Quest getQuestInstance(int resKey) {
        return quests.get(GameActivity.getTextFromResources(resKey));
    }

    public void addQuest(Quest quest) {
        this.activeQuests.add(quest);
        quest.onAccept();
    }

    public void checkQuests() {

        for (int i = 0; i < activeQuests.size(); i++) {
            Quest quest = activeQuests.get(i);
            if (quest.isCompleted()) {
                quest.onCompletion();
                activeQuests.remove(quest);
                completedQuests.add(quest);
                Quest next = quest.getNext();
                if (next!= null){
                    this.addQuest(next);
                    next.onAccept();
                    areaManager.sendNotification(GameActivity.getTextFromResources(R.string.quest)+" "+
                            next.getName()+" "+
                            GameActivity.getTextFromResources(R.string.started));

                }
            }
        }

    }

    public ArrayList<Quest> getActiveQuests() {
        return activeQuests;
    }

    public ArrayList<Quest> getCompletedQuests() {
        return completedQuests;
    }

    public void init() {
        this.addQuest(quests.get(GameActivity.getTextFromResources(R.string.quest_visit_roshanaks_hut)));
    }

    ArrayList<GameEntity> getQuestEntities() {
        return questEntities;
    }

    public void addQuestEntity(GameEntity entity){
        this.questEntities.add(entity);
    }

    public AreaManager getAreaManager() {
        return areaManager;
    }
}
