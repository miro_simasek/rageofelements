package com.example.ms.rageofelements.combat;

import android.util.Log;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.character.Character;
import com.example.ms.rageofelements.map.GameEntity;
import com.example.ms.rageofelements.map.Segment;

/**
 * Created by MS on 3/25/2017.
 */

public class HealingPotion extends Item implements Usable{

    public HealingPotion() {
        super(R.drawable.healing_potion, 100,
                (String) GameActivity.getAppResources().getText(R.string.minor_heling_potion));
    }

    @Override
    public GameEntity[] onUse(Character character, Segment... segment) {
//        character.getInventory();
        int healthValue = 30;
        character.getAbilities().addHealth(healthValue);
        Log.w(character.getClass().getSimpleName(),String.valueOf(character.getAbilities().getHealth()));
        return null;
    }

    @Override
    public int getIcon() {
        return super.getIconSource();
    }

}
