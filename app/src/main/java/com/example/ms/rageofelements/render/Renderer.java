package com.example.ms.rageofelements.render;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.screens.GamePlayScreen;
import com.example.ms.rageofelements.character.Player;
import com.example.ms.rageofelements.map.Map;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES20.GL_BLEND;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_SRC_ALPHA;
import static android.opengl.Matrix.orthoM;

/**
 * Created by Miroslav Simasek
 *
 */

public class Renderer implements GLSurfaceView.Renderer {

    public static final float scaleFactor = 100.0f;
    //  number of bytes per float
    static final int bytesPerFloat = Float.SIZE / 8;

    private static TextureLoader textureLoader;
    private static float aspectRatio;
    private static float[] projectionMatrix;
    private static float[] playerMatrix;
    private Context context;
    private TextureShader textureShader;
    private Map map;
    private Player player;
    private GamePlayScreen root;


    public Renderer(Context context) {
        this.context = context;
        projectionMatrix = new float[16];
        playerMatrix = new float[16];
        textureLoader = new TextureLoader();
    }

    public static float[] getPlayerMatrix() {
        return playerMatrix;
    }

    public static float getAspectRatio() {
        return aspectRatio;
    }

    static TextureLoader getTextureLoader() {
        return textureLoader;
    }

    public static synchronized void setProjectionMatrixT(float x, float y) {
        System.arraycopy(playerMatrix, 0, projectionMatrix, 0, playerMatrix.length);
        Matrix.translateM(projectionMatrix,0,-x,-y,0f);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Matrix.setIdentityM(projectionMatrix, 0);
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 0f);
        textureShader = new TextureShader(context);
        Player.loadInstance(player);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

        GLES20.glViewport(0, 0, width, height);
        aspectRatio = width > height ? (float) width / (float) height : (float) height / (float) width;


        if (width > height) {
            orthoM(projectionMatrix, 0, -aspectRatio * scaleFactor, aspectRatio * scaleFactor, -1f * scaleFactor, 1f * scaleFactor, -1f, 1f);
        } else {
            orthoM(projectionMatrix, 0, -1f * scaleFactor, 1f * scaleFactor, -aspectRatio * scaleFactor, aspectRatio * scaleFactor, -1f, 1f);
        }

        System.arraycopy(projectionMatrix, 0, playerMatrix, 0, projectionMatrix.length);
        Matrix.translateM(projectionMatrix, 0, -player.getPosition().getX(), -player.getPosition().getY(), 0f);
        // load textures necessary for game
        this.loadTextures();

        map.initGraphics();
        root.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // notify game that gl context is ready
                root.startGame();
            }
        });


    }

    private void loadTextures() {
        // load necessary textures
        textureLoader.loadTextures(
                R.drawable.necromancer_standing_s,
                R.drawable.skeleton_archer_standing,
                R.drawable.necromancer_running_n,
                R.drawable.necromancer_running_nw,
                R.drawable.necromancer_running_sw,
                R.drawable.vampiress_death,
                R.drawable.skeleton_running2,
                R.drawable.skeleton_hit,
                R.drawable.skeleton_attack2,
                R.drawable.necromancer_standing_e,
                R.drawable.icicle,
                R.drawable.skeleton_archer_running2,
                R.drawable.necromancer_standing_nw,
                R.drawable.vampiress_attack,
                R.drawable.skeleton_archer_hit,
                R.drawable.vampiress_standing,
                R.drawable.necromancer_standing_w,
                R.drawable.skeleton_archer_death,
                R.drawable.health,
                R.drawable.skeleton_death1,
                R.drawable.necromancer_running_e,
                R.drawable.vampires_running1,
                R.drawable.necromancer_running_s,
                R.drawable.necromancer_standing_ne,
                R.drawable.skeleton_running1,
                R.drawable.skeleton_archer_shooting,
                R.drawable.skeleton_standing,
                R.drawable.necromancer_running_ne,
                R.drawable.skeleton_attack1,
                R.drawable.vampiress_hit,
                R.drawable.necromancer_standing_n,
                R.drawable.skeleton_death2,
                R.drawable.necromancer_standing_se,
                R.drawable.necromancer_standing_sw,
                R.drawable.necromancer_running_se,
                R.drawable.well,
                R.drawable.vampires_running2,
                R.drawable.necromancer_running_w,
                R.drawable.skeleton_archer_running1,
                R.drawable.vampiress_casting,
                R.drawable.grass_texture1,
                R.drawable.green_tree1,
                R.drawable.building_hut,
                R.drawable.broken_tower,
                R.drawable.barrel_1,
                R.drawable.door,
                R.drawable.magic_shield
        );
    }

    @Override
    public void onDrawFrame(GL10 gl) {
//       prepare for drawing
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 0f);
        GLES20.glClear(GL_COLOR_BUFFER_BIT);
        GLES20.glDisable(GL_DEPTH_TEST);

        GLES20.glBlendFunc(GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glEnable(GL_BLEND);

        textureShader.useProgram();

        if (map != null) {
            map.draw(projectionMatrix, textureShader);
        }
        player.getPlayerBar().updateStatus();
        player.getPlayerBar().draw(projectionMatrix, textureShader);

        GLES20.glDisable(GL_BLEND);
        GLES20.glEnable(GL_DEPTH_TEST);

    }

    public void translateProjectionMatrix(float dX, float dY) {
        Matrix.translateM(projectionMatrix, 0, -dX, -dY, 0f);
    }

    public void disposeCurrent() {
        Shape.disposeAll();
        Texture.disposeAll();
        TextureLoader.clearTextures();
    }

    public void initRenderable(Renderable renderable) {
        renderable.initGraphics();
    }

    public void initGameDisplay(Map map, Player player) {
        this.map = map;
        this.map.setPlayer(player);
        this.player = player;

        System.arraycopy(playerMatrix, 0, projectionMatrix, 0, playerMatrix.length);
        Matrix.translateM(projectionMatrix, 0, -player.getPosition().getX(), -player.getPosition().getY(), 0f);
    }

    public void setRoot(GamePlayScreen root) {
        this.root = root;
    }
}
