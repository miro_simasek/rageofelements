package com.example.ms.rageofelements.base.screens;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.DataManager;
import com.example.ms.rageofelements.base.GameActivity;
import com.example.ms.rageofelements.base.util.InstantActionButton;
import com.example.ms.rageofelements.base.util.Node;
import com.example.ms.rageofelements.base.util.OnSkillChangeListener;
import com.example.ms.rageofelements.base.util.SkillNodeView;
import com.example.ms.rageofelements.combat.Skill;
import com.example.ms.rageofelements.combat.Usable;

import java.util.ArrayList;

/**
 * Ffragment to display skill tree for player
 */
public class SkillTreeScreen extends Fragment implements OnSkillChangeListener {

    private DataManager dataManager;
    private LinearLayout skillTree;
    private int skillsInTree;
    private InstantActionButton[] activeUsables;
    private TextView pointsLabel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activeUsables = new InstantActionButton[GamePlayScreen.activeButtonsCount];
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.skill_tree_layout, container, false);

        dataManager = ((GameActivity) getActivity()).getDataManager();
        skillTree = (LinearLayout) v.findViewById(R.id.skill_tree);

        ArrayList<Node<Skill>> skillRoots = dataManager.getSkillRoots();
        if (skillRoots.isEmpty()) {
            return v;
        }



        LinearLayout ll = (LinearLayout) v.findViewById(R.id.skill_classes_list);
        // create button for each skill category
        for (Node<Skill> r : skillRoots) {
            Button genButton;
            genButton = (Button) inflater.inflate(R.layout.button_template, ll, false);
            genButton.setText(r.getNodeName());
            final Node<Skill> rootNode = r;
            // add listener to display skills for category
            genButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refreshLayout(rootNode);
                }
            });

            ll.addView(genButton);
        }

        pointsLabel = (TextView) v.findViewById(R.id.skill_points_value);
        pointsLabel.setText(String.valueOf(dataManager.getAbilities().getSkillPoints()));

        Node<Skill> root = skillRoots.get(0);
        refreshLayout(root);

        View.OnDragListener onDragListener = new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                if (event.getAction() == DragEvent.ACTION_DROP) {
                    InstantActionButton iab = (InstantActionButton) v;
                    int index;
                    index = Integer.valueOf(event.getClipData().getItemAt(0).getText().toString());
                    SkillNodeView skillNodeView = (SkillNodeView) skillTree.getChildAt(index - 1);
                    iab.setUsabe(skillNodeView.getSkill());
                    return true;
                }
                return true;
            }
        };

        activeUsables[0] = (InstantActionButton) v.findViewById(R.id.activeView1);
        activeUsables[0].setOnDragListener(onDragListener);
        activeUsables[1] = (InstantActionButton) v.findViewById(R.id.activeView2);
        activeUsables[1].setOnDragListener(onDragListener);
        activeUsables[2] = (InstantActionButton) v.findViewById(R.id.activeView3);
        activeUsables[2].setOnDragListener(onDragListener);
        activeUsables[3] = (InstantActionButton) v.findViewById(R.id.activeView4);
        activeUsables[3].setOnDragListener(onDragListener);
        activeUsables[4] = (InstantActionButton) v.findViewById(R.id.activeView5);
        activeUsables[4].setOnDragListener(onDragListener);

        return v;
    }

    public void refreshLayout(Node<Skill> root) {
        if (root.getParent() == null) {
            // we are int the root node
            skillTree.removeAllViews();
            skillsInTree = 0;
        }
        SkillNodeView skillNodeView = new SkillNodeView(skillTree.getContext());
        skillNodeView.setSkillNode(root, ++this.skillsInTree);
        skillNodeView.setOnSkillChangeListener(this);
        skillNodeView.setDataManager(dataManager);

        skillTree.addView(skillNodeView);


        for (Node<Skill> child : root.getChildren()) {
            refreshLayout(child);
        }

    }

    @Override
    public void onPause() {
        super.onPause();

        Usable[] usables = new Usable[activeUsables.length];
        for (int i = 0; i < activeUsables.length; i++) {
            usables[i] = activeUsables[i].getUsable();
        }
        dataManager.setActionButtonsContent(usables);
    }

    @Override
    public void onResume() {
        super.onResume();
        Usable[] usables = dataManager.getActionButtonsContent();
        for (int i = 0; i < usables.length; i++) {
            if (activeUsables[i] != null)
                activeUsables[i].setUsabe(usables[i]);
        }
    }

    @Override
    public void onSkillChange(Node<Skill> skill) {
        this.refreshLayout(skill.getRoot());
        pointsLabel.setText(String.valueOf(dataManager.getAbilities().getSkillPoints()));
    }
}
