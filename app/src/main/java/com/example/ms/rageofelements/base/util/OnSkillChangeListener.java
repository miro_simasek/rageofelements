package com.example.ms.rageofelements.base.util;

import com.example.ms.rageofelements.combat.Skill;



public interface OnSkillChangeListener {

    void onSkillChange(Node<Skill> root);
}
