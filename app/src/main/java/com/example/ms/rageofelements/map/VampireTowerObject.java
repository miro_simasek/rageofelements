package com.example.ms.rageofelements.map;

import android.content.res.Resources;

import com.example.ms.rageofelements.R;
import com.example.ms.rageofelements.base.GameActivity;

/**
 * Created by Miroslav Simasek
 * Building Tower
 */

public class VampireTowerObject extends MultisegmentObject {

    VampireTowerObject(Segment[][] area) {
        super(area, "tower1", R.drawable.tower1);

        Resources resources = GameActivity.getAppResources();
        this.setImageHeight(resources.getDimension(R.dimen.tower1_height));
        this.setImageWidth(resources.getDimension(R.dimen.tower1_width));
        this.setMarginRight(resources.getDimension(R.dimen.tower1_margin_right));

    }
}
